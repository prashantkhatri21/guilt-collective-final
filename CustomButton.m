//
//  CustomButton.m
//  Billangua
//
//  Created by Prashant khatri on 15/10/15.
//  Copyright (c) 2015 360itpro. All rights reserved.
//

#import "CustomButton.h"
#import "Utility.h"
@implementation CustomButton{
    NSString *_language;
}

- (void)awakeFromNib{
    
   
   
    [self.titleLabel setFont:[self whichFontShouldBeUsed]];
}

- (UIFont *)whichFontShouldBeUsed{
    
    UIFont *fontToUse = [UIFont fontWithName:@"OSWALD"
                                        size:self.titleLabel.font.pointSize-2];
    return fontToUse;
}
@end
