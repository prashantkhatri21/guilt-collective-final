//
//  CustomLabel.m
//  Billangua
//
//  Created by Prashant khatri on 15/10/15.
//  Copyright (c) 2015 360itpro. All rights reserved.
//

#import "CustomLabel.h"
#import "Utility.h"
@implementation CustomLabel
{
    NSString *_language;
}
- (void)awakeFromNib{
    [super awakeFromNib];
    [self setFont:[self whichFontShouldBeUsed]];
    
    
    
    
}


- (UIFont *)whichFontShouldBeUsed{
    
    UIFont *fontToUse = [UIFont fontWithName:@"OSWALD"
                                        size:self.font.pointSize-2];
    return fontToUse;
}

@end
