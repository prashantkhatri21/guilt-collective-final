//
//  CustomTextField.m
//  Billangua
//
//  Created by Prashant khatri on 19/10/15.
//  Copyright © 2015 360itpro. All rights reserved.
//

#import "CustomTextField.h"
#import "Utility.h"
@implementation CustomTextField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib{
    
    
    
    [self setFont:[self whichFontShouldBeUsed]];
}

- (UIFont *)whichFontShouldBeUsed{
    
    UIFont *fontToUse = [UIFont fontWithName:@"OSWALD"
                                        size:self.font.pointSize];
    return fontToUse;
}


@end
