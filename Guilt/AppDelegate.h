//
//  AppDelegate.h
//  Guilt
//
//  Created by Gourav Sharma on 10/28/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "ModelProfile.h"
#import "Instagram.h"
#import <Google/SignIn.h>
#import <Google/SignIn.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate>
@property (strong, nonatomic) Instagram *instagram;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ModelProfile *profile;

-(BOOL)isNetwork;
+(id)sharedAppdelegate;
-(void)showAlert:(NSString *)title message:(NSString*)msg;

-(void)showToast:(NSString *)msgString;

-(void)removeToast;
-(void)removeProgress;
-(void)ShowProgress;

-(void)initializeMBProgres:(UIView*)view;
@end

