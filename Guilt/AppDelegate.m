//
//  AppDelegate.m
//  Guilt
//
//  Created by Gourav Sharma on 10/28/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"
#import "Utility.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#define APP_ID @"fd725621c5e44198a5b8ad3f7a0ffa09"

@interface AppDelegate ()
{
    Reachability *reachability;
    MBProgressHUD *HUD,*HUDToast;
}
@end

@implementation AppDelegate
@synthesize profile;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSError* configureError;
    //simple    acfguyasgdfsdgfhg
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    
    self.instagram = [[Instagram alloc] initWithClientId:APP_ID
                                                delegate:nil];
   
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];

    reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    
    // Override point for customization after application launch.
    
    
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];;
}

- (BOOL)application: (UIApplication *)application
            openURL: (NSURL *)url
  sourceApplication: (NSString *)sourceApplication
         annotation: (id)annotation
{
    if ([url.scheme isEqualToString:@"fb1925573477668484"])
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation];
        
    }
    else if ([url.scheme isEqualToString:@"com.googleusercontent.apps.877623624245-5quo1h9gisukq566e5amtfhkceb1t4mo"])
    {
        NSDictionary *options = @{UIApplicationOpenURLOptionsSourceApplicationKey: sourceApplication,
                                  UIApplicationOpenURLOptionsAnnotationKey: annotation};
        return [self application:application
                         openURL:url
                         options:options];
    }
    else{
        return [self.instagram handleOpenURL:url];

    }

}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options {
    
    
    if ([url.scheme isEqualToString:@"fb1925573477668484"])
    {
        return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                              openURL:url
                                                    sourceApplication:  options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                           annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
        

    }
    else if ([url.scheme isEqualToString:@"com.googleusercontent.apps.877623624245-5quo1h9gisukq566e5amtfhkceb1t4mo"])
    {
        return [[GIDSignIn sharedInstance] handleURL:url
                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];

    }
    else{
        return [self.instagram handleOpenURL:url];
        
    }
    
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *name = user.profile.name;
    NSString *email = user.profile.email;
    // ...
}
-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    return [self.instagram handleOpenURL:url];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

/**
 *  Network check
 *
 *  @return return bool
 */
-(BOOL)isNetwork
{
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable) {return NO;}
    else if (remoteHostStatus == ReachableViaWiFi) {return YES; }
    else if (remoteHostStatus == ReachableViaWWAN) {return YES; }
    else{
        return YES;
    }
}
/**
 *  On network chnage method fire
 *
 *  @param notice notification
 */
- (void) handleNetworkChange:(NSNotification *)notice
{
    
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    if(remoteHostStatus == NotReachable)
    {
        [self showAlert:@"" message:@"No Internet Access"];
    }
    else if (remoteHostStatus == ReachableViaWiFi) {NSLog(@"wifi"); }
    else if (remoteHostStatus == ReachableViaWWAN) {NSLog(@"cell"); }
}

-(void)showAlert:(NSString *)title message:(NSString*)msg
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:title message:msg delegate:nil cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
    [alert show];
}

/*!
 @brief Shared instance
 
 @discussion method use for add loading view
 
 @param nothing
 
 @return nothing
 */
+(AppDelegate *)sharedAppdelegate

{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
    
}


-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:view];
    HUD.color=[UIColor whiteColor];
   
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;

    
    [view addSubview:HUDToast];
    
}

#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}
@end
