//
//  CameraVC.m
//  Guilt
//
//  Created by Gourav Sharma on 11/3/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "CameraControlVC.h"
#import "LLSimpleCamera.h"
#import "UploadViewController.h"
#import "Utility.h"
#import "FilterVCUpload.h"
#import "TOCropViewController.h"
//#import "ImageCropView.h"
@interface CameraControlVC ()<UINavigationControllerDelegate,ImageCropViewControllerDelegate,UIImagePickerControllerDelegate>
{
    
    
    
}
@property (strong, nonatomic) UIButton *snapButton;
@property (strong, nonatomic) UIButton *switchButton;
@property (strong, nonatomic) UIButton *flashButton;

@property (strong, nonatomic) UIButton *mediaButton;
@property (strong, nonatomic) UIButton *instagramIcon;
@property (strong, nonatomic) UIButton *crossIcon;

@property (strong, nonatomic) UILabel *errorLabel;
@property (strong, nonatomic) LLSimpleCamera *camera;

@end

@implementation CameraControlVC
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Set Title
        UIImage *musicImage = [UIImage imageNamed:@"camra"];
        UIImage *musicImageSel = [UIImage imageNamed:@"snapWhite"];
        
        musicImage = [musicImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        musicImageSel = [musicImageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        self.navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"SNAP" image:musicImage selectedImage:musicImageSel];
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor blackColor], NSForegroundColorAttributeName,[UIFont fontWithName:@"OSWALD" size:10], UITextAttributeFont,
                                                           nil] forState:UIControlStateNormal];
        
        
        
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];

    // ----- initialize camera -------- //
    CGRect screenRect = [[UIScreen mainScreen] bounds];

    // create camera vc
    self.camera = [[LLSimpleCamera alloc] initWithQuality:AVCaptureSessionPresetHigh
                                                 position:CameraPositionBack
                                             videoEnabled:YES];
    
    // attach to a view controller
     [self.camera attachToViewController:self withFrame:CGRectMake(0, 0, screenRect.size.width, screenRect.size.height)];
    
    // read: http://stackoverflow.com/questions/5427656/ios-uiimagepickercontroller-result-image-orientation-after-upload
    // you probably will want to set this to YES, if you are going view the image outside iOS.
    self.camera.fixOrientationAfterCapture = NO;
    
    // take the required actions on a device change
    __weak typeof(self) weakSelf = self;
    [self.camera setOnDeviceChange:^(LLSimpleCamera *camera, AVCaptureDevice * device) {
        
        NSLog(@"Device changed.");
        
        // device changed, check if flash is available
        if([camera isFlashAvailable]) {
            weakSelf.flashButton.hidden = NO;
            
            if(camera.flash == CameraFlashOff) {
                weakSelf.flashButton.selected = NO;
            }
            else {
                weakSelf.flashButton.selected = YES;
            }
        }
        else {
            weakSelf.flashButton.hidden = YES;
        }
    }];
    
    [self.camera setOnError:^(LLSimpleCamera *camera, NSError *error) {
        NSLog(@"Camera error: %@", error);
        
        if([error.domain isEqualToString:LLSimpleCameraErrorDomain]) {
            if(error.code == LLSimpleCameraErrorCodeCameraPermission ||
               error.code == LLSimpleCameraErrorCodeMicrophonePermission) {
                
                if(weakSelf.errorLabel) {
                    [weakSelf.errorLabel removeFromSuperview];
                }
                
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
                label.text = @"We need permission for the camera.\nPlease go to your settings.";
                label.numberOfLines = 2;
                label.lineBreakMode = NSLineBreakByWordWrapping;
                label.backgroundColor = [UIColor clearColor];
                label.font = [UIFont fontWithName:@"AvenirNext-DemiBold" size:13.0f];
                label.textColor = [UIColor whiteColor];
                label.textAlignment = NSTextAlignmentCenter;
                [label sizeToFit];
label.center = CGPointMake(screenRect.size.width / 2.0f, screenRect.size.height / 2.0f);                weakSelf.errorLabel = label;
                [weakSelf.view addSubview:weakSelf.errorLabel];
            }
        }
    }];
    
    // ----- camera buttons -------- //
    
    // snap button to capture image
    self.snapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.snapButton.frame = CGRectMake(CGRectGetMidX(self.view.frame)-35, self.view.frame.size.height-150, 70.0f, 70.0f);
    self.snapButton.clipsToBounds = YES;
    [self.snapButton setBackgroundImage:[UIImage imageNamed:@"circle_cam"] forState:UIControlStateNormal];
    self.snapButton.layer.cornerRadius = self.snapButton.frame.size.width / 2.0f;
    self.snapButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.snapButton.layer.borderWidth = 2.0f;
    self.snapButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    self.snapButton.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.snapButton.layer.shouldRasterize = YES;
    [self.snapButton addTarget:self action:@selector(snapButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.snapButton];
    
    // button to toggle flash
    self.flashButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.flashButton.frame = CGRectMake(self.view.frame.size.width-60, 16, 40, 40);
    self.flashButton.tintColor = [UIColor whiteColor];
    [self.flashButton setImage:[UIImage imageNamed:@"camera-flash.png"] forState:UIControlStateNormal];
//    self.flashButton.imageEdgeInsets = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
    [self.flashButton addTarget:self action:@selector(flashButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.flashButton];
    
    
    // button to toggle camera positions
    self.crossIcon = [UIButton buttonWithType:UIButtonTypeSystem];
    self.crossIcon.frame = CGRectMake(20, 16, 40, 40);
    self.crossIcon.tintColor = [UIColor whiteColor];
    [self.crossIcon setImage:[UIImage imageNamed:@"cross.png"] forState:UIControlStateNormal];
//    self.crossIcon.imageEdgeInsets = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
     [self.crossIcon addTarget:self action:@selector(crossButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.crossIcon];
    
    // button to toggle camera positions
    self.switchButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.switchButton.frame = CGRectMake(CGRectGetMidX(self.view.frame)-15, 16, 40, 40);
    self.switchButton.tintColor = [UIColor whiteColor];
    [self.switchButton setImage:[UIImage imageNamed:@"refreshIcon"] forState:UIControlStateNormal];
//    self.switchButton.imageEdgeInsets = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
    [self.switchButton addTarget:self action:@selector(switchButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.switchButton];
    
    
    
    // button to toggle camera positions
    self.instagramIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    self.instagramIcon.frame = CGRectMake(self.view.frame.size.width-60, self.view.frame.size.height-100, 40, 40);
    self.instagramIcon.tintColor = [UIColor whiteColor];
    [self.instagramIcon setImage:[UIImage imageNamed:@"instagram"] forState:UIControlStateNormal];
//    self.instagramIcon.imageEdgeInsets = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
    [self.instagramIcon addTarget:self action:@selector(instagramButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.instagramIcon];
    
    
    // button to toggle camera positions
    self.mediaButton = [UIButton buttonWithType:UIButtonTypeSystem];
    self.mediaButton.frame = CGRectMake(20, self.view.frame.size.height-100, 40, 40);
    self.mediaButton.tintColor = [UIColor whiteColor];
    [self.mediaButton setImage:[UIImage imageNamed:@"box"] forState:UIControlStateNormal];
//    self.mediaButton.imageEdgeInsets = UIEdgeInsetsMake(10.0f, 10.0f, 10.0f, 10.0f);
    [self.mediaButton addTarget:self action:@selector(mediaButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.mediaButton];
    
    
  //  imageView = [[UIImageView alloc]init];
//    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//    {
//        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
//        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
//        [imagePicker setDelegate:self];
//        [self presentViewController:imagePicker animated:YES completion:nil];
//    }
//    else
//    {
//        [[[UIAlertView alloc] initWithTitle:@"Warning" message:@"Your device doesn't have a camera." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
//    }
    // Do any additional setup after loading the view, typically from a nib.
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;

    // start the camera
    [self.camera start];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // stop the camera
    [self.camera stop];
}
- (void)snapButtonPressed:(UIButton *)button {
    
  
        // capture
        [self.camera capture:^(LLSimpleCamera *camera, UIImage *image, NSDictionary *metadata, NSError *error) {
            if(!error) {
                
                // we should stop the camera, since we don't need it anymore. We will open a new vc.
                // this very important, otherwise you may experience memory crashes
                [camera stop];
                
                // show the image
                imageView.image = image;
                [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
                
                if(image != nil){
                    ImageCropViewController *controller = [[ImageCropViewController alloc] initWithImage:image];
                    controller.delegate = self;
                    controller.blurredBackground = YES;
                    [[self navigationController] pushViewController:controller animated:YES];
                }

            }
            else {
                NSLog(@"An error has occured: %@", error);
            }
        } exactSeenImage:YES];
    
    
}




/* camera button methods */

- (void)switchButtonPressed:(UIButton *)button {
    [self.camera togglePosition];
}
- (void)instagramButtonPressed:(UIButton *)button {
    [self performSegueWithIdentifier:@"instagram" sender:self];
}


/**
 *  Open photo picker
 *
 *  @param button button
 */

- (void)mediaButtonPressed:(UIButton *)button {
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}




///* other lifecycle methods */
//- (void)viewWillLayoutSubviews {
//    [super viewWillLayoutSubviews];
//    
//    self.camera.view.frame = self.view.contentBounds;
//    
//    self.snapButton.center = self.view.contentCenter;
//    self.snapButton.bottom = self.view.height - 15;
//    
//    self.flashButton.center = self.view.contentCenter;
//    self.flashButton.top = 5.0f;
//    
//    self.switchButton.top = 5.0f;
//    self.switchButton.right = self.view.width - 5.0f;
//}
- (void)flashButtonPressed:(UIButton *)button {
    
    if(self.camera.flash == CameraFlashOff) {
        BOOL done = [self.camera updateFlashMode:CameraFlashOn];
        if(done) {
            self.flashButton.selected = YES;
            self.flashButton.tintColor = [UIColor yellowColor];
        }
    }
    else {
        BOOL done = [self.camera updateFlashMode:CameraFlashOff];
        if(done) {
            self.flashButton.selected = NO;
            self.flashButton.tintColor = [UIColor whiteColor];
        }
    }
}
- (void)crossButtonPressed:(UIButton *)button {
    self.tabBarController.selectedIndex=0;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)takeBarButtonClick:(id)sender {
   
}

- (IBAction)openBarButtonClick:(id)sender
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    [self presentViewController:imagePickerController animated:YES completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    imageSelecteed = [info valueForKey:UIImagePickerControllerOriginalImage];
    imageView.image = imageSelecteed;
    [[self navigationController] dismissViewControllerAnimated:YES completion:nil];
    
//    if(imageSelecteed != nil){
//        ImageCropViewController *controller = [[ImageCropViewController alloc] initWithImage:imageSelecteed];
//        controller.delegate = self;
//        controller.blurredBackground = YES;
//        [[self navigationController] pushViewController:controller animated:YES];
    
        
        
        TOCropViewController *cropViewController = [[TOCropViewController alloc] initWithImage:imageSelecteed];
        cropViewController.delegate = self;
        [self presentViewController:cropViewController animated:YES completion:nil];
    
    

}

- (void)cropViewController:(TOCropViewController *)cropViewController didCropToImage:(UIImage *)image withRect:(CGRect)cropRect angle:(NSInteger)angle
{
    // 'image' is the newly cropped version of the original image
    imageSelecteed = image;
    imageView.image = image;
    
    FilterVCUpload *obj =[self.storyboard instantiateViewControllerWithIdentifier:@"FilterVCUpload"];
    obj.imageSelected=imageSelecteed;
    //        obj.arrayColor=self.arraySelectcolor;
    [self.navigationController pushViewController:obj animated:YES];
}


- (IBAction)cropBarButtonClick:(id)sender {
   }

- (void)ImageCropViewController:(ImageCropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage{
    imageSelecteed = croppedImage;
    imageView.image = croppedImage;
    [[self navigationController] popViewControllerAnimated:NO];
//    if (imageSelecteed != nil){
//        UIImageWriteToSavedPhotosAlbum(imageSelecteed, self ,  @selector(thisImage:hasBeenSavedInPhotoAlbumWithError:usingContextInfo:), nil);
//    }
    
        FilterVCUpload *obj =[self.storyboard instantiateViewControllerWithIdentifier:@"FilterVCUpload"];
        obj.imageSelected=imageSelecteed;
//        obj.arrayColor=self.arraySelectcolor;
        [self.navigationController pushViewController:obj animated:YES];
    //[self performSegueWithIdentifier:@"upload" sender:self];
}

- (void)ImageCropViewControllerDidCancel:(ImageCropViewController *)controller{
    imageView.image = imageSelecteed;
    [[self navigationController] popViewControllerAnimated:NO];
    if (imageSelecteed != nil){
//        UIImageWriteToSavedPhotosAlbum(imageSelecteed, self ,  @selector(thisImage:hasBeenSavedInPhotoAlbumWithError:usingContextInfo:), nil);
    }
    POP
//    [self performSegueWithIdentifier:@"upload" sender:self];

}

- (void)thisImage:(UIImage *)image hasBeenSavedInPhotoAlbumWithError:(NSError *)error usingContextInfo:(void*)ctxInfo {
    if (error) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fail!"
//                                                        message:[NSString stringWithFormat:@"Saved with error %@", error.description]
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
        
    } else {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Succes!"
//                                                        message:@"Saved to camera roll"
//                                                       delegate:nil
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
      //  [alert show];
        
    }
}

- (IBAction)saveBarButtonClick:(id)sender {
//    if (imageSelecteed != nil){
//        UIImageWriteToSavedPhotosAlbum(imageSelecteed, self ,  @selector(thisImage:hasBeenSavedInPhotoAlbumWithError:usingContextInfo:), nil);
//    }
}

#pragma marka
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"upload"]) {
        
        
        UploadViewController *objUploadVC=segue.destinationViewController;
        objUploadVC.imageSelected=imageSelecteed;
    }
}
@end
