//
//  ChangePasswordVC.h
//  Guilt
//
//  Created by Gourav Sharma on 10/28/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingCollectionView.h"
#import "CommonViewController.h"
#import "Utility.h"

@interface ChangePasswordVC : UIViewController{
    
    IBOutlet CustomTextField *txtfieldConfirmPassword;
    IBOutlet CustomTextField *txtfieldNewPassword;
    IBOutlet CustomTextField *txtfieldCurrentpassword;
}
@property (strong, nonatomic) IBOutlet TPKeyboardAvoidingCollectionView *scrollview;

@end
