//
//  ChangePasswordVC.m
//  Guilt
//
//  Created by Gourav Sharma on 10/28/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "ChangePasswordVC.h"
#import "Utility.h"

@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC



- (void)viewDidLoad {
    [super viewDidLoad];
    INITIALIZE_LOADER
    
    [txtfieldConfirmPassword setValue:[UIColor darkGrayColor]
              forKeyPath:@"_placeholderLabel.textColor"];
    
    [txtfieldCurrentpassword setValue:[UIColor darkGrayColor]
             forKeyPath:@"_placeholderLabel.textColor"];
    
    [txtfieldNewPassword setValue:[UIColor darkGrayColor]
                           forKeyPath:@"_placeholderLabel.textColor"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)SaveClick:(id)sender {
    if ([self validate]) {
        [APPDELEGATE ShowProgress];
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,CHANGEPASSWORD_API];
        

        
       NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:txtfieldCurrentpassword.text forKey:@"old_password"];
        [dict setValue:txtfieldNewPassword.text forKey:@"new_password"];
        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"%@", responseObject);
            
            [APPDELEGATE removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                [APPDELEGATE showToast:[responseObject valueForKey:@"data"]];
            }
            
            else  if ([[responseObject valueForKey:@"status"]  intValue] == 0)
            {
                [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [APPDELEGATE removeProgress];
            
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];

        }];
        
        
//        [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
//           
//            
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            
//        }];
    }
}

- (IBAction)backClick:(id)sender {
    
    POP;
}

#pragma mark-
#pragma mark-TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:txtfieldCurrentpassword]) {
        [txtfieldNewPassword becomeFirstResponder];
    }
    else if ([textField isEqual:txtfieldNewPassword]) {
        [txtfieldConfirmPassword becomeFirstResponder];
    }
    else if ([textField isEqual:txtfieldConfirmPassword]) {
        [txtfieldConfirmPassword resignFirstResponder];
    }
    
    
    return  NO;
}

-(BOOL)validate
{
    if (txtfieldCurrentpassword.text.length==0) {
        [APPDELEGATE showAlert:@"" message:@"Please enter password"];
            return false;
    }
   
    else if (txtfieldNewPassword.text.length==0){
        [APPDELEGATE showAlert:@"" message:@"Please enter new password"];
        return false;
    }
    else if (![self Passwordvalidation]){
        
        return false;
    }
    else if (txtfieldConfirmPassword.text.length==0){
        [APPDELEGATE showAlert:@"" message:@"Please enter confirm password"];

        return false;
    }
    else if (![txtfieldConfirmPassword.text isEqualToString:txtfieldNewPassword.text]){
        [APPDELEGATE showAlert:@"" message:@"New password and confirm password must be same"];

        return false;
    }
    return true;
}






-(BOOL)Passwordvalidation
{
    if([txtfieldNewPassword.text length] <= 16 && [txtfieldNewPassword.text length] >= 8)
    {
        NSString *Pattern = @"(?=.*[@#$%])";
        if ([self string:txtfieldNewPassword.text matches:Pattern] )
        {
            NSLog(@"PW is valid");
            return YES;
        }
        
        else
        {
            [APPDELEGATE showAlert:@"" message:@"Password must contain one special charcter"];
            return NO;
        }
        
    }
    else
    {
        [APPDELEGATE showAlert:@"" message:@"Password should be 8 to 16 characters"];
        return NO;
        
    }
}


- (BOOL)string:(NSString *)text matches:(NSString *)pattern
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
    
    NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)];
    
    return matches.count > 0;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark Response Delegate

- (void)didReceivedresponse:(NSDictionary *)responsedict
{
    NSLog(@"%@", responsedict);
    
    [APPDELEGATE removeProgress];
    if ([[responsedict valueForKey:@"status"]  intValue] == 1)
    {
//        [APPDELEGATE showAlert:@"" message:[responsedict valueForKey:@"data"]];
        [APPDELEGATE showToast:[responsedict valueForKey:@"data"]];

        
    }
    
    else  if ([[responsedict valueForKey:@"status"]  intValue] == 0)
    {
        [APPDELEGATE showAlert:@"" message:[responsedict valueForKey:@"message"]];
    }
}

- (void)didFailedWithError:(NSError *)error
{
    [APPDELEGATE removeProgress];
    [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    
}


@end
