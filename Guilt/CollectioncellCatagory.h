//
//  CollectioncellCatagory.h
//  Guilt
//
//  Created by Gourav Sharma on 10/28/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectioncellCatagory : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *ivCheck;
@property (strong, nonatomic) IBOutlet UIImageView *ivProduct;

@end
