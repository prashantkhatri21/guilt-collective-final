//
//  ColorPickerVC.h
//  Guilt
//
//  Created by Prashant khatri on 03/11/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorPickerVC : UIViewController
{
    __weak IBOutlet UICollectionView *collectionviewPicker;
    
    IBOutlet UIView *viewPickewrColorwhite;
    __weak IBOutlet UIView *viewColorPicker;
}
@property(nonatomic,strong)id delegate;
-(void)selectedColor:(NSMutableArray*)arrayColors;

@end
