//
//  ColorPickerVC.m
//  Guilt
//
//  Created by Prashant khatri on 03/11/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "ColorPickerVC.h"
#import "NKOColorPickerView.h"
#import "Addmorecell.h"
#import "Utility.h"
#import "ModelColor.h"
@interface ColorPickerVC ()
{
    NKOColorPickerView *colorPickerView;
    NSMutableArray *arrayColor;
    NSMutableArray *arraySelectedColor;
}
@end

@implementation ColorPickerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    INITIALIZE_LOADER
    viewPickewrColorwhite.hidden =YES;
    arrayColor=[[NSMutableArray alloc]init];
    arraySelectedColor=[[NSMutableArray alloc]init];
    //Adds a shadow to sampleView
    viewColorPicker.layer.shadowOffset = CGSizeMake(1, 1);
    viewColorPicker.layer.shadowColor = [[UIColor blackColor] CGColor];
    viewColorPicker.layer.shadowRadius = 4.0f;
    viewColorPicker.layer.shadowOpacity = 0.80f;
    //    viewColorPicker.layer.shadowPath = [[UIBezierPath bezierPathWithRect:viewColorPicker.layer.bounds] CGPath];
    
    [self getColors];
    // Do any additional setup after loading the view.
}

#pragma mark--- API Methods

/**
 *  Get colors
 */
-(void)getColors
{
    [APPDELEGATE ShowProgress];
    NSString *url =[NSString stringWithFormat:@"%@%@%@%@",kBaseUrl_Live,COLORS_API,APITokenURL_Product,@"&offset=0&limit=10"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [APPDELEGATE removeProgress];
        [self parseData:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        [APPDELEGATE removeProgress];
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    }];
    
//    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//       
//        
//    }];
}
/**
 *  Parse data
 *
 *  @param responseObject responsem object
 */
-(void)parseData:(id)responseObject
{
    [arrayColor removeAllObjects];
    
    if ([responseObject valueForKey:@"colors"] !=nil)
    {
        NSLog(@"colors=%@",[responseObject valueForKey:@"colors"]);
        if ([[responseObject valueForKey:@"colors"] count]==0) {
            [APPDELEGATE showToast:@"No Colors Found"];
        }
        for (id dictProduct in [responseObject valueForKey:@"colors"]) {
            ModelColor *objModelProdct=[[ModelColor alloc]init];
            
            if ([dictProduct valueForKey:@"name"]!=nil) {
                objModelProdct.name=[dictProduct valueForKey:@"name"];
            }
            if ([dictProduct valueForKey:@"id"]!=nil) {
                objModelProdct.idcolor=[dictProduct valueForKey:@"id"];
                
            }
            
            [arrayColor addObject:objModelProdct];
        }
    }
    [collectionviewPicker reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark--- Click Actions
- (IBAction)clickDone:(id)sender {
    viewPickewrColorwhite.hidden =YES;
    [colorPickerView removeFromSuperview];
    
}
- (IBAction)firstButtonClick:(id)sender {
    UIButton *btn = sender;
    viewPickewrColorwhite.hidden =NO;
    
    //Color did change block declaration
    NKOColorPickerDidChangeColorBlock colorDidChangeBlock = ^(UIColor *color){
        btn.backgroundColor = color;
        //Your code handling a color change in the picker view.
    };
    
    colorPickerView = [[NKOColorPickerView alloc] initWithFrame:CGRectMake(10, 20, self.view.frame.size.width-10, self.view.frame.size.width-100) color:[UIColor blueColor] andDidChangeColorBlock:colorDidChangeBlock];
    
    //Add color picker to your view
    [self.view addSubview:colorPickerView];
}

- (IBAction)secondButtonClick:(id)sender {
    UIButton *btn = sender;
    viewPickewrColorwhite.hidden =NO;
    
    //Color did change block declaration
    NKOColorPickerDidChangeColorBlock colorDidChangeBlock = ^(UIColor *color){
        btn.backgroundColor = color;
        //Your code handling a color change in the picker view.
    };
    
    colorPickerView = [[NKOColorPickerView alloc] initWithFrame:CGRectMake(10, 20, self.view.frame.size.width-10, self.view.frame.size.width-100) color:[UIColor blueColor] andDidChangeColorBlock:colorDidChangeBlock];
    
    //Add color picker to your view
    [self.view addSubview:colorPickerView];
}
- (IBAction)thirdButtonClick:(id)sender {
    UIButton *btn = sender;
    viewPickewrColorwhite.hidden =NO;
    
    //Color did change block declaration
    NKOColorPickerDidChangeColorBlock colorDidChangeBlock = ^(UIColor *color){
        btn.backgroundColor = color;
        //Your code handling a color change in the picker view.
    };
    
    colorPickerView = [[NKOColorPickerView alloc] initWithFrame:CGRectMake(10, 20
                                                                           , self.view.frame.size.width-10, self.view.frame.size.width-100) color:[UIColor blueColor] andDidChangeColorBlock:colorDidChangeBlock];
    
    //Add color picker to your view
    [self.view addSubview:colorPickerView];
}
- (IBAction)fourthButtonClick:(id)sender {
    UIButton *btn = sender;
    viewPickewrColorwhite.hidden =NO;
    //Color did change block declaration
    NKOColorPickerDidChangeColorBlock colorDidChangeBlock = ^(UIColor *color){
        btn.backgroundColor = color;
        //Your code handling a color change in the picker view.
    };
    
    colorPickerView = [[NKOColorPickerView alloc] initWithFrame:CGRectMake(10, 20, self.view.frame.size.width-10, self.view.frame.size.width-100) color:[UIColor blueColor] andDidChangeColorBlock:colorDidChangeBlock];
    
    //Add color picker to your view
    [self.view addSubview:colorPickerView];
}
- (IBAction)addClick:(id)sender {
    [self.delegate selectedColor:arraySelectedColor];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)cancelClick:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark--- Collection View Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [arrayColor count];
}

- (NSInteger)numberOfSections
{
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"ColorCell";
    
    
    Addmorecell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.imageViewColor.image =[UIImage imageNamed:@"check_gray"];
    ModelColor *obj=[arrayColor objectAtIndex:indexPath.row];
    if (obj.isselected) {
        cell.imageViewColor.hidden=NO;
    }
    else{
        cell.imageViewColor.hidden=YES;
    }
    if ([obj.name isEqualToString:@"Orange"]) {
        cell.viewColor.backgroundColor=[UIColor orangeColor];
    }
    else if ([obj.name isEqualToString:@"Brown"])
    {
        cell.viewColor.backgroundColor=[UIColor brownColor];
        
    }
    else if ([obj.name isEqualToString:@"Yellow"])
    {
        cell.viewColor.backgroundColor=[UIColor yellowColor];
        
    }
    else if ([obj.name isEqualToString:@"Red"])
    {
        cell.viewColor.backgroundColor=[UIColor redColor];
        
    }
    else if ([obj.name isEqualToString:@"Purple"])
    {
        cell.viewColor.backgroundColor=[UIColor purpleColor];
        
    }
    else if ([obj.name isEqualToString:@"Blue"])
    {
        cell.viewColor.backgroundColor=[UIColor blueColor];
        
    }
    else if([obj.name isEqualToString:@"Green"])
    {
        cell.viewColor.backgroundColor=[UIColor greenColor];
        
    }
    else if([obj.name isEqualToString:@"Gray"])
    {
        cell.viewColor.backgroundColor=[UIColor grayColor];
        
    }
    else if([obj.name isEqualToString:@"White"])
    {
        cell.viewColor.backgroundColor=[UIColor whiteColor];
        
    }
    else if ([obj.name isEqualToString:@"Black"])
    {
        cell.viewColor.backgroundColor=[UIColor blackColor];
        
    }
    else if([obj.name isEqualToString:@"Pink"])
    {
        //Pink color 255-192-203
        cell.viewColor.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:203.0/255.0 alpha:1.0];
        
    }
    else if([obj.name isEqualToString:@"Gold"])
    {
        //Golden color 255-215-0
        cell.viewColor.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:0.0/255.0 alpha:1.0];
        
    }
    else if([obj.name isEqualToString:@"Silver"])
    {
        //Silver color 192-192-192
        cell.viewColor.backgroundColor=[UIColor colorWithRed:192.0/255.0 green:192.0/255.0 blue:192.0/255.0 alpha:1.0];
    }
    else if([obj.name isEqualToString:@"Beige"])
    {
        //Silver color 192-192-192
        cell.viewColor.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:220.0/255.0 alpha:1.0];
    }
    else if([obj.name isEqualToString:@"Rose"])
    {
        //Silver color 192-192-192
        cell.viewColor.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:102.0/255.0 blue:204.0/255.0 alpha:1.0];
    }
    
    cell.clipsToBounds=YES;
    cell.layer.cornerRadius =cell.viewColor.frame.size.width/2;
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    Addmorecell *cell=(Addmorecell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    ModelColor *obj=[arrayColor objectAtIndex:indexPath.row];
    if (obj.isselected) {
        obj.isselected=NO;
        cell.imageViewColor.hidden=YES;
        [arraySelectedColor removeObject:obj];
        
    }
    else{
        if ([arraySelectedColor count]<4) {
            obj.isselected=YES;
            cell.imageViewColor.hidden=NO;
            [arraySelectedColor addObject:obj];
        }
        else{
            [APPDELEGATE showToast:@"you can add only four color"];
        }
        
    }
    [arrayColor replaceObjectAtIndex:indexPath.row withObject:obj];
    
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(collectionView.frame.size.width /4-10, collectionView.frame.size.width /4-10 );
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
