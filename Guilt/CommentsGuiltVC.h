//
//  CommentsGuiltVC.h
//  Guilt
//
//  Created by Jatin on 15/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface CommentsGuiltVC : UIViewController
{
    __weak IBOutlet UITableView *objTableview;
    
    __weak IBOutlet CustomTextField *txtcomment;
    IBOutlet NSLayoutConstraint *constextfieldFrame;

    __weak IBOutlet CustomLabel *lblComments;
}
@property(nonatomic,strong)NSString *guiltID;
@end
