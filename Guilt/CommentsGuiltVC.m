//
//  CommentsGuiltVC.m
//  Guilt
//
//  Created by Jatin on 15/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "CommentsGuiltVC.h"
#import "ModelGuilt.h"
#import "TableCellComments.h"
#import "UIImageView+WebCache.h"
@interface CommentsGuiltVC ()
{
    NSMutableArray *arrayComments;
    MBProgressHUD *HUD,*HUDToast;
    NSDateFormatter *dateFormatter;
}
@end

@implementation CommentsGuiltVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeMBProgres:self.view];
    self.tabBarController.tabBar.hidden=YES;
    [self registerForKeyboardNotifications];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];

    //2015-12-16 19:36:05

    [dateFormatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];

    objTableview.rowHeight = UITableViewAutomaticDimension;
    objTableview.estimatedRowHeight = 44.0 ;// set this to whatever your "average" cell height is; it doesn't need to be very accurate
    arrayComments=[[NSMutableArray alloc]init];
    
    //    arrayComments =[[NSMutableArray alloc]initWithObjects:@"hfiygiflglADIFGIadfgifaugiudfgiuagfugafg",@"hufhdsufhu9sdhfuds",@"bayfhasufhufhiuffhudhfusdhfuhsdufhuhfuha",@"ndiufhdufhusdhfsudyfusdhfuhsdhfsfhfoishdfoihDF",@"GASDGAIDGAIGSD www.google.com",@"hfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafg" ,@"hfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafg   hfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafg",nil];
    [self callAPI];
    // Do any additional setup after loading the view.
}

/*!
 @brief this method is calling when View will disappear
 
 @discussion Un Register the notification
 
 @param nothing
 
 @return nothing
 */
- (void)viewWillDisappear:(BOOL)animated
{
    
    [self deregisterFromKeyboardNotifications];
    
    [super viewWillDisappear:animated];
    
}

#pragma mark -----Keyboard Methods----------
/*!
 @brief UnRegister the keyboard Notification
 
 @discussion Show and Hide Notifications will be unregistered
 
 @param nothing
 
 @return nothing
 */
- (void)deregisterFromKeyboardNotifications {
    
    // remove observer for keyboardWasShown
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    // remove observer for keyboardWillBeHidden
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

/*!
 @brief Register the keyboard Notification
 
 @discussion Show and Hide Notifications will be registered
 
 @param nothing
 
 @return nothing
 */
- (void)registerForKeyboardNotifications
{
    
    // add observer for keyboardWasShown
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    // add observer for keyboardWillBeHidden
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
}

/*!
 @brief Handle UIKeyboardDidShowNotification keyboard Notification
 
 @discussion Show and Hide Notifications
 
 @param notification - type of notification received
 
 @return nothing
 */
-(void)keyboardWasShown:(NSNotification *)notification
{
    
    NSDictionary* info = [notification userInfo];
    
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //    [viewTextcomment setFrame:CGRectMake(0, self.view.frame.size.height- keyboardSize.height-viewTextcomment.frame.size.height, viewTextcomment.frame.size.width, viewTextcomment.frame.size.height)];
    constextfieldFrame.constant=keyboardSize.height;
    //  show keyboard
    
    //    [UIView animateWithDuration:0.5
    //                          delay:0.0
    //                        options:UIViewAnimationOptionLayoutSubviews
    //                     animations:^{
    //
    //                         [viewTextcomment setFrame:CGRectMake(0, self.view.frame.size.height- keyboardSize.height-viewTextcomment.frame.size.height, viewTextcomment.frame.size.width, viewTextcomment.frame.size.height)];
    //                     }
    //                     completion:nil];
    
    
    // Make framing when keyboard will shown
    
    
    
}


/*!
 @brief Handle UIKeyboardWillHideNotification keyboard Notification
 
 @discussion Show and Hide Notifications
 
 @param notification - type of notification received
 
 @return nothing
 */
-(void)keyboardWillBeHidden:(NSNotification *)notification
{
    //[objTxtComment resignFirstResponder];
    //  show keyboard
    NSDictionary* info = [notification userInfo];
    
    //    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    constextfieldFrame.constant=0;
    //     [viewTextcomment setFrame:CGRectMake(0, self.view.frame.size.height- viewTextcomment.frame.size.height, viewTextcomment.frame.size.width, viewTextcomment.frame.size.height)];
    //    [UIView animateWithDuration:0.5
    //                          delay:0.0
    //                        options:UIViewAnimationOptionLayoutSubviews
    //                     animations:^{
    //
    //                         [viewTextcomment setFrame:CGRectMake(0, self.view.frame.size.height- keyboardSize.height-viewTextcomment.frame.size.height, viewTextcomment.frame.size.width, viewTextcomment.frame.size.height)];
    //                     }
    //                     completion:nil];
    
}


-(void)callAPI{
    [self ShowProgress];
    
    
    NSMutableDictionary *dict;
    NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,COMMENTS_API];
    dict=[[NSMutableDictionary alloc]init];
    [dict setValue:TOKEN forKey:@"token"];
    [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
    //hardcode
    [dict setValue:self.guiltID forKey:@"guilt_id"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager setRequestSerializer:requestSerializer];
    [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self removeProgress];
        [arrayComments removeAllObjects];
        if ([[responseObject valueForKey:@"status"]  intValue] == 1)
        {
            //-----Not found Guilt--//
            
            if ([responseObject valueForKey:@"data"]!=nil) {
                [arrayComments removeAllObjects];
                for (id DictfoundGuilt in [responseObject valueForKey:@"data"]) {
                    ModelGuilt *objGuilt=[[ModelGuilt alloc]init];
                    objGuilt.guilt_id=[DictfoundGuilt valueForKey:@"guilt_id"];
                    objGuilt.comment_ID=[DictfoundGuilt valueForKey:@"id"];
                    objGuilt.comment=[DictfoundGuilt valueForKey:@"comment"];
                    objGuilt.user_id=[DictfoundGuilt valueForKey:@"user_id"];
                    if([DictfoundGuilt valueForKey:@"created"]!=nil)
                    {
                        NSDate *date=[dateFormatter dateFromString:[DictfoundGuilt valueForKey:@"created"]];
                        objGuilt.comment_dateString=[dateFormatter stringFromDate:date] ;
                    }
                    
                    if ([DictfoundGuilt valueForKey:@"user"]!=nil) {
                        NSDictionary *dictUser=[DictfoundGuilt valueForKey:@"user"];
                        //------------------------User data--------------------------//
                        
                        objGuilt.objProfile=[[ModelProfile alloc]init];
                        objGuilt.objProfile.fullname= [NSString stringWithFormat:@"%@",[dictUser valueForKey:@"first_name"]];
                        
                        objGuilt.objProfile.email= [NSString stringWithFormat:@"%@",[dictUser valueForKey:@"email"]];
                        if ([dictUser valueForKey:@"description"]!=nil) {
                            objGuilt.objProfile.description_profile= [NSString stringWithFormat:@"%@",[dictUser valueForKey:@"description"]];
                            
                        }
                        else{
                            objGuilt.objProfile.description_profile=@"";
                        }
                        
                        
                        objGuilt.objProfile.profile_pic_url= [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[dictUser valueForKey:@"profile_pic"]]];
                        objGuilt.objProfile.age= [[NSString stringWithFormat:@"%@",[dictUser valueForKey:@"age"]] intValue];
                        if ([dictUser valueForKey:@"gender"]!=nil) {
                            NSString *str=[[dictUser valueForKey:@"gender"] lowercaseString];
                            if ([str isEqualToString:@"male"]) {
                                objGuilt.objProfile.ismale=YES;
                            }
                            else
                            {
                                objGuilt.objProfile.ismale=NO;
                            }
                            
                            
                        }

                        
                        
                    }
                    
                   
                    
                    [arrayComments addObject:objGuilt];
                    
                }
            }
            
        }
        else
        {
            if ([responseObject valueForKey:@"data"]!=nil) {
                if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                    
                }
                else{
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];

                }
            }
            
        }
        [objTableview reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self removeProgress];
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        
        
    }];
}
#pragma mark Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (arrayComments.count==0) {
        lblComments.hidden=NO;
        objTableview.hidden=YES;
    }
    else{
        lblComments.hidden=YES;
        objTableview.hidden=NO;
    }
    return arrayComments.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    TableCellComments *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[TableCellComments alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    ModelGuilt *objGuilt=[arrayComments objectAtIndex:indexPath.row];
    cell.textviewComments.text=objGuilt.comment;
    [cell.ivProfile sd_setImageWithURL:objGuilt.objProfile.profile_pic_url placeholderImage:[UIImage imageNamed:@"user"]];
    cell.lblName.text=objGuilt.objProfile.fullname;
    cell.lblDate.text=objGuilt.comment_dateString;
    cell.textviewComments.editable= NO;
    cell.textviewComments.dataDetectorTypes = UIDataDetectorTypeAll;
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded ];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backclick:(id)sender {
    POP
}
- (IBAction)sendClick:(id)sender {
    if ([txtcomment.text length]==0) {
        [self showToast:@"Please add comment"];
        }
    else{
        [self ShowProgress];
        NSMutableDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,ADD_COMMENTS_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        //hardcode
        
        [dict setValue:self.guiltID forKey:@"guilt_id"];
        [dict setValue:txtcomment.text forKey:@"comment"];

        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                [txtcomment resignFirstResponder];
                txtcomment.text=@"";
//                [self showToast:[responseObject valueForKey:@"message"]];
                [self callAPI];
                [NS_NOTIFICATION postNotificationName:NOTIFICATION_UPDATEFEEDS object:nil];

            }
            else
            {
                [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];

        
    }
}

-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}

#pragma mark-
#pragma mark-TextFieldDelegate
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
