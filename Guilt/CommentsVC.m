//
//  CommentsVC.m
//  Guilt
//
//  Created by Gourav Sharma on 10/28/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "CommentsVC.h"
#import "Utility.h"
#import "ModelGuilt.h"
@interface CommentsVC ()
{
    NSMutableArray *arrayComments;
    MBProgressHUD *HUD,*HUDToast;

}
@end

@implementation CommentsVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.objTableview.rowHeight = UITableViewAutomaticDimension;
    self.objTableview.estimatedRowHeight = 44.0 ;// set this to whatever your "average" cell height is; it doesn't need to be very accurate
    arrayComments=[[NSMutableArray alloc]init];

//    arrayComments =[[NSMutableArray alloc]initWithObjects:@"hfiygiflglADIFGIadfgifaugiudfgiuagfugafg",@"hufhdsufhu9sdhfuds",@"bayfhasufhufhiuffhudhfusdhfuhsdufhuhfuha",@"ndiufhdufhusdhfsudyfusdhfuhsdhfsfhfoishdfoihDF",@"GASDGAIDGAIGSD www.google.com",@"hfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafg" ,@"hfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafg   hfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafghfiygiflglADIFGIadfgifaugiudfgiuagfugafg",nil];
    [callAPI];
    
    // Do any additional setup after loading the view.
}

-(void)callAPI{
    [self ShowProgress];
    
    
    NSDictionary *dict;
    NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,COMMENTS_API];
    dict=[[NSMutableDictionary alloc]init];
    [dict setValue:TOKEN forKey:@"token"];
    [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
    //hardcode
    [dict setValue:@"6" forKey:@"guilt_id"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager setRequestSerializer:requestSerializer];
    [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self removeProgress];
        
        if ([[responseObject valueForKey:@"status"]  intValue] == 1)
        {
            //-----Not found Guilt--//
            [arrayComments removeAllObjects];
            if ([dataDict valueForKey:@"data"]!=nil) {
                [arrayNotfoundGuilt removeAllObjects];
                for (id DictfoundGuilt in [dataDict valueForKey:@"data"]) {
                    ModelGuilt *objGuilt=[[ModelGuilt alloc]init];
                    objGuilt.guilt_id=[DictfoundGuilt valueForKey:@"guilt_id"];
                    objGuilt.comment_ID=[DictfoundGuilt valueForKey:@"id"];
                    objGuilt.comment=[DictfoundGuilt valueForKey:@"comment"];
                     objGuilt.user_id=[DictfoundGuilt valueForKey:@"user_id"];


                    [arrayComments addObject:objGuilt];
                    
                }
            }
            [self.objTableview reloaddata];
        }
        else
        {
            [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self removeProgress];
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        
        
    }];
}
#pragma mark Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayComments.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    TableCellComments *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[TableCellComments alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textviewComments.editable= NO;
    cell.textviewComments.dataDetectorTypes = UIDataDetectorTypeAll;
    cell.textviewComments.text =[arrayComments objectAtIndex:indexPath.row];
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded ];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    // check here, if it is one of the cells, that needs to be resized
//    // to the size of the contained UITextView
//   
//        return [self textViewHeightForRowAtIndexPath:indexPath]+34;
//    
//}

//- (CGFloat)textViewHeightForRowAtIndexPath: (NSIndexPath*)indexPath {
//    UITextView *calculationView ;
//    CGFloat textViewWidth = 300;
//    if (!calculationView.attributedText) {
//        // This will be needed on load, when the text view is not inited yet
//        
//        calculationView = [[UITextView alloc] init];
//        calculationView.attributedText = [[NSAttributedString alloc ]initWithString:[arrayComments objectAtIndex:indexPath.row] ];// get the text from your datasource add attributes and insert here
//        textViewWidth = 300; // Insert the width of your UITextViews or include calculations to set it accordingly
//    }
//    CGSize size = [calculationView sizeThatFits:CGSizeMake(textViewWidth, FLT_MAX)];
//    NSLog(@"Size height =%f",size.height);
//    
//    return size.height;
//}


#pragma mark Click Action
- (IBAction)backClick:(id)sender {
    POP;
}


#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
