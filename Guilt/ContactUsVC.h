//
//  ContactUsVC.h
//  Guilt
//
//  Created by Gourav Sharma on 10/29/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface ContactUsVC : UIViewController
{
    
    IBOutlet CustomTextField *txtSubject;
    IBOutlet CustomTextView *textviewDescription;
}
@end
