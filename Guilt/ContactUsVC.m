//
//  ContactUsVC.m
//  Guilt
//
//  Created by Gourav Sharma on 10/29/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "ContactUsVC.h"
#import "Utility.h"
@interface ContactUsVC ()
{
    MBProgressHUD *HUD,*HUDToast;

}
@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    textviewDescription.text=@"";
    textviewDescription.placeholderText =@"TELL US HOW WE CAN HELP HERE";
    
    txtSubject.delegate=self;
    textviewDescription.delegate=self;
    [self initializeMBProgres:self.view];

    [txtSubject setValue:[UIColor blackColor]
                           forKeyPath:@"_placeholderLabel.textColor"];
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backTap:(id)sender {
    POP;
}

#pragma mark-TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:txtSubject]) {
        [textviewDescription becomeFirstResponder];
    }
   
    
    
    return  NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}

- (IBAction)doneTap:(id)sender {
    
    if ([txtSubject.text length]==0) {
        [APPDELEGATE showAlert:@"" message:@"Please enter subject"
         ];
    }
    else if ([textviewDescription.text length]==0)
    {
        [APPDELEGATE showAlert:@"" message:@"Please enter description"
         ];
    }
    else{
        [self ShowProgress];
        NSMutableDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,SUBMIT_QUERY_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:txtSubject.text forKey:@"subject"];
        [dict setValue:textviewDescription.text forKey:@"description"];

        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                
                [APPDELEGATE showAlert:@"" message:@"submit successfully"];

            }
            else
            {
                if ([responseObject valueForKey:@"data"]!=nil) {
                    if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                        
                    }
                    else{
                        [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                        
                    }
                }
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];
        

    }
}


#pragma mark MBProgress
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}

#pragma mark- text View delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
