//
//  FeedVC.h
//  Guilt
//
//  Created by Gourav Sharma on 10/29/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "Utility.h"
@interface FeedVC : UIViewController
{
    
    __weak IBOutlet UIImageView *imageview;
    __weak IBOutlet UIButton *btnCross;
    __weak IBOutlet UIScrollView *scrollview;
    __weak IBOutlet CustomLabel *nofeedfound;
    IBOutlet UITableView *tableFeeds;
}
@end
