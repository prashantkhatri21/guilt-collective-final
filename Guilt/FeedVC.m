//
//  FeedVC.m
//  Guilt
//
//  Created by Gourav Sharma on 10/29/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "FeedVC.h"
#import "TableCellComments.h"
#import "Utility.h"
#import "CommentsGuiltVC.h"
#import "ModelGuilt.h"
#import "ModelTag.h"
#import "UIImageView+WebCache.h"
@interface FeedVC ()
{
    NSMutableArray *arrayFeeds;
    MBProgressHUD *HUD,*HUDToast;
    //877623624245-5quo1h9gisukq566e5amtfhkceb1t4mo.apps.googleusercontent.com
    
}
@end

@implementation FeedVC

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Set Title
        UIImage *musicImage = [UIImage imageNamed:@"feed_blk"];
        UIImage *musicImageSel = [UIImage imageNamed:@"feed_wht"];
        
        musicImage = [musicImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        musicImageSel = [musicImageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        self.navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"FEED" image:musicImage selectedImage:musicImageSel];
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor blackColor], NSForegroundColorAttributeName,[UIFont fontWithName:@"OSWALD" size:10], UITextAttributeFont,
                                                           nil] forState:UIControlStateNormal];
        
        
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor whiteColor], NSForegroundColorAttributeName,
                                                           nil] forState:UIControlStateSelected];
        [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"black"]];
        
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
   
    
    
   
    
    
    
    [NS_NOTIFICATION addObserver:self selector:@selector(callApi) name:NOTIFICATION_UPDATEFEEDS object:nil];
    
    arrayFeeds=[[NSMutableArray alloc]init];
    [self initializeMBProgres:self.view];
    arrayFeeds =[[NSMutableArray alloc]init];
    
    //    tableFeeds.rowHeight = UITableViewAutomaticDimension;
    //    tableFeeds.estimatedRowHeight = 44.0 ;// set this to whatever your "average" cell height is; it doesn't need to be very accurate
    
    
    
    // Do any additional setup after loading the view.
}
-(void)callApi
{
    [self ShowProgress];
    NSMutableDictionary *dict;
    NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,FEED_API];
    dict=[[NSMutableDictionary alloc]init];
    [dict setValue:TOKEN forKey:@"token"];
    //hardcode
    [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager setRequestSerializer:requestSerializer];
    [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self removeProgress];
        [arrayFeeds removeAllObjects];
        if ([[responseObject valueForKey:@"status"]  intValue] == 1)
        {
            
            
            if ([responseObject valueForKey:@"data"]!=nil  && ![[responseObject valueForKey:@"data"] isKindOfClass:[NSNull class]]) {
                for (id DictfoundGuilt in [responseObject valueForKey:@"data"]) {
                    ModelGuilt *objGuilt=[[ModelGuilt alloc]init];
                    objGuilt.guilt_id=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"guilt_id"]];
                    objGuilt.descriptionGuilt=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"guilt_description"]];
                    objGuilt.user_id=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"user_id"]];
                    if ([[DictfoundGuilt valueForKey:@"guilt_found"]  intValue] == 1)
                    {
                        objGuilt.found=YES;
                    }
                    else{
                        objGuilt.found=NO;
                        
                    }
                    
                    objGuilt.guilt_share_count=[[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"guilt_share"]]  intValue];
                    
                    if ([DictfoundGuilt valueForKey:@"guilt_gender"]!=nil) {
                        NSString *str=[[DictfoundGuilt valueForKey:@"guilt_gender"] lowercaseString];
                        if ([str isEqualToString:@"male"]||[str isEqualToString:@"men"]) {
                            objGuilt.ismale=YES;
                        }
                        else
                        {
                            objGuilt.ismale=NO;
                        }
                        
                        
                    }
                    objGuilt.image_url= [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[DictfoundGuilt valueForKey:@"guilt_image"]]];
                    objGuilt.found_link=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"guiltfound_link"]];
                    objGuilt.guilt_comment_count=[[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"comment_count"]]  intValue];
                    
                    if ([[DictfoundGuilt valueForKey:@"user_like"]  intValue] == 1)
                    {
                        objGuilt.isUserLike=YES;
                    }
                    else{
                        objGuilt.isUserLike=NO;
                        
                    }
                    
                    //------------------------User data--------------------------//
                    
                    objGuilt.objProfile=[[ModelProfile alloc]init];
                    if ([DictfoundGuilt valueForKey:@"guilt_user_detail"]!=nil) {
                        NSDictionary *dictUerdetail=[DictfoundGuilt valueForKey:@"guilt_user_detail"];
                    
                    objGuilt.objProfile.fullname= [NSString stringWithFormat:@"%@",[dictUerdetail valueForKey:@"first_name"]];
                    objGuilt.objProfile.email= [NSString stringWithFormat:@"%@",[dictUerdetail valueForKey:@"email"]];
                    if ([dictUerdetail valueForKey:@"description"]!=nil) {
                        objGuilt.objProfile.description_profile= [NSString stringWithFormat:@"%@",[dictUerdetail valueForKey:@"description"]];
                        
                    }
                    else{
                        objGuilt.objProfile.description_profile=@"";
                    }
                    
                    
                    objGuilt.objProfile.profile_pic_url= [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[dictUerdetail valueForKey:@"profile_pic"]]];
                    objGuilt.objProfile.age= [[NSString stringWithFormat:@"%@",[dictUerdetail valueForKey:@"age"]] intValue];
                    if ([DictfoundGuilt valueForKey:@"gender"]!=nil) {
                        NSString *str=[[dictUerdetail valueForKey:@"gender"] lowercaseString];
                        if ([str isEqualToString:@"male"]) {
                            objGuilt.objProfile.ismale=YES;
                        }
                        else
                        {
                            objGuilt.objProfile.ismale=NO;
                        }
                        
                        
                    }
                    }
                    
                    //Tag array
                    if ([DictfoundGuilt valueForKey:@"guilt_tag"]!=nil) {
                        NSArray *arrayTag=[DictfoundGuilt valueForKey:@"guilt_tag"];
                        objGuilt.arrayTag=[[NSMutableArray alloc]init];
                        for (id dictTag in arrayTag) {
                            ModelTag *objTag=[[ModelTag alloc]init];
                            if ([dictTag valueForKey:@"tag_id"]!=nil) {
                                
                                objTag.tag_id=[dictTag valueForKey:@"tag_id"];
                            }
                            if ([dictTag valueForKey:@"guilt_id"]!=nil) {
                                
                                objTag.guilt_id=[dictTag valueForKey:@"guilt_id"];
                            }
                            if ([dictTag valueForKey:@"tag"]!=nil) {
                                
                                objTag.tag=[dictTag valueForKey:@"tag"];
                            }
                            if ([dictTag valueForKey:@"type"]!=nil) {
                                
                                objTag.type=[dictTag valueForKey:@"type"];
                            }
                            [objGuilt.arrayTag addObject:objTag ];
                        }
                    }
                    
                    
                    //Color array
                    if ([DictfoundGuilt valueForKey:@"guilt_color"]!=nil) {
                        NSArray *arrayColor=[DictfoundGuilt valueForKey:@"guilt_color"];
                        objGuilt.arrayColor=[[NSMutableArray alloc]init];
                        for (id dictTag in arrayColor) {
                            ModelColor *objTag=[[ModelColor alloc]init];
                            if ([dictTag valueForKey:@"id"]!=nil) {
                                
                                objTag.idcolor=[dictTag valueForKey:@"id"];
                            }
                            if ([dictTag valueForKey:@"color"]!=nil) {
                                
                                objTag.name=[dictTag valueForKey:@"color"];
                            }
                            if ([dictTag valueForKey:@"guilt_id"]!=nil) {
                                
                                objTag.guiltid=[dictTag valueForKey:@"guilt_id"];
                            }
                            
                            [objGuilt.arrayColor addObject:objTag ];
                        }
                    }
                    
                    
                    [arrayFeeds addObject:objGuilt];
                    
                    
                }
            }
            
        }
        else
        {
            if ([responseObject valueForKey:@"data"]!=nil) {
                if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                    
                }
                else{
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                    
                }
            }
            
        }
        [tableFeeds reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self removeProgress];
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        
        
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden=NO;
    [self callApi];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)filterClick:(id)sender {
}
- (IBAction)commentsClick:(id)sender {
    UIButton *btn=sender;
    CommentsGuiltVC *objCommentVC=[self.storyboard instantiateViewControllerWithIdentifier:@"CommentsGuiltVC"];
    ModelGuilt *objGuilt= [arrayFeeds objectAtIndex:btn.tag];
    //hardcode
    objCommentVC.guiltID=objGuilt.guilt_id;
    [self.navigationController pushViewController:objCommentVC animated:YES];
    
    
}
- (IBAction)shareClick:(id)sender {
    
    UIButton *btn=sender;
    ModelGuilt *objGuilt=[arrayFeeds objectAtIndex:btn.tag];
    
    NSIndexPath *index=[NSIndexPath indexPathForRow:btn.tag inSection:0];
    TableCellComments *cell =[tableFeeds cellForRowAtIndexPath:index];
    NSString * message =@"Guilt";
    NSString * description =objGuilt.descriptionGuilt;
    UIImage * image ;
    if (cell.ivFeeds.image!=nil) {
        image = cell.ivFeeds.image;
    }
    NSArray * shareItems;
    if (image) {
        shareItems = @[message, image,description];
        
    }
    else{
        shareItems = @[message,description];
        
    }
    
    UIActivityViewController * avc = [[UIActivityViewController alloc] initWithActivityItems:shareItems applicationActivities:nil];
    [self presentViewController:avc animated:YES completion:nil];
    
    
    avc.completionWithItemsHandler = ^(NSString * __nullable activityType, BOOL completed, NSArray * __nullable returnedItems, NSError * __nullable activityError) {
        if (completed ) {
            [self ShowProgress];
            NSMutableDictionary *dict;
            NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,SHARE_GUILT_API];
            dict=[[NSMutableDictionary alloc]init];
            [dict setValue:TOKEN forKey:@"token"];
            [dict setValue:objGuilt.guilt_id forKey:@"guilt_id"];
            [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
            
            
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
            
            [manager setRequestSerializer:requestSerializer];
            [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [self removeProgress];
                if ([[responseObject valueForKey:@"status"]  intValue] == 1)
                {
                    
                    if ([responseObject valueForKey:@"message"]!=nil) {
                        if ([[responseObject valueForKey:@"message"] isEqualToString:@"success"]) {
                            [self showToast:@"Share successfully"];
                            int sharecount= objGuilt.guilt_share_count;
                            sharecount++;
                            objGuilt.guilt_share_count=sharecount;
                            [arrayFeeds replaceObjectAtIndex:btn.tag withObject:objGuilt];
                            [tableFeeds reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:btn.tag inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
                            
                        }
                        else{
                            [self showToast:[responseObject valueForKey:@"message"]];
                        }
                    }
                    
                    
                }
                else
                {
                    if ([responseObject valueForKey:@"message"]!=nil) {
                        if ([responseObject valueForKey:@"message"]!=nil) {
                            [self showToast:[responseObject valueForKey:@"message"]];
                            
                        }
                        else{
                            if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                                
                            }
                            else{
                                [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                                
                            }
                        }
                        
                    }
                    
                }
                
                
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [self removeProgress];
                [APPDELEGATE showAlert:@"" message:error.localizedDescription];
                
                
            }];
            
        }
        
    };
}
- (IBAction)crossClick:(id)sender {
    btnCross.hidden = YES;
    scrollview.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        scrollview.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        scrollview.hidden = YES;
        scrollview.zoomScale=1.0;
    }];
}
- (IBAction)downloadClick:(id)sender {
}
- (IBAction)filterClickcell:(id)sender {
}

- (IBAction)backClick:(id)sender {
    
    POP;
}
- (IBAction)likeClickGuilt:(id)sender {
    [self ShowProgress];
    UIButton *btn=sender;
    ModelGuilt *objGuilt=[arrayFeeds objectAtIndex:btn.tag];
    NSMutableDictionary *dict;
    NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,LIKE_GUILT_API];
    dict=[[NSMutableDictionary alloc]init];
    [dict setValue:TOKEN forKey:@"token"];
    [dict setValue:objGuilt.guilt_id forKey:@"guilt_id"];
    [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager setRequestSerializer:requestSerializer];
    [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self removeProgress];
        if ([[responseObject valueForKey:@"status"]  intValue] == 1)
        {
            
            if ([responseObject valueForKey:@"message"]!=nil) {
                if ([[responseObject valueForKey:@"message"] isEqualToString:@"success"]) {
//                    [self showToast:[responseObject valueForKey:@"message"]];
                    TableCellComments *cell =[tableFeeds cellForRowAtIndexPath:[NSIndexPath indexPathForRow:btn.tag inSection:0]];
                    [cell.btnLike setBackgroundImage:[UIImage imageNamed:@"heart_selected_heart"] forState:UIControlStateNormal];
                    objGuilt.isUserLike=YES;
                    
                    
                }
                else if ([[responseObject valueForKey:@"message"] isEqualToString:@"unlike success"]){
//                    [self showToast:[responseObject valueForKey:@"message"]];
                    
                    TableCellComments *cell =[tableFeeds cellForRowAtIndexPath:[NSIndexPath indexPathForRow:btn.tag inSection:0]];
                    [cell.btnLike setBackgroundImage:[UIImage imageNamed:@"heart_selected"] forState:UIControlStateNormal];
                    objGuilt.isUserLike=NO;
                    
                }
            }
            [arrayFeeds replaceObjectAtIndex:btn.tag withObject:objGuilt];
            
            
            
        }
        else
        {
            if ([responseObject valueForKey:@"data"]!=nil) {
                if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                    
                }
                else{
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                    
                }
            }
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self removeProgress];
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        
        
    }];
    
}

#pragma mark Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (arrayFeeds.count ==0) {
        nofeedfound.hidden=NO;
        tableFeeds.hidden=YES;
    }
    else {
        nofeedfound.hidden=YES;
        tableFeeds.hidden=NO;
    }
    return arrayFeeds.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    TableCellComments *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[TableCellComments alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    ModelGuilt *objguilt=[arrayFeeds objectAtIndex:indexPath.row];
    [cell.ivFeeds sd_setImageWithURL:objguilt.image_url placeholderImage:[UIImage imageNamed:@"placeholder"]];
    cell.textviewComments.text=objguilt.descriptionGuilt;
    cell.lblName.text=objguilt.objProfile.fullname;
    cell.ivFeeds.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    
    tapGesture1.numberOfTapsRequired = 1;
    
    [tapGesture1 setDelegate:self];
    
    [cell.ivFeeds addGestureRecognizer:tapGesture1];
    cell.ivFeeds.tag=indexPath.row;
//    cell.tagsControl.backgroundColor=[UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
    if (objguilt.isUserLike) {
        //cell.bt
        [cell.btnLike setBackgroundImage:[UIImage imageNamed:@"heart_selected_heart"] forState:UIControlStateNormal];
    }
    else{
        [cell.btnLike setBackgroundImage:[UIImage imageNamed:@"heart_selected"] forState:UIControlStateNormal];
    }
    
    if (objguilt.ismale) {
        //cell.bt
        [cell.btnGender setBackgroundImage:[UIImage imageNamed:@"male"] forState:UIControlStateNormal];
    }
    else{
        [cell.btnGender setBackgroundImage:[UIImage imageNamed:@"female"] forState:UIControlStateNormal];
        
    }
    
    
    if (objguilt.found) {
        //cell.bt
        cell.lblDress.text = @"GUILT FOUND";
    }
    else{
        cell.lblDress.text = @"GUILT NOT FOUND";
        
    }
    
    [cell.ivProfile sd_setImageWithURL:objguilt.objProfile.profile_pic_url placeholderImage:[UIImage imageNamed:@"user.png"]];
    [cell.tagsControl.tags removeAllObjects];
    for (ModelTag *objtag in objguilt.arrayTag) {
        [cell.tagsControl addTag:[NSString stringWithFormat:@"#%@",objtag.tag]];
    }
    
    for (int i=0; i<[objguilt.arrayColor count]; i++) {
        ModelColor *objcolor=[objguilt.arrayColor objectAtIndex:i];
        if (i==0) {
            [cell.btnColor1 setBackgroundColor:[self getColorwithname:objcolor.name]];
        }
        else if (i==1)
        {
            [cell.btnColor2 setBackgroundColor:[self getColorwithname:objcolor.name]];
            
        }   
        else if (i==2)
        {
            [cell.btnColor3 setBackgroundColor:[self getColorwithname:objcolor.name]];
            
        }
        else if (i==3)
        {
            [cell.btnColor4 setBackgroundColor:[self getColorwithname:objcolor.name]];
            
        }
        
    }
    cell.btnComments.tag=indexPath.row;
    cell.btnShare.tag=indexPath.row;
    
    cell.btDownloads.tag=indexPath.row;
    cell.btnLike.tag=indexPath.row;
    if (objguilt.guilt_comment_count==0) {
        cell.lblCommentsCount.textColor=[UIColor grayColor];
    }
    else{
        cell.lblCommentsCount.textColor=[UIColor colorWithRed:29.0/255.0 green:65.0/255.0 blue:222.0/255.0 alpha:1.0];
    }
    if (objguilt.guilt_share_count==0) {
        cell.lblhareCounts.textColor=[UIColor grayColor];
    }
    else{
        cell.lblhareCounts.textColor=[UIColor colorWithRed:29.0/255.0 green:65.0/255.0 blue:222.0/255.0 alpha:1.0];
    }
    cell.lblCommentsCount.text=[NSString stringWithFormat:@"%d",objguilt.guilt_comment_count];
    cell.lblDlownloadCount.text=[NSString stringWithFormat:@"%d",objguilt.guilt_download_count];
    cell.lblhareCounts.text=[NSString stringWithFormat:@"%d",objguilt.guilt_share_count];
    
    cell.viewcontent.layer.masksToBounds = YES;
    //Adds a shadow to sampleView
    cell.viewcontent.layer.shadowOffset = CGSizeMake(1, 1);
    cell.viewcontent.layer.shadowColor = [[UIColor blackColor] CGColor];
    cell.viewcontent.layer.shadowRadius = 4.0f;
    cell.viewcontent.layer.shadowOpacity = 0.80f;
    cell.viewcontent.layer.shadowPath = [[UIBezierPath bezierPathWithRect:cell.viewcontent.layer.bounds] CGPath];
    cell.viewcontent.layer.cornerRadius = 2;
    cell.textviewComments.editable= NO;
    cell.textviewComments.dataDetectorTypes = UIDataDetectorTypeAll;
    //    [cell setNeedsUpdateConstraints];
    //    [cell updateConstraintsIfNeeded ];
    return cell;
}

- (void) tapGesture: (id)sender
{
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;

    TableCellComments *cell = [tableFeeds cellForRowAtIndexPath:[NSIndexPath indexPathForRow:tapRecognizer.view.tag inSection:0]];
    imageview.image=cell.ivFeeds.image;

    //-- Show view.
    scrollview.hidden=NO;
    btnCross.hidden=NO;
    scrollview.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        scrollview.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished){
        // do something once the animation finishes, put it here
    }];
    
}

-(UIColor*)getColorwithname:(NSString*)colorname
{
    if ([colorname isEqualToString:@"Orange"]) {
        return [UIColor orangeColor];
    }
    else if ([colorname isEqualToString:@"Brown"])
    {
        return [UIColor brownColor];
        
    }
    else if ([colorname isEqualToString:@"Yellow"])
    {
        return [UIColor yellowColor];
        
    }
    else if ([colorname isEqualToString:@"Red"])
    {
        return [UIColor redColor];
        
    }
    else if ([colorname isEqualToString:@"Purple"])
    {
        return [UIColor purpleColor];
        
    }
    else if ([colorname isEqualToString:@"Blue"])
    {
        return [UIColor blueColor];
        
    }
    else if([colorname isEqualToString:@"Green"])
    {
        return [UIColor greenColor];
        
    }
    else if([colorname isEqualToString:@"Gray"])
    {
        return [UIColor grayColor];
        
    }
    else if([colorname isEqualToString:@"White"])
    {
        return [UIColor whiteColor];
        
    }
    else if ([colorname isEqualToString:@"Black"])
    {
        return [UIColor blackColor];
        
    }
    else if([colorname isEqualToString:@"Pink"])
    {
        //Pink color 255-192-203
        return [UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:203.0/255.0 alpha:1.0];
        
    }
    else if([colorname isEqualToString:@"Gold"])
    {
        //Golden color 255-215-0
        return [UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:0.0/255.0 alpha:1.0];
        
    }
    else if([colorname isEqualToString:@"Silver"])
    {
        //Silver color 192-192-192
        return [UIColor colorWithRed:192.0/255.0 green:192.0/255.0 blue:192.0/255.0 alpha:1.0];
    }
    else if([colorname isEqualToString:@"Beige"])
    {
        //Silver color 192-192-192
        return [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:220.0/255.0 alpha:1.0];
    }
    else if([colorname isEqualToString:@"Rose"])
    {
        //Silver color 192-192-192
        return [UIColor colorWithRed:255.0/255.0 green:102.0/255.0 blue:204.0/255.0 alpha:1.0];
    }
    else{
        return [UIColor whiteColor];
    }
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
}

-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imageview;
}

#pragma mark MBProgress
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
