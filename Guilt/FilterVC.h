//
//  FilterVC.h
//  Guilt
//
//  Created by Gourav Sharma on 11/2/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#import "ModelColor.h"
#import "ModelProduct.h"
@interface FilterVC : UIViewController
{
    __weak IBOutlet UICollectionView *collectionviewPicker;
    __weak IBOutlet NSLayoutConstraint *consviewSubcatagoryMen;
    __weak IBOutlet NSLayoutConstraint *consviewSubcatagoryWomen;

    __weak IBOutlet NSLayoutConstraint *consverticalWomen;
    __weak IBOutlet NSLayoutConstraint *consverticalMen;
    __weak IBOutlet UIView *viewWomen;
    __weak IBOutlet UIView *viewMen;
    
    __weak IBOutlet CustomButton *btnSubcatagory;
    __weak IBOutlet UIImageView *imageSelectmaincatagory;
    IBOutlet CustomButton *BTNSELECTCATAGORY;
    IBOutlet UITableView *tableviewBrands;
    IBOutlet CustomButton *btnWomen;
    IBOutlet CustomButton *btnMen;
    __weak IBOutlet UIView *viewShadow;
    IBOutlet UITableView *tableview;
}
-(void)selectFilter:(ModelProduct*)product genderType:(NSString *)gendertype;

@property (strong, nonatomic) NSMutableArray *arrayColor;
@property (strong, nonatomic) IBOutlet CustomLabel *lblHeAder;

@property (weak, nonatomic) id delegate ;

@property (strong, nonatomic) NSString  *navigationType;
-(void)getSelectFilter:(NSMutableArray*)arraycolorlocal catagory:(ModelProduct*)objcatagory barnd:(ModelProduct*)objBrandlocal;

@end
