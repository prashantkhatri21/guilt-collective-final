//
//  FilterVC.m
//  Guilt
//
//  Created by Gourav Sharma on 11/2/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "FilterVC.h"
#import "TablecellSetting.h"
#import "Utility.h"
#import "ModelProduct.h"
#import "ViewController.h"
#import "Addmorecell.h"
@interface FilterVC ()<UIActionSheetDelegate>
{
    NSMutableArray *arraySettings,*newDataarray;
    NSMutableArray *arrayChecks;
    BOOL isMen;
     BOOL isMenSubviewOpen,isWoenSubviewOpen;
    NSString *gender;
    NSMutableArray *arrayBrands;
    NSMutableArray *arrayColor;
    NSMutableArray *arraySelectedColor;
    ModelProduct *objCatagory;
    ModelColor *objColor;
    ModelProduct *objBrand;
    NSString *stringMAINCATAGORY;
    NSIndexPath *lastselectindexpathCatagory;
    NSIndexPath *lastselectindexpathProduct;

}
@end

@implementation FilterVC


- (void)viewDidLoad {
    [super viewDidLoad];
    consviewSubcatagoryWomen.constant=0;
    consviewSubcatagoryMen.constant=0;
    consverticalMen.constant=10;
     consverticalWomen.constant=10;
    viewMen.hidden=YES;
    viewWomen.hidden=YES;
    [btnWomen setTitle:@"+  WOMEN" forState:UIControlStateNormal];
    [btnMen setTitle:@"+  MEN" forState:UIControlStateNormal];
    isMenSubviewOpen=NO;
    isWoenSubviewOpen=NO;
    self.tabBarController.tabBar.hidden=YES;

    arrayColor=[[NSMutableArray alloc]init];
    arraySelectedColor=[[NSMutableArray alloc]init];
    arrayBrands=[[NSMutableArray alloc]init];
    arraySettings =[[NSMutableArray alloc]init];
    newDataarray =[[NSMutableArray alloc]init];
    
    tableview.hidden=NO;
    tableviewBrands.hidden=YES;
    
    collectionviewPicker.hidden=YES;
    //Adds a shadow to sampleView
    CALayer *layer = viewShadow.layer;
    
    //changed to zero for the new fancy shadow
    layer.shadowOffset = CGSizeMake(0.0, 7.0);
    
    layer.shadowColor = [[UIColor blackColor] CGColor];
    
    //changed for the fancy shadow
    layer.shadowRadius = 0.0f;
    
    layer.shadowOpacity = 0.80f;
    
    //call our new fancy shadow method
    //    layer.shadowPath = [self fancyShadowForRect:layer.bounds];
    
    arrayChecks =[[NSMutableArray alloc]initWithObjects:@"0",@"0", @"0",@"0",@"0",@"0",@"0",nil];
    INITIALIZE_LOADER
    isMen=YES;
    gender=@"men";
    stringMAINCATAGORY=@"mens-clothes";
    [BTNSELECTCATAGORY setTitle:@"CLOTHING" forState:UIControlStateNormal];

    [self GetCatagory];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    INITIALIZE_LOADER
}

/**
 *  Get catagory accroding to filter men and women.
 */
-(void)GetCatagory
{
    [APPDELEGATE ShowProgress];
    
    
    NSString *url =[NSString stringWithFormat:@"%@%@%@&cat=%@",kBaseUrl_Live,CATAGORIES_API,APITokenURL_Product,stringMAINCATAGORY];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [APPDELEGATE removeProgress];
        [self parseData:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        [APPDELEGATE removeProgress];
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    }];

}


-(void)parseData:(id)responseObject
{
    [arraySettings removeAllObjects];
    
    
    if ([responseObject valueForKey:@"categories"] !=nil)
    {
        NSLog(@"categories=%@",[responseObject valueForKey:@"categories"]);
        if ([[responseObject valueForKey:@"categories"] count]==0) {
            [APPDELEGATE showToast:@"No categories Found"];
        }
        for (id dictProduct in [responseObject valueForKey:@"categories"]) {
            ModelProduct *objModelProdct=[[ModelProduct alloc]init];
            
            if ([dictProduct valueForKey:@"name"]!=nil) {
                objModelProdct.name=[dictProduct valueForKey:@"name"];
            }
            if ([dictProduct valueForKey:@"id"]!=nil) {
                objModelProdct.idProduct=[dictProduct valueForKey:@"id"];
                if([objModelProdct.idProduct isEqualToString:objCatagory.idProduct])
                {
                    objModelProdct.iselected=YES;

                }
                
            }
            
            [arraySettings addObject:objModelProdct];
        }
        [tableview reloadData];
    }
    
    
}
- (CGPathRef)fancyShadowForRect:(CGRect)rect
{
    CGSize size = rect.size;
    UIBezierPath* path = [UIBezierPath bezierPath];
    
    //right
    [path moveToPoint:CGPointZero];
    [path addLineToPoint:CGPointMake(size.width, 0.0f)];
    [path addLineToPoint:CGPointMake(size.width, size.height + 15.0f)];
    
    //curved bottom
    [path addCurveToPoint:CGPointMake(0.0, size.height + 15.0f)
            controlPoint1:CGPointMake(size.width - 15.0f, size.height)
            controlPoint2:CGPointMake(15.0f, size.height)];
    
    [path closePath];
    
    return path.CGPath;
}
- (IBAction)backClick:(id)sender {
//    [self dismissViewControllerAnimated:YES completion:nil];
    POP
    
}
- (IBAction)resetClick:(id)sender {
}
- (IBAction)applyFilterClick:(id)sender {
    
    [self.delegate getSelectFilter:arraySelectedColor catagory:objCatagory barnd:objBrand];
    
    POP
    
    
    
}
- (IBAction)clickColor:(id)sender {
    tableview.hidden=YES;
    tableviewBrands.hidden=YES;
    BTNSELECTCATAGORY.hidden=YES;
    imageSelectmaincatagory.hidden=YES;
    collectionviewPicker.hidden=NO;
    [self getColors];
    
}
- (IBAction)clickBrand:(id)sender {
    BTNSELECTCATAGORY.hidden=YES;
    imageSelectmaincatagory.hidden=YES;
    collectionviewPicker.hidden=YES;
    tableview.hidden=YES;
    tableviewBrands.hidden=NO;
    [self GetBrands];
}

- (IBAction)clickMen:(id)sender {
    consviewSubcatagoryWomen.constant=0;
    consverticalWomen.constant=10;
    isWoenSubviewOpen=NO;

    viewWomen.hidden=YES;
    [btnWomen setTitle:@"+  WOMEN" forState:UIControlStateNormal];
    if (isMenSubviewOpen) {
        consviewSubcatagoryMen.constant=0;
        consverticalMen.constant=10;
        viewMen.hidden=YES;
        [btnMen setTitle:@"+  MEN" forState:UIControlStateNormal];
        
    }
    else{
        consviewSubcatagoryMen.constant=153;
        consverticalMen.constant=168;
        viewMen.hidden=NO;
        [btnMen setTitle:@"-  MEN" forState:UIControlStateNormal];
    }
    isMenSubviewOpen=!isMenSubviewOpen;
    BTNSELECTCATAGORY.hidden=NO;
    imageSelectmaincatagory.hidden=NO;
    tableview.hidden=NO;
    tableviewBrands.hidden=YES;
    
    collectionviewPicker.hidden=YES;
    isMen=YES;
//    stringMAINCATAGORY=@"mens-clothes";
//    [self GetCatagory];

    
}

- (IBAction)clickWomen:(id)sender {
    consviewSubcatagoryMen.constant=0;
    consverticalMen.constant=10;
    isMenSubviewOpen=NO;
    viewMen.hidden=YES;
    
    [btnMen setTitle:@"+  MEN" forState:UIControlStateNormal];
    if (isWoenSubviewOpen) {
        consviewSubcatagoryWomen.constant=0;
        consverticalWomen.constant=10;
        viewWomen.hidden=YES;
        [btnWomen setTitle:@"+  WOMEN" forState:UIControlStateNormal];
        
    }
    else{
        consviewSubcatagoryWomen.constant=157;
        consverticalWomen.constant=168;

        viewWomen.hidden=NO;
        [btnWomen setTitle:@"-  WOMEN" forState:UIControlStateNormal];
    }
    isWoenSubviewOpen=!isWoenSubviewOpen;
    
    
    
    BTNSELECTCATAGORY.hidden=NO;
    imageSelectmaincatagory.hidden=NO;
    tableview.hidden=NO;
    tableviewBrands.hidden=YES;
    
    collectionviewPicker.hidden=YES;
    isMen=NO;
//    stringMAINCATAGORY=@"womens-clothes";
//    [self GetCatagory];
}
- (IBAction)subcatagoryClick:(id)sender {
    NSString *selectedmainCatagory=@"";

    UIButton *btn=sender;
    [btn setBackgroundColor:[UIColor colorWithRed:10.0/255.0 green:10.0/255.0 blue:10.0/255.0 alpha:1.0]];
    
        if (btn.tag==101) {
            selectedmainCatagory=@"clothes";

        }else if (btn.tag==102)
        {
            if (isMen) {
                selectedmainCatagory=@"bags";
            }
            else{
                selectedmainCatagory=@"handbags";

            }

        }
        else if (btn.tag==103)
        {
            selectedmainCatagory=@"shoes";

        }
        else if (btn.tag==104)
        {
            
            if (isMen) {
                selectedmainCatagory=@"grooming";
            }
            else{
                selectedmainCatagory=@"beauty";
                
            }

        }
        
    if (isMen) {
        stringMAINCATAGORY =[NSString stringWithFormat:@"mens-%@",selectedmainCatagory];
        
    }
    else{
        if ([selectedmainCatagory isEqualToString:@"handbags" ]) {
            stringMAINCATAGORY=[NSString stringWithFormat:@"%@",selectedmainCatagory];
            
        }
        else{
            stringMAINCATAGORY =[NSString stringWithFormat:@"womens-%@",selectedmainCatagory];
            
        }
        
    }
    [self GetCatagory];

}



#pragma mark Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView==tableview) {
        if (tableview.hidden==YES) {
            return 0;
        }
        else{
            return arraySettings.count;
            
        }
    }
    else if (tableView==tableviewBrands){
        if (tableviewBrands.hidden==YES) {
            return 0;
        }
        else{
            return arrayBrands.count;

        }
        
    }
    else{
        return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    TablecellSetting *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[TablecellSetting alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    ModelProduct *objProduct;
   
    
    
    if (tableView == tableviewBrands) {
        objProduct=[arrayBrands objectAtIndex:indexPath.row];
        if (objProduct.iselected) {
            cell.lblSetting.backgroundColor=[UIColor colorWithRed:3.0/255.0 green:3.0/255.0  blue:3.0/255.0  alpha:1.0];
            cell.lblSetting.textColor=[UIColor whiteColor];
        }
        else
        {
            cell.lblSetting.backgroundColor=[UIColor clearColor];
            cell.lblSetting.textColor=[UIColor blackColor];
        }
        cell.ivCheck.hidden = YES;
        cell.lblSetting.text = objProduct.name;
        
        if (indexPath.row==[arrayBrands count]-2) {
            [self addMoreSearchData:[arrayBrands count]];
        }
          return cell;
        
    }
    else if (tableView == tableview){
        objProduct=[arraySettings objectAtIndex:indexPath.row];
        if (objProduct.iselected) {
            cell.lblSetting.backgroundColor=[UIColor colorWithRed:3.0/255.0 green:3.0/255.0  blue:3.0/255.0  alpha:1.0];
            cell.lblSetting.textColor=[UIColor whiteColor];
        }
        else
        {
            cell.lblSetting.backgroundColor=[UIColor clearColor];
            cell.lblSetting.textColor=[UIColor blackColor];
        }
        cell.ivCheck.hidden = YES;
        cell.lblSetting.text = objProduct.name;

          return cell;
    }

    
    return nil;
    
    
  
    
    

  
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
   

    
    TablecellSetting *objcelllast=[tableView cellForRowAtIndexPath:lastselectindexpathCatagory];
    if (objcelllast!=nil) {
        objcelllast.lblSetting.backgroundColor=[UIColor clearColor];
        objcelllast.lblSetting.textColor=[UIColor blackColor];
    }
    
    TablecellSetting *objcelllast1=[tableView cellForRowAtIndexPath:lastselectindexpathProduct];
    if (objcelllast1!=nil) {
        objcelllast1.lblSetting.backgroundColor=[UIColor clearColor];
        objcelllast1.lblSetting.textColor=[UIColor blackColor];
    }
   
    TablecellSetting *objCell=[tableView cellForRowAtIndexPath:indexPath];
  
    if (tableView== tableviewBrands) {
        
        objBrand=[arrayBrands objectAtIndex:indexPath.row];
        objBrand.iselected=YES;
        [arrayBrands replaceObjectAtIndex:indexPath.row withObject:objBrand];
        
      ModelProduct  *objBrandlast=[arrayBrands objectAtIndex:lastselectindexpathProduct.row];
        objBrandlast.iselected=NO;
        [arrayBrands replaceObjectAtIndex:lastselectindexpathProduct.row withObject:objBrandlast];
        lastselectindexpathProduct=indexPath;

    }
    else
        
    {
        objCatagory=[arraySettings objectAtIndex:indexPath.row];
        objCatagory.iselected=YES;
        [arraySettings replaceObjectAtIndex:indexPath.row withObject:objCatagory];
        
        ModelProduct  *objCatagorylast=[arraySettings objectAtIndex:lastselectindexpathCatagory.row];
        objCatagorylast.iselected=NO;
        [arraySettings replaceObjectAtIndex:lastselectindexpathCatagory.row withObject:objCatagorylast];
        lastselectindexpathCatagory=indexPath;

    }
    
    objCell.lblSetting.backgroundColor=[UIColor colorWithRed:3.0/255.0 green:3.0/255.0  blue:3.0/255.0  alpha:1.0];
    objCell.lblSetting.textColor=[UIColor whiteColor];
    
    
}

/**
 *  For getting more Brands
 *
 *  @param offset offest is index from where you wanto get data
 */
-(void)addMoreSearchData :(int)offset
{
    
    NSString *url =[NSString stringWithFormat:@"%@%@%@&offset=%@&limit=%@",kBaseUrl_Live,Brands_API,APITokenURL_Product,[NSString stringWithFormat:@"%d",offset],@"10"];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject valueForKey:@"brands"] !=nil)
        {
            NSLog(@"brands=%@",[responseObject valueForKey:@"brands"]);
            [newDataarray removeAllObjects ];
            
            for (id dictProduct in [responseObject valueForKey:@"brands"]) {
                ModelProduct *objModelProdct=[[ModelProduct alloc]init];
                
                if ([dictProduct valueForKey:@"name"]!=nil) {
                    objModelProdct.name=[dictProduct valueForKey:@"name"];
                }
                if ([dictProduct valueForKey:@"id"]!=nil) {
                    objModelProdct.idProduct=[dictProduct valueForKey:@"id"];
                    if([objModelProdct.idProduct isEqualToString:objBrand.idProduct])
                    {
                        objModelProdct.iselected=YES;
                        
                    }
                    
                }
                
                
                [newDataarray addObject:objModelProdct];
            }
        }
        [self addNewCells];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    }];
    
//    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//       
//        
//    }];
}


- (IBAction)selectSubCatagoryClick:(id)sender {
    if (isMen) {
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Choose a source:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                @"CLOTHING",
                                @"BAGS",@"SHOES",@"GROOMING",
                                nil];
        popup.backgroundColor = [UIColor whiteColor];
        [popup showInView:self.view];
    }
    else{
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Choose a source:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                @"CLOTHING",
                                @"HAND BAGS",@"SHOES",@"BEAUTY",
                                nil];
        popup.backgroundColor = [UIColor whiteColor];
        [popup showInView:self.view];
    }
    
}

#pragma mark -  Action Sheet Methods


/*!
 @brief Performed when Action sheet dismissed
 
 @discussion Load the respective things when Action sheet dismissed
 
 @param sender
 
 @return nothing
 */
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSString *selectedmainCatagory=@"";
    if(buttonIndex == 0) {
        [BTNSELECTCATAGORY setTitle:@"CLOTHING" forState:UIControlStateNormal];
        selectedmainCatagory=@"clothes";
    }
    
    else if (buttonIndex == 1){
        
        if (isMen) {
            [BTNSELECTCATAGORY setTitle:@"BAGS" forState:UIControlStateNormal];
            selectedmainCatagory=@"bags";
            
        }
        else
        {
            [BTNSELECTCATAGORY setTitle:@"HAND BAGS" forState:UIControlStateNormal];
            selectedmainCatagory=@"handbags";
            
            
        }
        
    }
    else if (buttonIndex == 2){
        [BTNSELECTCATAGORY setTitle:@"SHOES" forState:UIControlStateNormal];
        selectedmainCatagory=@"shoes";
        
    }
    
    else if (buttonIndex == 3){
        if (isMen) {
            [BTNSELECTCATAGORY setTitle:@"GROOMING" forState:UIControlStateNormal];
            selectedmainCatagory=@"grooming";
            
        }
        else
        {
            [BTNSELECTCATAGORY setTitle:@"BEAUTY" forState:UIControlStateNormal];
            selectedmainCatagory=@"beauty";
            
            
        }
        
        
    }
    if (isMen) {
        stringMAINCATAGORY =[NSString stringWithFormat:@"mens-%@",selectedmainCatagory];
        
    }
    else{
        if ([selectedmainCatagory isEqualToString:@"handbags" ]) {
            stringMAINCATAGORY=[NSString stringWithFormat:@"%@",selectedmainCatagory];
            
        }
        else{
            stringMAINCATAGORY =[NSString stringWithFormat:@"womens-%@",selectedmainCatagory];
            
        }
        
    }
    if (buttonIndex!=4) {
        [self GetCatagory];

    }
    
}





-(void)addNewCells {
    
    [tableviewBrands beginUpdates];
    
    
    
    
    int resultsSize = [arrayBrands count];
    [arrayBrands addObjectsFromArray:newDataarray];
    NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
    for (int i = resultsSize; i < resultsSize + newDataarray.count; i++)
    {
        [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    
    [tableviewBrands insertRowsAtIndexPaths:arrayWithIndexPaths withRowAnimation:UITableViewRowAnimationNone];
    [tableviewBrands endUpdates];
    
}

/**
 *  Get colors
 */
-(void)getColors
{
    [APPDELEGATE ShowProgress];
    NSString *url =[NSString stringWithFormat:@"%@%@%@%@",kBaseUrl_Live,COLORS_API,APITokenURL_Product,@"&offset=0&limit=10"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [APPDELEGATE removeProgress];
        [self parseColorsData:responseObject];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        [APPDELEGATE removeProgress];
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    }];
//    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//       
//        
//    }];
}
/**
 *  Parse data
 *
 *  @param responseObject responsem object
 */
-(void)parseColorsData:(id)responseObject
{
    [arrayColor removeAllObjects];
    
    if ([responseObject valueForKey:@"colors"] !=nil)
    {
        NSLog(@"colors=%@",[responseObject valueForKey:@"colors"]);
        if ([[responseObject valueForKey:@"colors"] count]==0) {
            [APPDELEGATE showToast:@"No Colors Found"];
        }
        for (id dictProduct in [responseObject valueForKey:@"colors"]) {
            ModelColor *objModelProdct=[[ModelColor alloc]init];
            
            if ([dictProduct valueForKey:@"name"]!=nil) {
                objModelProdct.name=[dictProduct valueForKey:@"name"];
            }
            if ([dictProduct valueForKey:@"id"]!=nil) {
                objModelProdct.idcolor=[dictProduct valueForKey:@"id"];
                
            }
            
            [arrayColor addObject:objModelProdct];
        }
    }
    [collectionviewPicker reloadData];
}

#pragma mark--- Collection View Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [arrayColor count];
}

- (NSInteger)numberOfSections
{
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"ColorCell";
    
    
    Addmorecell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.imageViewColor.image =[UIImage imageNamed:@"check_gray"];
    ModelColor *obj=[arrayColor objectAtIndex:indexPath.row];
    if (obj.isselected) {
        cell.imageViewColor.hidden=NO;
    }
    else{
        cell.imageViewColor.hidden=YES;
    }
    if ([obj.name isEqualToString:@"Orange"]) {
        cell.viewColor.backgroundColor=[UIColor orangeColor];
    }
    else if ([obj.name isEqualToString:@"Brown"])
    {
        cell.viewColor.backgroundColor=[UIColor brownColor];
        
    }
    else if ([obj.name isEqualToString:@"Yellow"])
    {
        cell.viewColor.backgroundColor=[UIColor yellowColor];
        
    }
    else if ([obj.name isEqualToString:@"Red"])
    {
        cell.viewColor.backgroundColor=[UIColor redColor];
        
    }
    else if ([obj.name isEqualToString:@"Purple"])
    {
        cell.viewColor.backgroundColor=[UIColor purpleColor];
        
    }
    else if ([obj.name isEqualToString:@"Blue"])
    {
        cell.viewColor.backgroundColor=[UIColor blueColor];
        
    }
    else if([obj.name isEqualToString:@"Green"])
    {
        cell.viewColor.backgroundColor=[UIColor greenColor];
        
    }
    else if([obj.name isEqualToString:@"Gray"])
    {
        cell.viewColor.backgroundColor=[UIColor grayColor];
        
    }
    else if([obj.name isEqualToString:@"White"])
    {
        cell.viewColor.backgroundColor=[UIColor whiteColor];
        
    }
    else if ([obj.name isEqualToString:@"Black"])
    {
        cell.viewColor.backgroundColor=[UIColor blackColor];
        
    }
    else if([obj.name isEqualToString:@"Pink"])
    {
        //Pink color 255-192-203
        cell.viewColor.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:203.0/255.0 alpha:1.0];
        
    }
    else if([obj.name isEqualToString:@"Gold"])
    {
        //Golden color 255-215-0
        cell.viewColor.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:0.0/255.0 alpha:1.0];
        
    }
    else if([obj.name isEqualToString:@"Silver"])
    {
        //Silver color 192-192-192
        cell.viewColor.backgroundColor=[UIColor colorWithRed:192.0/255.0 green:192.0/255.0 blue:192.0/255.0 alpha:1.0];
    }
    else if([obj.name isEqualToString:@"Beige"])
    {
        //Silver color 192-192-192
        cell.viewColor.backgroundColor=[UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:220.0/255.0 alpha:1.0];
    }
    else if([obj.name isEqualToString:@"Rose"])
    {
        //Silver color 192-192-192
        cell.viewColor.backgroundColor=[UIColor colorWithRed:255.0/255.0 green:102.0/255.0 blue:204.0/255.0 alpha:1.0];
    }
    
    cell.clipsToBounds=YES;
    cell.layer.cornerRadius =cell.viewColor.frame.size.width/2;
    
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    Addmorecell *cell=(Addmorecell*)[collectionView cellForItemAtIndexPath:indexPath];
    
    ModelColor *obj=[arrayColor objectAtIndex:indexPath.row];
    if (obj.isselected) {
        obj.isselected=NO;
        cell.imageViewColor.hidden=YES;
        [arraySelectedColor removeObject:obj];
        
    }
    else{
        if ([arraySelectedColor count]<4) {
            obj.isselected=YES;
            cell.imageViewColor.hidden=NO;
            [arraySelectedColor addObject:obj];
        }
        else{
            [APPDELEGATE showToast:@"you can add only four color"];
        }
        
    }
    [arrayColor replaceObjectAtIndex:indexPath.row withObject:obj];
    
}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(collectionView.frame.size.width /2-10, collectionView.frame.size.width /2-10 );
    
}


/**
 *  Fetch all brands
 */
-(void)GetBrands
{
   
    
    [APPDELEGATE ShowProgress];
    
    NSString *url;
    
    url =[NSString stringWithFormat:@"%@%@%@%@",kBaseUrl_Live,Brands_API,APITokenURL_Product,@"&offset=0&limit=10"];
    
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [APPDELEGATE removeProgress];
        [self parseBrandsData:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        [APPDELEGATE removeProgress];
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];

    }];
//    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//    }];
}


-(void)parseBrandsData:(id)responseObject
{
    [arrayBrands removeAllObjects];
    
    if ([responseObject valueForKey:@"brands"] !=nil)
    {
        NSLog(@"brands=%@",[responseObject valueForKey:@"brands"]);
        if ([[responseObject valueForKey:@"brands"] count]==0) {
            [APPDELEGATE showToast:@"No brands Found"];
        }
        for (id dictProduct in [responseObject valueForKey:@"brands"]) {
            ModelProduct *objModelProdct=[[ModelProduct alloc]init];
            
            if ([dictProduct valueForKey:@"name"]!=nil) {
                objModelProdct.name=[dictProduct valueForKey:@"name"];
            }
            if ([dictProduct valueForKey:@"id"]!=nil) {
                objModelProdct.idProduct=[dictProduct valueForKey:@"id"];
                if([objModelProdct.idProduct isEqualToString:objBrand.idProduct])
                {
                    objModelProdct.iselected=YES;
                    
                }
                
            }
            
            [arrayBrands addObject:objModelProdct];
        }
    }
    [tableviewBrands reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
