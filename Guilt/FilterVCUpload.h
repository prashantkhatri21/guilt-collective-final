//
//  FilterVCUpload.h
//  Guilt
//
//  Created by Jatin on 11/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface FilterVCUpload : UIViewController
{
    IBOutlet UITableView *tableview;

    __weak IBOutlet CustomButton *btnwomen;
    __weak IBOutlet CustomButton *btnMen;
}
@property(nonatomic,strong)NSMutableArray *arrayColor;
@property(nonatomic,strong)UIImage *imageSelected;
@end
