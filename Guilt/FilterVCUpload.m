//
//  FilterVCUpload.m
//  Guilt
//
//  Created by Jatin on 11/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "FilterVCUpload.h"
#import "Utility.h"
#import "ModelProduct.h"
#import "ViewController.h"
#import "TablecellSetting.h"
#import "UploadViewController.h"
@interface FilterVCUpload ()
{
    NSMutableArray *arraySettings,*newDataarray;
    BOOL isMen;
    ModelProduct *objCatagory;
    NSString *stringMAINCATAGORY;
    NSIndexPath *lastselectindexpathCatagory;
    MBProgressHUD *HUD,*HUDToast;

    
}
@end

@implementation FilterVCUpload

- (void)viewDidLoad {
    [super viewDidLoad];
    arraySettings =[[NSMutableArray alloc]init];
    newDataarray =[[NSMutableArray alloc]init];
    [self initializeMBProgres:self.view];
    isMen=YES;
    stringMAINCATAGORY=@"men";
    [self GetCatagory];
    
    // Do any additional setup after loading the view.
}

/**
 *  Get catagory accroding to filter men and women.
 */
-(void)GetCatagory
{
    [self ShowProgress];
    
    
    NSString *url =[NSString stringWithFormat:@"%@%@%@&cat=%@",kBaseUrl_Live,CATAGORIES_API,APITokenURL_Product,stringMAINCATAGORY];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self removeProgress];
        [self parseData:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        [self removeProgress];
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    }];
    
    //    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
    //
    //
    //
    //    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    //
    //
    //    }];
}

-(void)parseData:(id)responseObject
{
    [arraySettings removeAllObjects];
    
    if ([responseObject valueForKey:@"categories"] !=nil)
    {
        NSLog(@"categories=%@",[responseObject valueForKey:@"categories"]);
        if ([[responseObject valueForKey:@"categories"] count]==0) {
            [APPDELEGATE showToast:@"No categories Found"];
        }
        for (id dictProduct in [responseObject valueForKey:@"categories"]) {
            ModelProduct *objModelProdct=[[ModelProduct alloc]init];
            
            if ([dictProduct valueForKey:@"name"]!=nil) {
                objModelProdct.name=[dictProduct valueForKey:@"name"];
            }
            if ([dictProduct valueForKey:@"id"]!=nil) {
                objModelProdct.idProduct=[dictProduct valueForKey:@"id"];
                if([objModelProdct.idProduct isEqualToString:objCatagory.idProduct])
                {
                    objModelProdct.iselected=YES;
                    
                }
                
            }
            
            [arraySettings addObject:objModelProdct];
        }
    }
    [tableview reloadData];
}




#pragma mark Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arraySettings.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    TablecellSetting *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[TablecellSetting alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    ModelProduct *objProduct;
    objProduct=[arraySettings objectAtIndex:indexPath.row];
    if (objProduct.iselected) {
        cell.lblSetting.backgroundColor=[UIColor colorWithRed:3.0/255.0 green:3.0/255.0  blue:3.0/255.0  alpha:1.0];
        cell.lblSetting.textColor=[UIColor whiteColor];
    }
    else
    {
        cell.lblSetting.backgroundColor=[UIColor clearColor];
        cell.lblSetting.textColor=[UIColor blackColor];
    }
    cell.ivCheck.hidden = YES;
    cell.lblSetting.text = objProduct.name;
    
    
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    
    
    TablecellSetting *objcelllast=[tableView cellForRowAtIndexPath:lastselectindexpathCatagory];
    if (objcelllast!=nil) {
        objcelllast.lblSetting.backgroundColor=[UIColor clearColor];
        objcelllast.lblSetting.textColor=[UIColor blackColor];
    }
    
    
    TablecellSetting *objCell=[tableView cellForRowAtIndexPath:indexPath];
    
    objCatagory=[arraySettings objectAtIndex:indexPath.row];
    objCatagory.iselected=YES;
    [arraySettings replaceObjectAtIndex:indexPath.row withObject:objCatagory];
    
    ModelProduct  *objCatagorylast=[arraySettings objectAtIndex:lastselectindexpathCatagory.row];
    objCatagorylast.iselected=NO;
    [arraySettings replaceObjectAtIndex:lastselectindexpathCatagory.row withObject:objCatagorylast];
    lastselectindexpathCatagory=indexPath;
    
    
    
    objCell.lblSetting.backgroundColor=[UIColor colorWithRed:3.0/255.0 green:3.0/255.0  blue:3.0/255.0  alpha:1.0];
    objCell.lblSetting.textColor=[UIColor whiteColor];
    
    UploadViewController *objViewcontroller=[self.storyboard instantiateViewControllerWithIdentifier:@"UploadViewController"];
    objViewcontroller.imageSelected=self.imageSelected;
    objViewcontroller.objCatagory=objCatagory;
    if (isMen) {
        objViewcontroller.gender=@"men";
    }
    else{
        objViewcontroller.gender=@"women";

    }
[self.navigationController pushViewController:objViewcontroller animated:YES];
    
    
    
    
//    ViewController *objViewcontroller=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//    objViewcontroller.objProduct=objCatagory;
//    objViewcontroller.navigationType=@"FIND";
    
    
    
    
}

- (IBAction)backClick:(id)sender {
    //    [self dismissViewControllerAnimated:YES completion:nil];
    POP
    
}

- (IBAction)clickMen:(id)sender {
    
    isMen=YES;
    stringMAINCATAGORY= @"men";
    [btnMen setBackgroundColor :[UIColor colorWithRed:53.0/255.0 green:53.0/255.0 blue:53.0/255.0 alpha:1.0]];
    
    
    [btnwomen setBackgroundColor :[UIColor colorWithRed:3/255 green:3/255 blue:3/255 alpha:1.0]];
    [self GetCatagory];
    
}

- (IBAction)clickWomen:(id)sender {
    isMen=NO;
    stringMAINCATAGORY= @"women";
    [btnwomen setBackgroundColor :[UIColor colorWithRed:53.0/255.0 green:53.0/255.0 blue:53.0/255.0 alpha:1.0]];
    
    [btnMen setBackgroundColor :[UIColor colorWithRed:3/255 green:3/255 blue:3/255 alpha:1.0]];
    [self GetCatagory];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    INITIALIZE_LOADER
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark MBProgress
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}




/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
