//
//  FollowingCell.h
//  Guilt
//
//  Created by Gourav Sharma on 11/20/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface FollowingCell : UITableViewCell
@property (strong, nonatomic) IBOutlet CustomLabel *lblName;
@property (weak, nonatomic) IBOutlet CustomButton *btnFollow;

@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;
@end
