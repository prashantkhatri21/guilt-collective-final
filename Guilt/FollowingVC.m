//
//  FollowingVC.m
//  Guilt
//
//  Created by Gourav Sharma on 11/20/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "FollowingVC.h"
#import "FollowingCell.h"
#import "Utility.h"
#import "Model/ModelGuilt.h"
#import "UIImageView+WebCache.h"
@interface FollowingVC ()
{
    MBProgressHUD *HUD,*HUDToast;
    NSMutableArray *arrayFollowing;
}
@end

@implementation FollowingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayFollowing=[[NSMutableArray alloc]init];
    [self initializeMBProgres:self.view];
    [self callAPI];

    // Do any additional setup after loading the view.
}

//-Get Following data
-(void)callAPI{
    [self ShowProgress];
    NSMutableDictionary *dict;
    NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,FOLLOWING_API];
    dict=[[NSMutableDictionary alloc]init];
    [dict setValue:TOKEN forKey:@"token"];
    [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager setRequestSerializer:requestSerializer];
    [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self removeProgress];
        [arrayFollowing removeAllObjects];
        if ([[responseObject valueForKey:@"status"]  intValue] == 1)
        {
            //-----Not found Guilt--//
            
            if ([responseObject valueForKey:@"following_data"]!=nil && ![[responseObject valueForKey:@"following_data"] isKindOfClass:[NSNull class]]) {
                [arrayFollowing removeAllObjects];
                for (id DictfoundGuilt in [responseObject valueForKey:@"following_data"]) {
                    ModelGuilt *objGuilt=[[ModelGuilt alloc]init];
                    objGuilt.followingID=[DictfoundGuilt valueForKey:@"follow_id"];
                    objGuilt.followerID=[DictfoundGuilt valueForKey:@"follower_id"];
                   
                    //------------------------User data--------------------------//
                    
                    objGuilt.objProfile=[[ModelProfile alloc]init];
                    if ([DictfoundGuilt valueForKey:@"first_name"]!=nil  && ![[DictfoundGuilt valueForKey:@"first_name"] isKindOfClass:[NSNull class]]) {
                        objGuilt.objProfile.fullname= [NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"first_name"]];

                    }
                    if ([DictfoundGuilt valueForKey:@"email"]!=nil  && ![[DictfoundGuilt valueForKey:@"email"] isKindOfClass:[NSNull class]]) {
                        objGuilt.objProfile.email= [NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"email"]];
                        
                    }
                    if ([DictfoundGuilt valueForKey:@"description"]!=nil &&![[DictfoundGuilt valueForKey:@"description"] isKindOfClass:[NSNull class]]) {
                        objGuilt.objProfile.description_profile= [NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"description"]];
                    }
                    else{
                        objGuilt.objProfile.description_profile=@"";
                    }
                    if ([DictfoundGuilt valueForKey:@"profile_pic"]!=nil &&![[DictfoundGuilt valueForKey:@"profile_pic"] isKindOfClass:[NSNull class]]) {
                        objGuilt.objProfile.profile_pic_url= [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[DictfoundGuilt valueForKey:@"profile_pic"]]];
                        
                    }
                    if ([DictfoundGuilt valueForKey:@"age"]!=nil &&![[DictfoundGuilt valueForKey:@"age"] isKindOfClass:[NSNull class]]) {
                        objGuilt.objProfile.age= [[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"age"]] intValue];
                        
                    }
                    
                    if ([DictfoundGuilt valueForKey:@"gender"]!=nil &&[[DictfoundGuilt valueForKey:@"gender"] isKindOfClass:[NSNull class]]) {
                        NSString *str=[[DictfoundGuilt valueForKey:@"gender"] lowercaseString];
                        if ([str isEqualToString:@"male"]) {
                            objGuilt.objProfile.ismale=YES;
                        }
                        else
                        {
                            objGuilt.objProfile.ismale=NO;
                        }
                        
                        
                    }
                    if ([[DictfoundGuilt valueForKey:@"follow_chk"]  intValue] == 1)
                    {
                        objGuilt.objProfile.follow_chk=YES;
                        
                    }
                    else{
                        objGuilt.objProfile.follow_chk=NO;
                        
                        
                    }
                    [arrayFollowing addObject:objGuilt];
                    
                }
            }
            
        }
        else
        {
            if ([responseObject valueForKey:@"data"]!=nil) {
                if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                }
                else{
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                }
            }
        }
        [tableview reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self removeProgress];
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        
        
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)followingClick:(id)sender {
    UIButton *btn=sender;
    ModelGuilt *objGuilt=[arrayFollowing objectAtIndex:btn.tag];
    [self ShowProgress];
    NSMutableDictionary *dict;
    NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,ADD_FOLLOWING_API];
    dict=[[NSMutableDictionary alloc]init];
    [dict setValue:TOKEN forKey:@"token"];
    [dict setValue:objGuilt.followingID forKey:@"follow_id"];
    [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"follower_id"];
//    [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];

    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager setRequestSerializer:requestSerializer];
    [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self removeProgress];
        if ([[responseObject valueForKey:@"status"]  intValue] == 1)
        {
            
            if ([responseObject valueForKey:@"message"]!=nil) {
                if ([[responseObject valueForKey:@"message"] isEqualToString:@"unfollow success"]) {
//                    FollowingCell *cell =[tableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:btn.tag inSection:0]];
//                    [cell.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
//                    ModelGuilt *objguilt=[arrayFollowing objectAtIndex:btn.tag];
//                    objGuilt.objProfile.follow_chk=NO;
                    [arrayFollowing removeObjectAtIndex:btn.tag];
//                    [tableview reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:btn.tag inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
                    [tableview deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:btn.tag inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
                    


                }
                else if ([[responseObject valueForKey:@"message"] isEqualToString:@"follow success"]){
//                    FollowingCell *cell =[tableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:btn.tag inSection:0]];
//                    [cell.btnFollow setTitle:@"Following" forState:UIControlStateNormal];
//                    objGuilt.objProfile.follow_chk=YES;
//                    [arrayFollowing replaceObjectAtIndex:btn.tag withObject:objGuilt];
                }
                [self showToast:[responseObject valueForKey:@"message"]];

            }
            [NS_NOTIFICATION postNotificationName:NOTIFICATION_PROFILEUPDATE object:nil];
//            [self callAPI];
        }
        else
        {
            if ([responseObject valueForKey:@"data"]!=nil) {
                if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                    
                }
                else{
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                    
                }
            }
            
        }
        [tableview reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self removeProgress];
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        
        
    }];

    
}
- (IBAction)back:(id)sender {
    POP;
}
#pragma mark Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (arrayFollowing.count==0) {
        lblNofollowing.hidden=NO;
        tableview.hidden=YES;
    }
    else{
        lblNofollowing.hidden=YES;
        tableview.hidden=NO;
    }
    return arrayFollowing.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    FollowingCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[FollowingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.btnFollow.tag=indexPath.row;
    ModelGuilt *objGuilt=[arrayFollowing objectAtIndex:indexPath.row];
    cell.lblName.text=objGuilt.objProfile.fullname;
    [cell.imgProfile sd_setImageWithURL:objGuilt.objProfile.profile_pic_url placeholderImage:[UIImage imageNamed:@"user" ]];
    ;
    [cell.btnFollow setTitle:@"Following" forState:UIControlStateNormal];

//    if (objGuilt.objProfile.follow_chk)
//    {
//        [cell.btnFollow setTitle:@"Following" forState:UIControlStateNormal];
//    }
//    else{
//        [cell.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
//    }
    
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
}

#pragma mark MBProgress
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
