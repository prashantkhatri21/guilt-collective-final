//
//  LoginVC.h
//  Guilt
//
//  Created by Gourav Sharma on 10/29/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#define APP_ID @"fd725621c5e44198a5b8ad3f7a0ffa09"
@interface LoginVC : UIViewController
{
    
    IBOutlet UIView *viewEmail;
    IBOutlet UIView *viewPassword;
    IBOutlet CustomButton *lblClickHere;
    IBOutlet CustomButton *btnRemember;
    IBOutlet CustomTextField *txtPassword;
    IBOutlet CustomTextField *txtEmail;
    IBOutlet CustomButton *btnSignIn;
    IBOutlet CustomButton *btnCreateAccount;
    IBOutlet CustomButton *btnSignUpSocial;

    
    //Instagram
    IBOutlet UIWebView *webview;

    NSMutableData *receivedData;
}
@end
