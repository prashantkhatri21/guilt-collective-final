//
//  LoginVC.m
//  Guilt
//
//  Created by Gourav Sharma on 10/29/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "LoginVC.h"
#import "socialNetwork.h"
#import "UIImageView+WebCache.h"
#import "Base64Transcoder.h"
@interface LoginVC ()<WebHelperDelegate,socialNetworkDelegate,GIDSignInDelegate,GIDSignInUIDelegate>
{
    BOOL isRemember;
    socialNetwork *social;
    NSString *socialTypeGlobal;
    NSString *instagramName;
    NSString *instagramuserID;
    NSString *googleplusName;
     NSURL *googleplusImageUrl;
    NSString *googleplususerID;
    MBProgressHUD *HUD,*HUDToast;
}
@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate=self;
//    txtEmail.text=@"s123@gmail.com";
//    txtPassword.text=@"123456";
  
    txtEmail.tintColor = [UIColor blackColor];
    txtPassword.tintColor = [UIColor blackColor];
    
    social = [[socialNetwork alloc] init];
    [social setDelegate:self];
    WEBHELPER.WebHelperDelegate= self;
    viewEmail.layer.borderColor =[UIColor blackColor].CGColor;
    viewPassword.layer.borderColor =[UIColor blackColor].CGColor;
    btnSignIn.layer.borderColor =[UIColor blackColor].CGColor;
    btnCreateAccount.layer.borderColor =[UIColor blackColor].CGColor;
    btnSignUpSocial.layer.borderColor =[UIColor blackColor].CGColor;
    viewEmail.layer.cornerRadius = 5.0;
    viewEmail.layer.borderWidth = 2.0;
    viewPassword.layer.cornerRadius = 5.0;
    viewPassword.layer.borderWidth = 2.0;
    btnSignIn.layer.borderWidth = 2.0;
    btnCreateAccount.layer.borderWidth = 2.0;
    btnSignUpSocial.layer.borderWidth = 2.0;
    btnSignIn.layer.cornerRadius = 5.0;
    btnCreateAccount.layer.cornerRadius = 5.0;
    btnSignUpSocial.layer.cornerRadius = 5.0;
    isRemember =YES;
    [txtEmail setValue:[UIColor grayColor]
            forKeyPath:@"_placeholderLabel.textColor"];
    [txtPassword setValue:[UIColor grayColor]
               forKeyPath:@"_placeholderLabel.textColor"];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:@"Click here"];
    [attributeString addAttribute:NSUnderlineStyleAttributeName
                            value:[NSNumber numberWithInt:1]
                            range:(NSRange){0,[attributeString length]}];
    lblClickHere.titleLabel.attributedText=attributeString;
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([DEFAULTS valueForKey:USERID]!=nil) {
        txtEmail.text=@"";
        txtPassword.text=@"";
        [self performSegueWithIdentifier:@"gototab" sender:self];

    }
    INITIALIZE_LOADER
}
#pragma mark Social Delegate
-(void)success :(NSString*)socialType{
    socialType=socialType;
    NSLog(@"%@,%@,%@,%@",social.fbUserName,social.fbUserId,social.fbProfilePic,social.fbEmail);
    NSLog(@"%@,%@,%@,%@",social.twitterUserId,social.twitterProfilePic,social.twitterScreenName,social.twitterUserName);
    [APPDELEGATE removeProgress];
    [self login_social:socialType];
    
    
}
-(void)failedWithError:(NSString *)error{
    [APPDELEGATE removeProgress];
    //    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Failed" message:[NSString stringWithFormat:@"%@",error] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    //    [alert show];
    [APPDELEGATE showAlert:nil message:[NSString stringWithFormat:@"%@",error]];
}
-(void)cancel
{
    [APPDELEGATE removeProgress];

}


-(void)login_social :(NSString*)socialType{
    [APPDELEGATE ShowProgress];
    NSDictionary *dict;
    NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,REGISTER_API];
    dict=[[NSMutableDictionary alloc]init];
    [dict setValue:TOKEN forKey:@"token"];
    [dict setValue:@"1" forKey:@"is_social"];
    if ([socialType isEqualToString:@"facebook"])
    {
        [dict setValue:social.fbUserId forKey:@"social_id"];
        [dict setValue:social.fbEmail forKey:@"email"];
        [dict setValue:social.fbUserName forKey:@"name"];
        
        UIImageView *imageview=[[UIImageView alloc]init];
        NSData *dataImage=[NSData dataWithContentsOfURL:[NSURL URLWithString:social.fbProfilePic]];
        Base64Transcoder *objBase64=[[Base64Transcoder alloc]init];

      NSString *str= [objBase64 base64EncodedStringfromData:dataImage];
        [dict setValue:str forKey:@"profile_pic"];

//       __block NSString *str;
//        [imageview sd_setImageWithURL:[NSURL URLWithString:social.fbProfilePic] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            imageview.image=image;
//            NSData *data=UIImageJPEGRepresentation(image, 0.5);
//           str= [objBase64 base64EncodedStringfromData:data];
//        }];
        
      
        //[dict setValue:str forKey:@"profile_pic"];

    }
    else if ([socialType isEqualToString:@"Instagram"])
    {
        [dict setValue:instagramuserID forKey:@"social_id"];
        [dict setValue:instagramName forKey:@"name"];

    }
    else if ([socialType isEqualToString:@"Google"])
    {
        [dict setValue:googleplususerID forKey:@"social_id"];
        [dict setValue:googleplusName forKey:@"name"];
        NSData *dataImage=[NSData dataWithContentsOfURL:googleplusImageUrl];
        Base64Transcoder *objBase64=[[Base64Transcoder alloc]init];
        
        NSString *str= [objBase64 base64EncodedStringfromData:dataImage];
        [dict setValue:str forKey:@"profile_pic"];

        
    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager setRequestSerializer:requestSerializer];
    
    [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        
        [APPDELEGATE removeProgress];
        if ([[responseObject valueForKey:@"status"]  intValue] == 1)
        {
            if ([responseObject valueForKey:@"info"]!=nil) {
                NSDictionary *dictInfo =[responseObject valueForKey:@"info"];
                NSString *userId=[dictInfo valueForKey:@"user_id"];
                [DEFAULTS setValue:userId forKey:USERID];
                [DEFAULTS synchronize];
                txtEmail.text=@"";
                txtPassword.text=@"";
                [self performSegueWithIdentifier:@"gototab" sender:self];
                
            }
        }
        else if ([[responseObject valueForKey:@"status"]  intValue] == 0)
        {
            if ([responseObject valueForKey:@"info"]!=nil) {
                NSDictionary *dictInfo =[responseObject valueForKey:@"info"];
                NSString *userId=[dictInfo valueForKey:@"user_id"];
                [DEFAULTS setValue:userId forKey:USERID];
                [DEFAULTS synchronize];
                txtEmail.text=@"";
                txtPassword.text=@"";
                [self performSegueWithIdentifier:@"gototab" sender:self];
                
            }
            else{
                [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];

            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [APPDELEGATE removeProgress];
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    }];
    
//    [manager POST:url parameters:dict success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        
//        
//        
//    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Click Action

- (IBAction)forgetPasswordClick:(id)sender {
    [self performSegueWithIdentifier:@"forget" sender:self];
}
- (IBAction)loginClick:(id)sender {
    if ([self validate]) {
        if (![APPDELEGATE isNetwork]) {
            NETWORKALERT
        }
        else{
            [APPDELEGATE ShowProgress];
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:TOKEN forKey:@"token"];
            [dict setValue:@"0" forKey:@"is_social"];
            [dict setValue:txtEmail.text forKey:@"email"];
            [dict setValue:txtPassword.text forKey:@"password"];
            
            
            NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,LOGIN_API];
            
            
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
            
            [manager setRequestSerializer:requestSerializer];
            
            [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSLog(@"%@", responseObject);
                
                [APPDELEGATE removeProgress];
                if ([[responseObject valueForKey:@"status"]  intValue] == 1)
                {
                    //        [APPDELEGATE showAlert:@"" message:[responsedict valueForKey:@"message"]];
                    if ([responseObject valueForKey:@"info"]!=nil) {
                        NSDictionary *dictInfo =[responseObject valueForKey:@"info"];
                        NSString *userId=[dictInfo valueForKey:@"user_id"];
                        [DEFAULTS setValue:userId forKey:USERID];
                        [DEFAULTS synchronize];
                        txtEmail.text=@"";
                        txtPassword.text=@"";
                        [self performSegueWithIdentifier:@"gototab" sender:self];
                        
                    }
                }
                else if ([[responseObject valueForKey:@"status"]  intValue] == 0)
                {
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];
                    
                }

            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [APPDELEGATE removeProgress];
                [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            }];
            
            
            
        }
        
    }
    
    
}
-(BOOL)validate
{
    if (txtEmail.text.length==0) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    else if (![self NSStringIsValidEmail:txtEmail.text ]){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    else if (txtPassword.text.length==0){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    
    return true;
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (IBAction)registerClick:(id)sender {
    
}
- (IBAction)clickRemember:(id)sender {
    UIButton *btn =sender;
    
    if (isRemember) {
        [btn setBackgroundImage:[UIImage imageNamed:@"blank"] forState:UIControlStateNormal];
    }
    else
    {
        [btn setBackgroundImage:[UIImage imageNamed:@"checkmark"] forState:UIControlStateNormal];
    }
    isRemember =!isRemember;
    
}
- (IBAction)facebookClick:(id)sender {
    [APPDELEGATE ShowProgress];

    [social getFbInfo];
    
}
- (IBAction)instagramClick:(id)sender {
    
    
    
    
   
}
- (IBAction)signupSocialClick:(id)sender {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Choose a social site" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Facebook",
                            @"Instagram",@"Google Plus",
                            nil];
    popup.backgroundColor = [UIColor whiteColor];
    popup.tag=2002;
    [popup showInView:self.view];
}


#pragma marka Action sheet delegate
/*!
 @brief Performed when Action sheet dismissed
 
 @discussion Load the respective things when Action sheet dismissed
 
 @param sender
 
 @return nothing
 */
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if (actionSheet.tag==2002) {
        if (buttonIndex==0) {
            [social getFbInfo];
        }
        else if (buttonIndex==1)
        {
            AppDelegate *obj=[AppDelegate sharedAppdelegate];
            // here i can set accessToken received on previous login
            obj.instagram.accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"accessToken"];
            obj.instagram.sessionDelegate = self;
            if ([obj.instagram isSessionValid]) {
                // here i can store accessToken
                AppDelegate* appDelegate =[AppDelegate sharedAppdelegate];
                [[NSUserDefaults standardUserDefaults] setObject:appDelegate.instagram.accessToken forKey:@"accessToken"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"users/self", @"method", nil];
                [appDelegate.instagram requestWithParams:params
                                                delegate:self];
                
            } else {
                [obj.instagram authorize:[NSArray arrayWithObjects:@"comments", @"likes", nil]];
            }
        }
        else if (buttonIndex==2)
        {
            [APPDELEGATE ShowProgress];
            [[GIDSignIn sharedInstance] signIn];
        }
    }
  
}
// Implement these methods only if the GIDSignInUIDelegate is not a subclass of
// UIViewController.

// Stop the UIActivityIndicatorView animation that was started when the user
// pressed the Sign In button
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    [HUD show:YES];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations on signed in user here.
    if (error==nil) {
        NSString *userId = user.userID;                  // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *name = user.profile.name;
        NSString *email = user.profile.email;
        NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
        googleplusName= name;
        googleplususerID= userId;
        googleplusImageUrl=[user.profile imageURLWithDimension:60];
        [self login_social:@"Google"];
    }
    else{
        [APPDELEGATE showAlert:@"" message:error.description];
    }
    
    // ...
}


#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}


#pragma mark-
#pragma mark-TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:txtEmail]) {
        [txtPassword becomeFirstResponder];
    }
    else if ([textField isEqual:txtPassword]) {
        [txtPassword resignFirstResponder];
    }
    
    
    return  NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}


#pragma mark Response Delegate

- (void)didReceivedresponse:(NSDictionary *)responsedict
{
    NSLog(@"%@", responsedict);
    
    [APPDELEGATE removeProgress];
    if ([[responsedict valueForKey:@"status"]  intValue] == 1)
    {
        //        [APPDELEGATE showAlert:@"" message:[responsedict valueForKey:@"message"]];
        if ([responsedict valueForKey:@"info"]!=nil) {
            NSDictionary *dictInfo =[responsedict valueForKey:@"info"];
            NSString *userId=[dictInfo valueForKey:@"user_id"];
            [DEFAULTS setValue:userId forKey:USERID];
            [DEFAULTS synchronize];
            txtEmail.text=@"";
            txtPassword.text=@"";
            [self performSegueWithIdentifier:@"gototab" sender:self];
            
        }
        
    }
    
    else  if ([[responsedict valueForKey:@"status"]  intValue] == 0)
    {
        [APPDELEGATE showAlert:@"" message:[responsedict valueForKey:@"message"]];
    }
}

- (void)didFailedWithError:(NSError *)error
{
    [APPDELEGATE removeProgress];
    [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    
}

#pragma - IGSessionDelegate

-(void)igDidLogin {
    NSLog(@"Instagram did login");
    // here i can store accessToken
    AppDelegate* appDelegate =[AppDelegate sharedAppdelegate];
    [[NSUserDefaults standardUserDefaults] setObject:appDelegate.instagram.accessToken forKey:@"accessToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSMutableDictionary* params = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"users/self", @"method", nil];
    [appDelegate.instagram requestWithParams:params
                                    delegate:self];
}

-(void)igDidNotLogin:(BOOL)cancelled {
    NSLog(@"Instagram did not login");
    NSString* message = nil;
    if (cancelled) {
        message = @"Access cancelled!";
    } else {
        message = @"Access denied!";
    }
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
}

-(void)igDidLogout {
    NSLog(@"Instagram did logout");
    // remove the accessToken
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"accessToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)igSessionInvalidated {
    NSLog(@"Instagram session was invalidated");
}



#pragma mark - IGRequestDelegate

- (void)request:(IGRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"Instagram did fail: %@", error);
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:[error localizedDescription]
                                                       delegate:nil
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
}

- (void)request:(IGRequest *)request didLoad:(id)result {
    NSLog(@"Instagram did load: %@", result);
    NSDictionary *dataInstagram=[result valueForKey:@"data"];
    instagramName= [dataInstagram valueForKey:@"full_name"];
    instagramuserID= [dataInstagram valueForKey:@"id"];
    [self login_social:@"Instagram"];
    

}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
