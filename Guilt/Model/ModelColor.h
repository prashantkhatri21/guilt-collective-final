//
//  ModelColor.h
//  Guilt
//
//  Created by Jatin on 08/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelColor : NSObject
@property (strong, nonatomic) NSString *idcolor;
@property (strong, nonatomic) NSString *guiltid;
@property (strong, nonatomic) NSString *name;
@property(strong,nonatomic) NSString *colorcode;
@property(nonatomic) BOOL isselected;
@end
