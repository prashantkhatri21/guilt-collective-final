//
//  ModelGuilt.h
//  Guilt
//
//  Created by Jatin on 15/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModelProfile.h"
#import "ModelColor.h"
@interface ModelGuilt : NSObject
@property (strong, nonatomic) NSString *guilt_id;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *price;
@property (strong, nonatomic) NSString *descriptionGuilt;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *found_link;
@property (strong, nonatomic) ModelProfile *objProfile;
@property (strong, nonatomic) NSMutableArray *arrayColor;
@property (strong, nonatomic) NSMutableArray *arrayTag;



@property (nonatomic) int guilt_share_count;
@property (nonatomic) int guilt_comment_count;
@property (nonatomic) int guilt_download_count;
@property (nonatomic) BOOL isUserLike;
@property (nonatomic) BOOL isShare;


@property(strong,nonatomic) NSURL *image_url;
@property(nonatomic) int age;
@property(nonatomic) int followingCount;
@property(nonatomic) int followercCount;

@property(nonatomic) BOOL ismale;
@property(nonatomic) BOOL found;

//--Comment Data
@property(nonatomic,strong) NSString *comment;
@property(nonatomic,strong) NSString *comment_ID;
@property(nonatomic,strong) NSString *comment_dateString;

//--Follower/Following Data
@property(nonatomic,strong) NSString *followerID;
@property(nonatomic,strong) NSString *followingID;
@property(nonatomic,strong) NSString *createdat;

@end


