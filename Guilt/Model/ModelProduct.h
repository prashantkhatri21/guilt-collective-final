//
//  ModelProduct.h
//  Guilt
//
//  Created by Jatin on 08/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelProduct : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *idProduct;
@property(strong,nonatomic) NSURL *imageurl;
@property(strong,nonatomic)NSURL *productUrl;
@property(strong,nonatomic)NSString *price;
@property(strong,nonatomic)NSString *brand;
@property(nonatomic)BOOL iselected;
@end