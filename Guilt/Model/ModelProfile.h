//
//  ModelProfile.h
//  Guilt
//
//  Created by Jatin on 15/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelProfile : NSObject
@property (strong, nonatomic) NSString *fullname;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *description_profile;
@property (nonatomic) BOOL follow_chk;


//-Follower data
@property(strong,nonatomic) NSURL *profile_pic_url;
@property(nonatomic) int age;
@property(nonatomic) int followingCount;
@property(nonatomic) int followercCount;

@property(nonatomic) BOOL ismale;
@property (strong, nonatomic) NSString *gender;

//-Notification data
@property (strong, nonatomic) NSString *notificationID;
@property (strong, nonatomic) NSString *notificationType;
@property (strong, nonatomic) NSString *notificationMessage;
@end




