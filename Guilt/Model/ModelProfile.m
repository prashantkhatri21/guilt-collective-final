//
//  ModelProfile.m
//  Guilt
//
//  Created by Jatin on 15/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "ModelProfile.h"

@implementation ModelProfile
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.fullname=@"";
        self.email=@"";
        self.description_profile=@"";
        self.gender=@"";
    }
    return self;
}
@end
