//
//  ModelTag.h
//  Guilt
//
//  Created by Gourav Sharma on 12/17/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModelTag : NSObject
@property (strong, nonatomic) NSString *tag_id;
@property (strong, nonatomic) NSString *guilt_id;
@property (strong, nonatomic) NSString *tag;
@property (strong, nonatomic) NSString *type;
@end

