//
//  NewPasswordVC.h
//  Guilt
//
//  Created by Gourav Sharma on 11/4/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface NewPasswordVC : UIViewController
{
    
    IBOutlet CustomTextField *txtPasswordConfirmation;
    IBOutlet CustomTextField *txtNewPassword;
}
@end
