//
//  NewPasswordVC.m
//  Guilt
//
//  Created by Gourav Sharma on 11/4/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "NewPasswordVC.h"
#import "Utility.h"
@interface NewPasswordVC ()

@end

@implementation NewPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backClick:(id)sender {
    POP
}

#pragma mark-
#pragma mark-TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:txtNewPassword]) {
        [txtPasswordConfirmation becomeFirstResponder];
    }
    else if ([textField isEqual:txtPasswordConfirmation]) {
        [txtPasswordConfirmation resignFirstResponder ];
    }
   
    
    
    return  NO;
}
-(BOOL)validate
{
    if (txtNewPassword.text.length==0){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter new password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    else if (txtPasswordConfirmation.text.length==0){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter confirm password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    else if ([txtPasswordConfirmation.text isEqualToString:txtNewPassword.text]){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"New password and confirm password must be same" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    return true;
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
