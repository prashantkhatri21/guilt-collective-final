//
//  NewsLetterVC.h
//  Guilt
//
//  Created by Prashant khatri on 03/11/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface NewsLetterVC : UIViewController
{
    
    __weak IBOutlet CustomTextField *txtEmail;
    __weak IBOutlet CustomButton *btnWeekly;
    __weak IBOutlet CustomButton *btnDaily;
}
@end
