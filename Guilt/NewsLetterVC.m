//
//  NewsLetterVC.m
//  Guilt
//
//  Created by Prashant khatri on 03/11/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "NewsLetterVC.h"
#import "Utility.h"
@interface NewsLetterVC ()
{
    MBProgressHUD *HUD,*HUDToast;
    BOOL issubascribe;
    NSString *type;
}
@end

@implementation NewsLetterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeMBProgres:self.view];

    [txtEmail setValue:[UIColor darkGrayColor]
            forKeyPath:@"_placeholderLabel.textColor"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark Click Action
- (IBAction)subsCribeClick:(id)sender {
    [self ShowProgress];
    issubascribe=YES;

    if ([txtEmail.text length]==0) {
        [self showToast:@"Please enter email."];
    }
    else if (![self NSStringIsValidEmail:txtEmail.text]) {
        [self showToast:@"Please enter valid email"];
    }
    else{
        NSMutableDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,NEWSLETTER_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        [dict setValue:txtEmail.text forKey:@"email"];
        [dict setValue:type forKey:@"type"];

        if (issubascribe) {
            [dict setValue:@"1" forKey:@"subscribe"];
        }
        else{
            [dict setValue:@"0" forKey:@"subscribe"];

        }
        
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                
                if ([responseObject valueForKey:@"message"]!=nil) {
                        [self showToast:[responseObject valueForKey:@"message"]];
                    
                }
                
            }
            else
            {
                if ([responseObject valueForKey:@"data"]!=nil) {
                    if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                        
                    }
                    else{
                        [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                        
                    }
                }
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];

    }
}
- (IBAction)unsubscribeClick:(id)sender {
    issubascribe=NO;
    [self ShowProgress];

    
    if ([txtEmail.text length]==0) {
        [self showToast:@"Please enter email."];
    }
    else if (![self NSStringIsValidEmail:txtEmail.text]) {
        [self showToast:@"Please enter valid email"];
    }
    else{
        NSMutableDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,NEWSLETTER_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        [dict setValue:txtEmail.text forKey:@"email"];
        [dict setValue:type forKey:@"type"];
        
        if (issubascribe) {
            [dict setValue:@"1" forKey:@"subscribe"];
        }
        else{
            [dict setValue:@"0" forKey:@"subscribe"];
            
        }
        
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                
                if ([responseObject valueForKey:@"message"]!=nil) {
                    [self showToast:[responseObject valueForKey:@"message"]];
                    
                }
                
            }
            else
            {
                if ([responseObject valueForKey:@"data"]!=nil) {
                    if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                        
                    }
                    else{
                        [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                        
                    }
                }
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];
        
    }

}
- (IBAction)dailyClick:(id)sender {
    type=@"news";
    [btnDaily setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDaily setBackgroundColor :[UIColor blackColor]];
    [btnWeekly setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    [btnWeekly setBackgroundColor :[UIColor whiteColor]];
}
- (IBAction)weeklyClick:(id)sender {
    type=@"news";

    [btnWeekly setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnWeekly setBackgroundColor :[UIColor blackColor]];
    [btnDaily setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    [btnDaily setBackgroundColor :[UIColor whiteColor]];
}
- (IBAction)backClick:(id)sender {
    POP
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark MBProgress
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
