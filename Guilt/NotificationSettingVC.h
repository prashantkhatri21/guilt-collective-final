//
//  NotificationSettingVC.h
//  Guilt
//
//  Created by Gourav Sharma on 10/29/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationSettingVC : UIViewController
{
    
    IBOutlet UISwitch *objSwitchNewLikes;
    IBOutlet UISwitch *objSwitchNewFollowers;
    IBOutlet UISwitch *objSwitchNewComments;
    IBOutlet UISwitch *objSwitchAllnotification;
}
@end
