//
//  NotificationVC.m
//  Guilt
//
//  Created by Gourav Sharma on 10/29/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "NotificationVC.h"
#import "TableCellComments.h"
#import "Utility.h"
#import "ModelProfile.h"
#import "UIImageView+WebCache.h"
@interface NotificationVC ()
{
     NSMutableArray *arrayComments;
     NSMutableArray *arraySections;
    MBProgressHUD *HUD,*HUDToast;

}
@end

@implementation NotificationVC


- (void)viewDidLoad {
    [super viewDidLoad];
    objTableview.delegate=self;
    objTableview.dataSource=self;
    arraySections=[[NSMutableArray alloc]init];
    arrayComments =[[NSMutableArray alloc]init];
    [self initializeMBProgres:self.view];
    [self callAPI];
//    arraySections =[[NSMutableArray alloc]initWithObjects:@"Today",@"Yesterday",@"Later This Month", nil];
    
   
    // Do any additional setup after loading the view.
}

-(void)callAPI
{
        [self ShowProgress];
        NSMutableDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,NOTIFICATIONS_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];

        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                [arrayComments removeAllObjects];
                if ([responseObject valueForKey:@"data"]!=nil) {
                    for (id DictNotification in [responseObject valueForKey:@"data"]) {
                        ModelProfile *objProfile=[[ModelProfile alloc]init];
                        if ([DictNotification valueForKey:@"id"]!=nil) {
                            objProfile.notificationID=[NSString stringWithFormat:@"%@",[DictNotification valueForKey:@"id"]];
                        }
                        if ([DictNotification valueForKey:@"notification_type"]!=nil) {
                            objProfile.notificationType=[NSString stringWithFormat:@"%@",[DictNotification valueForKey:@"notification_type"]];

                        }
                        if ([DictNotification valueForKey:@"message"]!=nil) {
                            objProfile.notificationMessage=[NSString stringWithFormat:@"%@",[DictNotification valueForKey:@"message"]];
                            
                        }
                        if ([DictNotification valueForKey:@"senderDetail"]!=nil) {
                            NSDictionary *dictUser =[DictNotification valueForKey:@"senderDetail"];
                            
                            objProfile.fullname= [NSString stringWithFormat:@"%@",[dictUser valueForKey:@"first_name"]];
                            objProfile.email= [NSString stringWithFormat:@"%@",[dictUser valueForKey:@"email"]];
                            if ([dictUser valueForKey:@"user_description"]!=nil) {
                                objProfile.description_profile= [NSString stringWithFormat:@"%@",[dictUser valueForKey:@"user_description"]];
                                
                            }
                            else{
                                objProfile.description_profile=@"";
                            }
                            
                            
                            objProfile.profile_pic_url= [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[dictUser valueForKey:@"user_profile_pic"]]];
                            objProfile.age= [[NSString stringWithFormat:@"%@",[dictUser valueForKey:@"age"]] intValue];
                            if ([dictUser valueForKey:@"gender"]!=nil) {
                                NSString *str=[[dictUser valueForKey:@"gender"] lowercaseString];
                                if ([str isEqualToString:@"male"]) {
                                    objProfile.ismale=YES;
                                }
                                else
                                {
                                    objProfile.ismale=NO;
                                }
                                
                                
                            }
                        }
                        
                        [arrayComments addObject:objProfile];
                    }
                }
               

            }
            else
            {
                if ([responseObject valueForKey:@"data"]!=nil) {
                    if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                        
                    }
                    else{
                        [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                        
                    }
                }
                
            }
            [objTableview reloadData];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];
    [objTableview reloadData];

}

#pragma mark Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayComments.count;
}

//-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    
//    return 35;
//}

//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    
//    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,35)];
//    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width,1)];
//     [line1 setBackgroundColor:[UIColor colorWithRed:0.7765 green:0.7765  blue:0.7765 alpha:1.0]];
//    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, header.bounds.size.height-5, tableView.bounds.size.width,1)];
//     [line2 setBackgroundColor:[UIColor colorWithRed:0.7765 green:0.7765  blue:0.7765 alpha:1.0]];
//    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, header.bounds.size.width-20, 20)];
//    
//    
//    dateLabel.text = [arraySections objectAtIndex:section];
//    
//    [dateLabel setBackgroundColor:[UIColor clearColor]];
//    [dateLabel setTextColor:[UIColor colorWithRed:0.92940 green:.1686  blue:0.1804 alpha:1.0]];
//    
//    [header addSubview:dateLabel];
//    [header addSubview:line1];
//    [header addSubview:line2];
//    
//    
//   
//    
//    return header;
//    
//}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    TableCellComments *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[TableCellComments alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textviewComments.editable= NO;
    cell.textviewComments.dataDetectorTypes = UIDataDetectorTypeAll;
    ModelProfile *objProfile =[arrayComments objectAtIndex:indexPath.row];
    cell.lblName.text=objProfile.fullname;
    cell.textviewComments.text=objProfile.notificationMessage;
    
    [cell.ivProfile sd_setImageWithURL:objProfile.profile_pic_url placeholderImage:[UIImage imageNamed:@"user.png"]];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    // check here, if it is one of the cells, that needs to be resized
//    // to the size of the contained UITextView
//
//        return [self textViewHeightForRowAtIndexPath:indexPath]+34;
//
//}

//- (CGFloat)textViewHeightForRowAtIndexPath: (NSIndexPath*)indexPath {
//    UITextView *calculationView ;
//    CGFloat textViewWidth = 300;
//    if (!calculationView.attributedText) {
//        // This will be needed on load, when the text view is not inited yet
//
//        calculationView = [[UITextView alloc] init];
//        calculationView.attributedText = [[NSAttributedString alloc ]initWithString:[arrayComments objectAtIndex:indexPath.row] ];// get the text from your datasource add attributes and insert here
//        textViewWidth = 300; // Insert the width of your UITextViews or include calculations to set it accordingly
//    }
//    CGSize size = [calculationView sizeThatFits:CGSizeMake(textViewWidth, FLT_MAX)];
//    NSLog(@"Size height =%f",size.height);
//
//    return size.height;
//}


#pragma mark Click Action
- (IBAction)backClick:(id)sender {
    POP;
}
#pragma mark MBProgress
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
@end
