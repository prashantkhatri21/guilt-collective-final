//
//  PasswordRecoveryVC.h
//  Guilt
//
//  Created by Gourav Sharma on 11/4/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#import "AFNetworking.h"
@interface PasswordRecoveryVC : UIViewController
@property (strong, nonatomic) IBOutlet CustomTextField *txtEmail;

@end
