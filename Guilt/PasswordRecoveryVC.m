//
//  PasswordRecoveryVC.m
//  Guilt
//
//  Created by Gourav Sharma on 11/4/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "PasswordRecoveryVC.h"
#import "Utility.h"
#import <AFNetworking.h>


@interface PasswordRecoveryVC ()

@end
@implementation PasswordRecoveryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    INITIALIZE_LOADER;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)validate
{
    if (self.txtEmail.text.length==0) {
        [APPDELEGATE showAlert:@"" message:@"Please enter email"];
        
        return false;
    }
    
    else if (![self NSStringIsValidEmail:self.txtEmail.text]){
        [APPDELEGATE showAlert:@"" message:@"Please enter valid email"];

        return false;
    }
    
    return true;
}

- (IBAction)RequestPasswordClick:(id)sender {
    if ([self validate]) {
        [APPDELEGATE ShowProgress];
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,FORGETPASSWORD_API];

        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                TOKEN, @"token",
                                self.txtEmail.text,@"email",
                                nil];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [APPDELEGATE removeProgress];
            
            
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                //        [APPDELEGATE showAlert:@"" message:[responsedict valueForKey:@"message"]];
                if ([responseObject valueForKey:@"message"]!=nil) {
                    
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];
                    [self dismissViewControllerAnimated:YES completion:nil];
                    
                }
            }
            else if ([[responseObject valueForKey:@"status"]  intValue] == 0)
            {
                [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [APPDELEGATE removeProgress];
            
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        }];
        
//        [manager POST:url parameters:params success:^(NSURLSessionTask *operation, id responseObject) {
//            
//            
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            [APPDELEGATE removeProgress];
//            
//            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
//            
//        }];

    }
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
#pragma mark-
#pragma mark-TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    
    return  NO;
}
- (IBAction)backClick:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    POP;
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
