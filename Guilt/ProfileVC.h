//
//  ProfileVC.h
//  Guilt
//
//  Created by Gourav Sharma on 10/29/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface ProfileVC : UIViewController
{
    
    IBOutlet CustomButton *btnProfile;
    IBOutlet CustomTextView *txtviewDescription;
    IBOutlet CustomTextField *txtEmail;
    IBOutlet CustomTextField *txtName;
    IBOutlet CustomTextField *txtFemale;
}
@end
