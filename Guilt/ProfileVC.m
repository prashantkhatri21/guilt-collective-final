//
//  ProfileVC.m
//  Guilt
//
//  Created by Gourav Sharma on 10/29/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "ProfileVC.h"
#import "Utility.h"
#import "LoginVC.h"
#import "UIImageView+WebCache.h"
#import "Base64Transcoder.h"

@interface ProfileVC ()
{
    MBProgressHUD *HUD,*HUDToast;
    BOOL isImageupload;
    UIImagePickerController *photoPicker;
    UIPopoverController *popOver;
    UIImage *selectImage;
    
}
@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    btnProfile.layer.masksToBounds=YES;
    btnProfile.layer.cornerRadius=btnProfile.frame.size.width/2.0;
    [self initializeMBProgres:self.view];
    [self updateUIProfile];
    
    txtName.placeholder=@"Name";
    txtEmail.placeholder=@"Email";
    txtFemale.placeholder=@"Gender";
    txtviewDescription.placeholderText=@"Description";

    txtEmail.userInteractionEnabled=NO;
    
    
    
    
    btnProfile.layer.cornerRadius = btnProfile.frame.size.width/2;
    [txtName setValue:[UIColor blackColor]
           forKeyPath:@"_placeholderLabel.textColor"];
    [txtEmail setValue:[UIColor blackColor]
            forKeyPath:@"_placeholderLabel.textColor"];
    
    [txtFemale setValue:[UIColor blackColor]
             forKeyPath:@"_placeholderLabel.textColor"];
    
    [txtviewDescription setValue:[UIColor blackColor]
                      forKeyPath:@"_placeholderLabel.textColor"];
    
    
    
    // Do any additional setup after loading the view.
}
-(void)updateUIProfile
{
    AppDelegate *objapdelegate= [AppDelegate sharedAppdelegate];
    txtEmail.text =objapdelegate.profile.email;
    if(objapdelegate.profile.gender!=nil)
    {
        txtFemale.text =objapdelegate.profile.gender;

    }
    

    txtName.text=objapdelegate.profile.fullname;
    txtviewDescription.text=objapdelegate.profile.description_profile;
    UIImageView *localimageview=[[UIImageView alloc]init];
    localimageview.image =[UIImage imageNamed:@"profile_pic.png"];
    NSLog(@"localimgae=%@",objapdelegate.profile.profile_pic_url);
    
    [localimageview sd_setImageWithURL:objapdelegate.profile.profile_pic_url placeholderImage:[UIImage imageNamed:@"profile_pic.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image==nil) {
            [btnProfile setImage:[UIImage imageNamed:@"profile_pic.png"] forState:UIControlStateNormal];

        }
        else{
            [btnProfile setImage:image forState:UIControlStateNormal];

        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)generClick:(id)sender {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Choose a Gender:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Male",
                            @"Female",
                            nil];
    popup.backgroundColor = [UIColor whiteColor];
    popup.tag=1002;
    [popup showInView:self.view];
    
}


- (IBAction)btnProfileClick:(id)sender {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Choose a source:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Camera",
                            @"Gallery",
                            nil];
    popup.backgroundColor = [UIColor whiteColor];
    [popup showInView:self.view];
}

#pragma mark- text View delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark-
#pragma mark-TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:txtName]) {
        [txtName resignFirstResponder];
    }
    else if ([textField isEqual:txtEmail]) {
        [txtFemale becomeFirstResponder];
    }
    else if ([textField isEqual:txtFemale]) {
        [txtviewDescription becomeFirstResponder];
    }
    
    return  NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
}
- (IBAction)backClick:(id)sender {
    
    POP;
}


-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
- (IBAction)saveClick:(id)sender {
    if ([self validate]) {
        [self ShowProgress];
        
        
        NSDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,Update_USERPROFILE_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        
        [dict setValue:txtName.text forKey:@"first_name"];
        //        [dict setValue:txtEmail.text forKey:@"email"];
        [dict setValue:txtFemale.text forKey:@"gender"];
        [dict setValue:txtviewDescription.text forKey:@"description"];
        if (selectImage==nil) {
            selectImage =btnProfile.imageView.image;
        }
        Base64Transcoder *objBase64=[[Base64Transcoder alloc]init];
        NSData *data=UIImageJPEGRepresentation(selectImage, 0.5);
        NSString *str= [objBase64 base64EncodedStringfromData:data];
        [dict setValue:str forKey:@"profile_pic"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];
                [NS_NOTIFICATION postNotificationName:NOTIFICATION_PROFILEUPDATE object:nil];
            }
            else
            {
                [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];
    }
}


-(BOOL)validate
{
    NSError *error = NULL;
    
    
    //    simple
    if (txtName.text.length==0){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    //    else if (txtEmail.text.length==0) {
    //        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    //        return false;
    //    }
    //   else if (![self NSStringIsValidEmail:txtEmail.text]) {
    //        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    //        return false;
    //    }
    else if (txtviewDescription.text.length==0) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter description" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    
    
    
    
    return true;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


#pragma marka Image Picker Delegate
/*!
 @brief Performed when Image choosed/clicked
 
 @discussion Load the respective things when Image choosed/clicked
 
 @param sender
 
 @return nothing
 */
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [photoPicker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    
    [btnProfile setImage:chosenImage forState:UIControlStateNormal];
    selectImage=chosenImage;
    isImageupload=YES;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    [photoPicker dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark -  Action Sheet Methods


/*!
 @brief Performed when Action sheet dismissed
 
 @discussion Load the respective things when Action sheet dismissed
 
 @param sender
 
 @return nothing
 */
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==1002) {
         if(buttonIndex == 0) {
             txtFemale.text=@"Male";
         }
         else if(buttonIndex == 1){
             txtFemale.text=@"Female";
         }
    }
    else{
        photoPicker = [[UIImagePickerController alloc] init];
        
        photoPicker.delegate = self;
        
        photoPicker.allowsEditing = YES;
        if(buttonIndex == 0) {
            
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                
                
                
                photoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                    
                    UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:photoPicker];
                    
                    [popover presentPopoverFromRect:btnProfile.bounds inView:self.view permittedArrowDirections:
                     
                     UIPopoverArrowDirectionAny animated:YES];
                    
                    popOver = popover;
                    
                } else {
                    
                    [self presentViewController:photoPicker animated:YES completion:NULL];
                    
                }
                
            }
            
            else{
                
            }
        }
        
        else if (buttonIndex == 1){
            
            photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                
                UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:photoPicker];
                
                [popover presentPopoverFromRect:btnProfile.bounds inView:self.view permittedArrowDirections:
                 
                 UIPopoverArrowDirectionAny animated:YES];
                
                popOver = popover;
                
            } else {
                
                [self presentViewController:photoPicker animated:YES completion:NULL];
                
            }
            
        }
    }
}




#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
