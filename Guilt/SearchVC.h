//
//  SearchVC.h
//  Guilt
//
//  Created by Gourav Sharma on 11/2/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchVC : UIViewController{
    
    __weak IBOutlet UIButton *btnCross;
    IBOutlet UISearchBar *searchBar;
    __weak IBOutlet UIImageView *imageview;
    __weak IBOutlet UIScrollView *scrollview;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;

@end
