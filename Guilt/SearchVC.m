//
//  SearchVC.m
//  Guilt
//
//  Created by Gourav Sharma on 11/2/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "SearchVC.h"
#import "ImageCell.h"
#import "ModelProduct.h"
#import "UIImageView+WebCache.h"
#import "Addmorecell.h"
#import "webVC.h"
#import "Model/ModelTag.h"
#import "Model/ModelGuilt.h"
#import "StoreItemVC.h"
@interface SearchVC ()
{
    NSMutableArray *arrayProduct,*newDataarray,*searcharrayProduct;
     MBProgressHUD *HUD,*HUDToast;
}
@end

@implementation SearchVC
- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Set Title
        UIImage *musicImage = [UIImage imageNamed:@"search_blk"];
        UIImage *musicImageSel = [UIImage imageNamed:@"search_wht"];
        
        musicImage = [musicImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        musicImageSel = [musicImageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        self.navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"SEARCH" image:musicImage selectedImage:musicImageSel];
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor blackColor], NSForegroundColorAttributeName,[UIFont fontWithName:@"OSWALD" size:10], UITextAttributeFont,
                                                           nil] forState:UIControlStateNormal];
        
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    arrayProduct=[[NSMutableArray alloc]init];
    newDataarray=[[NSMutableArray alloc]init];
    [self initializeMBProgres:self.view];
    
    [self callApi:@""];
    //[self fetchSearchResult];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

/**
 *  Fetch result of search
 */
-(void)callApi:(NSString*)SEARCHSTRING
{
    [self ShowProgress];
    NSMutableDictionary *dict;
    NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,SEARCH_GUILT_API];
    dict=[[NSMutableDictionary alloc]init];
    [dict setValue:TOKEN forKey:@"token"];
    [dict setValue:SEARCHSTRING forKey:@"search_text"];
    //hardcode
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager setRequestSerializer:requestSerializer];
    [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self removeProgress];
        [arrayProduct removeAllObjects];
        if ([[responseObject valueForKey:@"status"]  intValue] == 1)
        {
            
            
            if ([responseObject valueForKey:@"data"]!=nil  && ![[responseObject valueForKey:@"data"] isKindOfClass:[NSNull class]]) {
                for (id DictfoundGuilt in [responseObject valueForKey:@"data"]) {
                    ModelGuilt *objGuilt=[[ModelGuilt alloc]init];
                    objGuilt.guilt_id=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"guilt_id"]];
                    objGuilt.descriptionGuilt=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"description"]];
                    objGuilt.user_id=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"user_id"]];
                    if ([DictfoundGuilt valueForKey:@"price"]!=nil) {
                        NSString *strPrice=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"price"]];
                        objGuilt.price=[strPrice stringByReplacingOccurrencesOfString:@"$" withString:@""];
                        objGuilt.price=[NSString stringWithFormat:@"$ %@",objGuilt.price];
                    }
                    
                      objGuilt.title=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"title"]];
                    if ([[DictfoundGuilt valueForKey:@"found"]  intValue] == 1)
                    {
                        objGuilt.found=YES;
                    }
                    else{
                        objGuilt.found=NO;
                        
                    }
                    
                    if ([[DictfoundGuilt valueForKey:@"guilt_share"]  intValue] == 1)
                    {
                        objGuilt.isShare=YES;
                    }
                    else{
                        objGuilt.isShare=NO;
                        
                    }
                    
                    if ([DictfoundGuilt valueForKey:@"gender"]!=nil) {
                        NSString *str=[[DictfoundGuilt valueForKey:@"gender"] lowercaseString];
                        if ([str isEqualToString:@"male"]) {
                            objGuilt.ismale=YES;
                        }
                        else
                        {
                            objGuilt.ismale=NO;
                        }
                        
                        
                    }
                    objGuilt.image_url= [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[DictfoundGuilt valueForKey:@"image"]]];
                    objGuilt.found_link=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"found_link"]];
                    objGuilt.guilt_comment_count=[[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"comment_count"]]  intValue];
                    
                    if ([[DictfoundGuilt valueForKey:@"user_like"]  intValue] == 1)
                    {
                        objGuilt.isUserLike=YES;
                    }
                    else{
                        objGuilt.isUserLike=NO;
                        
                    }
                    
                    //------------------------User data--------------------------//
                    NSDictionary *dictUser=[DictfoundGuilt valueForKey:@"user"];
                    objGuilt.objProfile=[[ModelProfile alloc]init];
                    if ([dictUser valueForKey:@"address"]!=nil && ![[dictUser valueForKey:@"address"] isKindOfClass:[NSNull class]]) {
                        objGuilt.objProfile.address= [NSString stringWithFormat:@"%@",[dictUser valueForKey:@"address"]];

                    }
                    if ([dictUser valueForKey:@"first_name"]!=nil && ![[dictUser valueForKey:@"first_name"] isKindOfClass:[NSNull class]]) {
                        objGuilt.objProfile.fullname= [NSString stringWithFormat:@"%@",[dictUser valueForKey:@"first_name"]];
                        
                    }
                    if ([dictUser valueForKey:@"email"]!=nil && ![[dictUser valueForKey:@"email"] isKindOfClass:[NSNull class]]) {
                        objGuilt.objProfile.email= [NSString stringWithFormat:@"%@",[dictUser valueForKey:@"email"]];
                        
                    }
                   
                    
                    if ([dictUser valueForKey:@"user_description"]!=nil && ![[dictUser valueForKey:@"user_description"] isKindOfClass:[NSNull class]]) {
                        objGuilt.objProfile.description_profile= [NSString stringWithFormat:@"%@",[dictUser valueForKey:@"user_description"]];
                        
                    }

                    
                    else{
                        objGuilt.objProfile.description_profile=@"";
                    }
                    
                    
                    objGuilt.objProfile.profile_pic_url= [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[dictUser valueForKey:@"user_profile_pic"]]];
                    objGuilt.objProfile.age= [[NSString stringWithFormat:@"%@",[dictUser valueForKey:@"age"]] intValue];
                    if ([dictUser valueForKey:@"gender"]!=nil && ![[dictUser valueForKey:@"gender"] isKindOfClass:[NSNull class]]) {
                        NSString *str=[[dictUser valueForKey:@"gender"] lowercaseString];
                        if ([str isEqualToString:@"male"]) {
                            objGuilt.objProfile.ismale=YES;
                        }
                        else
                        {
                            objGuilt.objProfile.ismale=NO;
                        }
                        
                        
                    }
                    
                    
                    //Tag array
                    if ([DictfoundGuilt valueForKey:@"tags"]!=nil) {
                        NSArray *arrayTag=[DictfoundGuilt valueForKey:@"tags"];
                        objGuilt.arrayTag=[[NSMutableArray alloc]init];
                        for (id dictTag in arrayTag) {
                            ModelTag *objTag=[[ModelTag alloc]init];
                            if ([dictTag valueForKey:@"tag_id"]!=nil) {
                                
                                objTag.tag_id=[dictTag valueForKey:@"tag_id"];
                            }
                            if ([dictTag valueForKey:@"guilt_id"]!=nil) {
                                
                                objTag.guilt_id=[dictTag valueForKey:@"guilt_id"];
                            }
                            if ([dictTag valueForKey:@"tag"]!=nil) {
                                
                                objTag.tag=[dictTag valueForKey:@"tag"];
                            }
                            if ([dictTag valueForKey:@"type"]!=nil) {
                                
                                objTag.type=[dictTag valueForKey:@"type"];
                            }
                            [objGuilt.arrayTag addObject:objTag ];
                        }
                    }
                    
                    
                    //Color array
                    if ([DictfoundGuilt valueForKey:@"color"]!=nil) {
                        NSArray *arrayColor=[DictfoundGuilt valueForKey:@"color"];
                        objGuilt.arrayColor=[[NSMutableArray alloc]init];
                        for (id dictTag in arrayColor) {
                            ModelColor *objTag=[[ModelColor alloc]init];
                            if ([dictTag valueForKey:@"id"]!=nil) {
                                
                                objTag.idcolor=[dictTag valueForKey:@"id"];
                            }
                            if ([dictTag valueForKey:@"color"]!=nil) {
                                
                                objTag.name=[dictTag valueForKey:@"color"];
                            }
                            if ([dictTag valueForKey:@"guilt_id"]!=nil) {
                                
                                objTag.guiltid=[dictTag valueForKey:@"guilt_id"];
                            }
                            
                            [objGuilt.arrayColor addObject:objTag ];
                        }
                    }
                    
                    
                    [arrayProduct addObject:objGuilt];
                    
                    
                }
            }
            
        }
        else
        {
            if ([responseObject valueForKey:@"data"]!=nil) {
                if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                    
                }
                else{
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                    
                }
            }
            
        }
        [self.collectionview reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self removeProgress];
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        
        
    }];
    
}
-(void)parseData:(id)responseObject
{
    [arrayProduct removeAllObjects];
    
    if ([responseObject valueForKey:@"products"] !=nil)
    {
        NSLog(@"PRODUCTS=%@",[responseObject valueForKey:@"products"]);
        if ([[responseObject valueForKey:@"products"] count]==0) {
            [APPDELEGATE showToast:@"No product Found"];
        }
        for (id dictProduct in [responseObject valueForKey:@"products"]) {
            ModelProduct *objModelProdct=[[ModelProduct alloc]init];
            
            if ([dictProduct valueForKey:@"name"]!=nil) {
                objModelProdct.name=[dictProduct valueForKey:@"name"];
            }
            if ([dictProduct valueForKey:@"id"]!=nil) {
                objModelProdct.idProduct=[dictProduct valueForKey:@"id"];
                
            }
            
            if ([dictProduct valueForKey:@"brand"]!=nil) {
                NSDictionary *dictBrand=[dictProduct valueForKey:@"brand"];
                objModelProdct.brand=[dictBrand valueForKey:@"name"];
                
            }
            if ([dictProduct valueForKey:@"clickUrl"]!=nil) {
                objModelProdct.productUrl=[NSURL URLWithString:[dictProduct valueForKey:@"clickUrl"]];
            }
            if ([dictProduct valueForKey:@"image"]!=nil) {
                if ([[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]!=nil) {
                    if ([[[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]valueForKey:IMAGESize_Iphone]!=nil) {
                        objModelProdct.imageurl=[NSURL URLWithString:[[[[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]valueForKey:IMAGESize_Iphone] valueForKey:@"url"]];
                    }
                }
            }
            [arrayProduct addObject:objModelProdct];
        }
    }
    [self.collectionview reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrayProduct.count;
}

- (NSInteger)numberOfSections
{
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    
    
    ImageCell *cell;
    
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//    [cell.ivProfile updateConstraints];
    cell.tagscontrol.backgroundColor=[UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];

    cell.ivProfile.layer.masksToBounds=YES;
    cell.ivProfile.layer.cornerRadius = cell.ivProfile.frame.size.width/2.0;
    cell.ivProfile.layer.borderWidth = 1;
    cell.ivProduct.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    
    tapGesture1.numberOfTapsRequired = 1;
    
    [tapGesture1 setDelegate:self];
    
    [cell.ivProduct addGestureRecognizer:tapGesture1];
    cell.ivProduct.tag=indexPath.row;
    
    
    cell.ivProfile.userInteractionEnabled=YES;
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesturetwo:)];
    
    tapGesture2.numberOfTapsRequired = 1;
    
    [tapGesture2 setDelegate:self];
    
    [cell.ivProduct addGestureRecognizer:tapGesture1];
    cell.ivProduct.tag=indexPath.row;


    
    
    [cell.tagscontrol setMode:TLTagsControlModeList];

    
    ModelGuilt *objguilt=[arrayProduct objectAtIndex:indexPath.row];
    [cell.ivProduct sd_setImageWithURL:objguilt.image_url placeholderImage:[UIImage imageNamed:@"placeholder"]];
    cell.lblName.text=objguilt.objProfile.fullname;
    if (objguilt.isUserLike) {
        //cell.bt
    }
    else{
        
    }
    [cell.ivProfile sd_setImageWithURL:objguilt.objProfile.profile_pic_url placeholderImage:[UIImage imageNamed:@"user.png"]];
    
    [cell.ivProduct sd_setImageWithURL:objguilt.image_url placeholderImage:nil];
    [cell.tagscontrol.tags removeAllObjects];

    for (ModelTag *objtag in objguilt.arrayTag) {
        [cell.tagscontrol addTag:[NSString stringWithFormat:@"#%@",objtag.tag]];
    }
    
    for (int i=0; i<[objguilt.arrayColor count]; i++) {
        ModelColor *objcolor=[objguilt.arrayColor objectAtIndex:i];
        if (i==0) {
            [cell.btncolor1 setBackgroundColor:[self getColorwithname:objcolor.name]];
        }
        else if (i==1)
        {
            [cell.btncolor2 setBackgroundColor:[self getColorwithname:objcolor.name]];
            
        }
        else if (i==2)
        {
            [cell.btncolor3 setBackgroundColor:[self getColorwithname:objcolor.name]];
            
        }
        else if (i==3)
        {
            [cell.btncolor4 setBackgroundColor:[self getColorwithname:objcolor.name]];
            
        }
        
    }

    
    
    
    return cell;
    
    
    

}

- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake([[UIScreen mainScreen] bounds].size.width /2-8, ([[UIScreen mainScreen] bounds].size.width/2-8)*1.5 );
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    ModelGuilt *objModelProduct=[arrayProduct objectAtIndex:indexPath.row];
    
    StoreItemVC *objVC=[self.storyboard instantiateViewControllerWithIdentifier:@"StoreItemVC"];
    objVC.objGuilt=objModelProduct;
    objVC.navigation_type=@"search";
    [self.navigationController pushViewController:objVC animated:YES];
    
    
//    webVC *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"webVC"];
//    obj.urlString=objModelProduct.productUrl;
//    [self.navigationController pushViewController:obj animated:YES];
    
}

- (void) tapGesture: (id)sender
{
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    
//   ImageCell *cell = [self.collectionview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:tapRecognizer.view.tag inSection:0]];
    
    
                      ImageCell *cell=(ImageCell*)[self.collectionview cellForItemAtIndexPath:[NSIndexPath indexPathForRow:tapRecognizer.view.tag inSection:0]];
    imageview.image=cell.ivProduct.image;
    
    //-- Show view.
    scrollview.hidden=NO;
    scrollview.transform = CGAffineTransformMakeScale(0.01, 0.01);
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        scrollview.transform = CGAffineTransformIdentity;
         btnCross.hidden=NO;
    } completion:^(BOOL finished){
        // do something once the animation finishes, put it here
    }];
    
}
- (void) tapGesturetwo: (id)sender
{
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;

    ModelGuilt *objModelProduct=[arrayProduct objectAtIndex:tapRecognizer.view.tag];
    
    StoreItemVC *objVC=[self.storyboard instantiateViewControllerWithIdentifier:@"StoreItemVC"];
    objVC.objGuilt=objModelProduct;
    objVC.navigation_type=@"search";
    [self.navigationController pushViewController:objVC animated:YES];
}


- (IBAction)crossClick:(id)sender {
    btnCross.hidden = YES;
    scrollview.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        scrollview.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished){
        scrollview.hidden = YES;
        
        scrollview.zoomScale=1.0;
    }];
}

-(void)addMoreSearchData :(int)offset
{
    NSString *url =[NSString stringWithFormat:@"%@%@%@&offset=%@&limit=%@",kBaseUrl_Live,PRODUCTS_API,APITokenURL_Product,[NSString stringWithFormat:@"%d",offset],@"10"];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject valueForKey:@"products"] !=nil)
        {
            NSLog(@"PRODUCTS=%@",[responseObject valueForKey:@"products"]);
            [newDataarray removeAllObjects ];
            for (id dictProduct in [responseObject valueForKey:@"products"]) {
                ModelProduct *objModelProdct=[[ModelProduct alloc]init];
                
                if ([dictProduct valueForKey:@"name"]!=nil) {
                    objModelProdct.name=[dictProduct valueForKey:@"name"];
                }
                if ([dictProduct valueForKey:@"id"]!=nil) {
                    objModelProdct.idProduct=[dictProduct valueForKey:@"id"];
                    
                }
                if ([dictProduct valueForKey:@"brand"]!=nil) {
                    NSDictionary *dictBrand=[dictProduct valueForKey:@"brand"];
                    objModelProdct.brand=[dictBrand valueForKey:@"name"];
                    
                }
                if ([dictProduct valueForKey:@"clickUrl"]!=nil) {
                    objModelProdct.productUrl=[NSURL URLWithString:[dictProduct valueForKey:@"clickUrl"]];
                }
                if ([dictProduct valueForKey:@"image"]!=nil) {
                    if ([[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]!=nil) {
                        if ([[[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]valueForKey:IMAGESize_Iphone]!=nil) {
                            objModelProdct.imageurl=[NSURL URLWithString:[[[[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]valueForKey:IMAGESize_Iphone] valueForKey:@"url"]];
                        }
                    }
                }
                [newDataarray addObject:objModelProdct];
            }
        }
        [self addNewCells];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    }];
    
//    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        
//        
//        
//        //        [self.collectionview reloadData];
//        
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//       
//        
//    }];
}
-(void)addNewCells {
    [self.collectionview performBatchUpdates:^{
        int resultsSize = [arrayProduct count];
        [arrayProduct addObjectsFromArray:newDataarray];
        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
        for (int i = resultsSize; i < resultsSize + newDataarray.count; i++)
        {
            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
        [self.collectionview insertItemsAtIndexPaths:arrayWithIndexPaths];
    }
                                  completion:nil];
    
    
}


-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return imageview;
}
#pragma mark - SearchBar Delegate


/*!
 @brief search bar handler is called when text did begins editing
 
 @discussion search bar handler is called when text did begins editing
 
 @param UISearchBar
 
 @return nothing
 */
- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar
{
    searchBar.showsCancelButton = NO;
}



/*!
 @brief search bar handler is called when text did change in the search bar
 
 @discussion search bar handler is called when text did change in the search bar
 
 @param UISearchBar
 
 @return nothing
 */
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    // Call When text edit
    if(![searchBar isFirstResponder]) {
        // user tapped the 'clear' button
        [searchBar resignFirstResponder];

        // do whatever I want to happen when the user clears the search...
    }
    if (searchText.length==0) {
        [searchBar resignFirstResponder];
    }
    
    
    // Call createDataSource method
    
}






/*!
 @brief search bar handler is called when the cancel button of search bar is clicked
 
 @discussion search bar handler is called when the cancel button of search bar is clicked
 
 @param UISearchBar
 
 @return nothing
 */
- (void)searchBarCancelButtonClicked:(UISearchBar *) theSearchBar
{
    
    searchBar.showsCancelButton = NO;
    
    [searchBar resignFirstResponder];
    
    searchBar.text = @"";
    
    [self callApi:@""];
    
    
    
    // Reload Data
    
    
    //    [self createDataSource];
    
    // [mTableView reloadData];
}


/*!
 @brief search bar handler is called when it's search button is clicked
 
 @discussion search bar handler is called to load the results
 
 @param UISearchBar
 
 @return nothing
 */
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    [theSearchBar resignFirstResponder];
    [self.view endEditing:TRUE];
    if ([searcharrayProduct count] >0)
    {
        [searcharrayProduct removeAllObjects];
    }
    if([searchBar.text length] == 0) {
        
        [self callApi:@""];
        
        [searchBar resignFirstResponder];
    }
    
    if([searchBar.text length] > 0)
    {
        [self callApi:searchBar.text];
        
    }
    //This will dismiss the keyboard
    //[mTableView reloadData];
}


/*!
 @brief Search the contacts with keyword
 
 @discussion Search the contacts with keyword
 
 @param nothing
 
 @return nothing
 */
- (void) searchTableView
{
    NSString *searchText = searchBar.text;
    
}





#pragma mark MBProgress
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}

-(UIColor*)getColorwithname:(NSString*)colorname
{
    if ([colorname isEqualToString:@"Orange"]) {
        return [UIColor orangeColor];
    }
    else if ([colorname isEqualToString:@"Brown"])
    {
        return [UIColor brownColor];
        
    }
    else if ([colorname isEqualToString:@"Yellow"])
    {
        return [UIColor yellowColor];
        
    }
    else if ([colorname isEqualToString:@"Red"])
    {
        return [UIColor redColor];
        
    }
    else if ([colorname isEqualToString:@"Purple"])
    {
        return [UIColor purpleColor];
        
    }
    else if ([colorname isEqualToString:@"Blue"])
    {
        return [UIColor blueColor];
        
    }
    else if([colorname isEqualToString:@"Green"])
    {
        return [UIColor greenColor];
        
    }
    else if([colorname isEqualToString:@"Gray"])
    {
        return [UIColor grayColor];
        
    }
    else if([colorname isEqualToString:@"White"])
    {
        return [UIColor whiteColor];
        
    }
    else if ([colorname isEqualToString:@"Black"])
    {
        return [UIColor blackColor];
        
    }
    else if([colorname isEqualToString:@"Pink"])
    {
        //Pink color 255-192-203
        return [UIColor colorWithRed:255.0/255.0 green:192.0/255.0 blue:203.0/255.0 alpha:1.0];
        
    }
    else if([colorname isEqualToString:@"Gold"])
    {
        //Golden color 255-215-0
        return [UIColor colorWithRed:255.0/255.0 green:215.0/255.0 blue:0.0/255.0 alpha:1.0];
        
    }
    else if([colorname isEqualToString:@"Silver"])
    {
        //Silver color 192-192-192
        return [UIColor colorWithRed:192.0/255.0 green:192.0/255.0 blue:192.0/255.0 alpha:1.0];
    }
    else if([colorname isEqualToString:@"Beige"])
    {
        //Silver color 192-192-192
        return [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:220.0/255.0 alpha:1.0];
    }
    else if([colorname isEqualToString:@"Rose"])
    {
        //Silver color 192-192-192
        return [UIColor colorWithRed:255.0/255.0 green:102.0/255.0 blue:204.0/255.0 alpha:1.0];
    }
    else{
        return [UIColor whiteColor];
    }
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
