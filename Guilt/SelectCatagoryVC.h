//
//  SelectCatagoryVC.h
//  Guilt
//
//  Created by Gourav Sharma on 10/28/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageCropView.h"
@interface SelectCatagoryVC : UIViewController
{
    
    IBOutlet UISegmentedControl *objSegment;
    IBOutlet UICollectionView *objCollectonview;
    ImageCropView* imageCropView;
    UIImage* imageSelecteed;
    UIImageView *imageView;
}

@end
