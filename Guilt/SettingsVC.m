//
//  SettingsVC.m
//  Guilt
//
//  Created by Gourav Sharma on 11/2/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "SettingsVC.h"
#import "TablecellSetting.h"
#import "Utility.h"
#import "LoginVC.h"
#import "UserProfileVC.h"
@interface SettingsVC ()<UIActionSheetDelegate>
{
    NSMutableArray *arraySettings;
    NSMutableArray *arraySettingsIcons;
    MBProgressHUD *HUD,*HUDToast;

}
@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeMBProgres:self.view];

    self.tabBarController.hidesBottomBarWhenPushed=YES;
    self.tabBarController.tabBar.hidden=YES;
    arraySettings =[[NSMutableArray alloc]initWithObjects:@"PERSONAL INFORMATION",@"PASSWORD CHANGE", @"ABOUT US",@"MEET THE FOUNDER",@"NOTIFICATIONS",@"DELETE ACCOUNT",@"NEWSLETTER",@"VISIT WEBSITE",@"CONTACT US",@"TERMS AND CONDITIONS",@"PRIVACY POLICY",nil];
     arraySettingsIcons =[[NSMutableArray alloc]initWithObjects:@"prsnl_info",@"lock", @"about_us",@"user",@"notification_settingScreen",@"cross_black",@"news_letter",@"visit_website",@"contact_us",@"terms",@"privacy_lock",nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backClick:(id)sender {
    POP
}
- (IBAction)saveClick:(id)sender {
}
- (IBAction)clcikLogout:(id)sender {
    NSArray *arr=self.navigationController.viewControllers;
    for (int i=0;i<[arr count];i++) {
        UIViewController *view=[arr objectAtIndex:i];
        if ([view isKindOfClass:[UserProfileVC class]]) {
            [NS_NOTIFICATION postNotificationName:NOTIFICATION_LOGOUT object:nil];

            [self.navigationController popToViewController:view animated:NO];
        }
        
    }
}


#pragma mark Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arraySettings.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    TablecellSetting *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[TablecellSetting alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.lblSetting.text = [arraySettings objectAtIndex:indexPath.row];
    cell.ivIcon.image =[UIImage imageNamed:[arraySettingsIcons objectAtIndex:indexPath.row]];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
//    if (indexPath.row ==5) {
//        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
//                                @"Share on Facebook",
//                                @"Share on Instagram",
//                                @"Share on Twitter",
//                                @"Share on Google Plus",
//                                nil];
//        popup.tag = 1;
//        [popup showInView:[UIApplication sharedApplication].keyWindow];
//        
//    }
    if (indexPath.row ==0) {
        [self performSegueWithIdentifier:@"profile" sender:self];
        
    }
        else if (indexPath.row ==1) {
             [self performSegueWithIdentifier:@"password" sender:self];
              
              }
        else if (indexPath.row ==2) {
            [self performSegueWithIdentifier:@"about" sender:self];
            
        }
        else if (indexPath.row ==3) {
            [self performSegueWithIdentifier:@"Meet" sender:self];
            
        }
        else if (indexPath.row ==4) {
            [self performSegueWithIdentifier:@"notification" sender:self];
            
        }
        else if (indexPath.row ==5) {
           
            
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Guilt" message:@"Do you want delete account.?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            alert.tag=101;
            [alert show];
            
        }
        else if (indexPath.row ==6) {
            [self performSegueWithIdentifier:@"newsleter" sender:self];
            
        }
        else if (indexPath.row ==8) {
            [self performSegueWithIdentifier:@"CONTACTUS" sender:self];
            
        }
        else if (indexPath.row ==10) {
            [self performSegueWithIdentifier:@"PRIVACY" sender:self];
            
        }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex==1) {
        
        
        [self ShowProgress];
        
        
        NSMutableDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,DELETE_ACCOUNT_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        
        
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                
                [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];
                NSArray *arr=self.navigationController.viewControllers;
                for (int i=0;i<[arr count];i++) {
                    UIViewController *view=[arr objectAtIndex:i];
                    if ([view isKindOfClass:[UserProfileVC class]]) {
                        [NS_NOTIFICATION postNotificationName:NOTIFICATION_LOGOUT object:nil];
                        
                        [self.navigationController popToViewController:view animated:NO];
                    }
                    
                }
                
            }
            else
            {
                  [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];
                if ([responseObject valueForKey:@"data"]!=nil) {
                    if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                        
                    }
                    else{
                        [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                        
                    }
                }
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];
        
    }
    else{
       
    }
    
}


- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    }


#pragma mark MBProgress
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
