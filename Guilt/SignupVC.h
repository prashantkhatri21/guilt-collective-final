//
//  SignupVC.h
//  Guilt
//
//  Created by Gourav Sharma on 11/2/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface SignupVC : UIViewController
{
    
    IBOutlet UIButton *buttonProfile;
    IBOutlet UIView *viewname;
    IBOutlet UIView *viewPasswordConfirm;
    IBOutlet UIView *viewPassword;
    IBOutlet UIView *VIEWEMAIL;
    IBOutlet CustomTextField *txtName;
    IBOutlet CustomTextField *txtPasswordConfirm;
    IBOutlet CustomTextField *txtPassword;
    IBOutlet CustomTextField *txtEmail;
}
@end
