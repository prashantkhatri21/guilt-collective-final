//
//  SignupVC.m
//  Guilt
//
//  Created by Gourav Sharma on 11/2/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "SignupVC.h"
#import "Utility.h"
#import "Base64Transcoder.h"
#import "AFNetworking.h"
@interface SignupVC ()<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,WebHelperDelegate>
{
    BOOL isImageupload;
    UIImagePickerController *photoPicker;
    UIPopoverController *popOver;
    UIImage *selectImage;
}
@end

@implementation SignupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    txtEmail.tintColor = [UIColor blackColor];
    txtPassword.tintColor = [UIColor blackColor];
    txtPasswordConfirm.tintColor=[UIColor blackColor];
    txtName.tintColor=[UIColor blackColor];
    INITIALIZE_LOADER
    WEBHELPER.WebHelperDelegate= self;
    VIEWEMAIL.layer.borderColor = [UIColor blackColor].CGColor;
    viewname.layer.borderColor = [UIColor blackColor].CGColor;
    viewPassword.layer.borderColor = [UIColor blackColor].CGColor;
    
    viewPasswordConfirm.layer.borderColor = [UIColor blackColor].CGColor;
    VIEWEMAIL.layer.borderWidth = 2.0;
    VIEWEMAIL.layer.cornerRadius =5.0;
    viewname.layer.borderWidth = 2.0;
    viewname.layer.cornerRadius =5.0;
    viewPassword.layer.borderWidth = 2.0;
    viewPassword.layer.cornerRadius =5.0;
    viewPasswordConfirm.layer.borderWidth = 2.0;
    viewPasswordConfirm.layer.cornerRadius =5.0;
    [txtEmail setValue:[UIColor grayColor]
            forKeyPath:@"_placeholderLabel.textColor"];
    
    [txtName setValue:[UIColor grayColor]
           forKeyPath:@"_placeholderLabel.textColor"];
    
    [txtPassword setValue:[UIColor grayColor]
               forKeyPath:@"_placeholderLabel.textColor"];
    [txtPasswordConfirm setValue:[UIColor grayColor]
                      forKeyPath:@"_placeholderLabel.textColor"];
    // Do any additional setup after loading the view.
    [self setFontFamily:@"monofonto" forView:self.view andSubViews:YES];
    
    // Do any additional setup after loading the view.
}

-(void)setFontFamily:(NSString*)fontFamily forView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        [lbl setFont:[UIFont fontWithName:fontFamily size:[[lbl font] pointSize]]];
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontFamily:fontFamily forView:sview andSubViews:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)signupClick:(id)sender {
    if ([self validate]) {
        if (![APPDELEGATE isNetwork]) {
            NETWORKALERT
        }
        else{
            [APPDELEGATE ShowProgress];
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [dict setValue:TOKEN forKey:@"token"];
            [dict setValue:txtName.text forKey:@"name"];
            [dict setValue:txtEmail.text forKey:@"email"];
            [dict setValue:txtPassword.text forKey:@"password"];
            
            
            Base64Transcoder *objBase64=[[Base64Transcoder alloc]init];
            NSData *data=UIImageJPEGRepresentation(selectImage, 0.5);
            NSString *str= [objBase64 base64EncodedStringfromData:data];
            
            //            NSString *stringplusadd =[NSString stringWithFormat:@"+%@",str];
            //            NSCharacterSet *set= [NSCharacterSet characterSetWithCharactersInString:@"=\"#%/<>?@\\^`{|}+-!"];
            //            NSString *convertedstring =[stringplusadd stringByAddingPercentEncodingWithAllowedCharacters:set];
            //
            //            [dict setValue:convertedstring forKey:@"profile_pic"];
            //            NSString *imgbase64String = str;
            
            NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                                    TOKEN, @"token",
                                    str, @"profile_pic",
                                    txtName.text,@"name",
                                    txtEmail.text,@"email",
                                    txtPassword.text,@"password",
                                    @"0",@"is_social",
                                    nil];
            
            NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,REGISTER_API];
            
            
            AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
            AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
            
            [manager setRequestSerializer:requestSerializer];
            
            [manager POST:url parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                [APPDELEGATE removeProgress];
                
                
                if ([[responseObject valueForKey:@"status"]  intValue] == 1)
                {
                    //        [APPDELEGATE showAlert:@"" message:[responsedict valueForKey:@"message"]];
                    if ([responseObject valueForKey:@"info"]!=nil) {
                        NSDictionary *dictInfo =[responseObject valueForKey:@"info"];
                        if ([[dictInfo valueForKey:@"message"] isEqualToString:@"register successfully"]) {
                            [APPDELEGATE showAlert:@"" message:@"Registration Successful"];
                            
                        }
                        else
                        {
                            [APPDELEGATE showAlert:@"" message:[dictInfo valueForKey:@"message"]];
                            
                        }
                        //                        [APPDELEGATE showAlert:@"" message:[dictInfo valueForKey:@"message"]];
                        [self dismissViewControllerAnimated:YES completion:nil];
                        
                    }
                }
                else if ([[responseObject valueForKey:@"status"]  intValue] == 0)
                {
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];
                    
                }
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [APPDELEGATE removeProgress];
                
                [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            }];
            //            [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //
            //
            //            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //
            //
            //            }];
            
        }
        
    }
    
}
- (IBAction)clickBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(BOOL)validate
{
    
    
    //    simple
    
    if (txtEmail.text.length==0) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    if (![self NSStringIsValidEmail:txtEmail.text]) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter valid email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    else if (txtPassword.text.length==0){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    
    else if (![self Passwordvalidation]){
        
        return false;
    }
    
    
    else if (txtPasswordConfirm.text.length==0){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter confirm password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    else if (![txtPasswordConfirm.text  isEqualToString:txtPassword.text]){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Password and confirm must be same" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    else if (txtName.text.length==0){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter name" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    else if (!isImageupload){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please upload image" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    return true;
}

-(BOOL)Passwordvalidation
{
    if([txtPassword.text length] <= 16 && [txtPassword.text length] >= 8)
    {
        return YES;
        //        NSString *Pattern = @"(?=.*[@#$%])";
        //        if ([self string:txtPassword.text matches:Pattern] )
        //        {
        //            NSLog(@"PW is valid");
        //            return YES;
        //        }
        //
        //        else
        //        {
        //            [APPDELEGATE showAlert:@"" message:@"Password must contain one special charcter"];
        //            return NO;
        //        }
        
    }
    else
    {
        [APPDELEGATE showAlert:@"" message:@"Password should be 8 to 16 characters"];
        return NO;
        
    }
}


- (BOOL)string:(NSString *)text matches:(NSString *)pattern
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
    
    NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)];
    
    return matches.count > 0;
}


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark-
#pragma mark-TextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([textField isEqual:txtEmail]) {
        [txtPassword becomeFirstResponder];
    }
    else if ([textField isEqual:txtPassword]) {
        [txtPasswordConfirm becomeFirstResponder];
    }
    
    else if ([textField isEqual:txtPasswordConfirm]) {
        [txtName becomeFirstResponder];
    }
    else if ([textField isEqual:txtName]) {
        [txtName resignFirstResponder];
    }
    
    return  NO;
}


#pragma marka Image Picker Delegate
/*!
 @brief Performed when Image choosed/clicked
 
 @discussion Load the respective things when Image choosed/clicked
 
 @param sender
 
 @return nothing
 */
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [photoPicker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    
    [buttonProfile setImage:chosenImage forState:UIControlStateNormal];
    selectImage=chosenImage;
    isImageupload=YES;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    [photoPicker dismissViewControllerAnimated:YES completion:nil];
    
}

//func scaleImage(image: UIImage, maxDimension: CGFloat) -> UIImage {
//
//    var scaledSize = CGSizeMake(maxDimension, maxDimension)
//    var scaleFactor:CGFloat
//
//    if image.size.width > image.size.height {
//        scaleFactor = image.size.height / image.size.width
//        scaledSize.width = maxDimension
//        scaledSize.height = scaledSize.width * scaleFactor
//    } else {
//        scaleFactor = image.size.width / image.size.height
//        scaledSize.height = maxDimension
//        scaledSize.width = scaledSize.height * scaleFactor
//    }
//
//    UIGraphicsBeginImageContext(scaledSize)
//    image.drawInRect(CGRectMake(0, 0, scaledSize.width, scaledSize.height))
//    let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
//    UIGraphicsEndImageContext()
//
//    return scaledImage
//}



#pragma mark -  Action Sheet Methods


/*!
 @brief Performed when Action sheet dismissed
 
 @discussion Load the respective things when Action sheet dismissed
 
 @param sender
 
 @return nothing
 */
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    
    
    
    photoPicker = [[UIImagePickerController alloc] init];
    
    photoPicker.delegate = self;
    
    photoPicker.allowsEditing = YES;
    
    
    
    if(buttonIndex == 0) {
        
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            
            
            
            photoPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            
            
            
            if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
                
                UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:photoPicker];
                
                [popover presentPopoverFromRect:buttonProfile.bounds inView:self.view permittedArrowDirections:
                 
                 UIPopoverArrowDirectionAny animated:YES];
                
                popOver = popover;
                
            } else {
                
                [self presentViewController:photoPicker animated:YES completion:NULL];
                
            }
            
        }
        
        else{
            
        }
    }
    
    else if (buttonIndex == 1){
        
        photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
            
            UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:photoPicker];
            
            [popover presentPopoverFromRect:buttonProfile.bounds inView:self.view permittedArrowDirections:
             
             UIPopoverArrowDirectionAny animated:YES];
            
            popOver = popover;
            
        } else {
            
            [self presentViewController:photoPicker animated:YES completion:NULL];
            
        }
        
    }
    
}


#pragma marka Response Delegate

- (void)didReceivedresponse:(NSDictionary *)responsedict
{
    NSLog(@"%@", responsedict);
    
    [APPDELEGATE removeProgress];
    if ([[responsedict valueForKey:@"status"]  intValue] == 1)
    {
        [APPDELEGATE showAlert:@"" message:[responsedict valueForKey:@"data"]];
    }
    
    else  if ([[responsedict valueForKey:@"status"]  intValue] == 0)
    {
        [APPDELEGATE showAlert:@"" message:[responsedict valueForKey:@"message"]];
    }
}

- (void)didFailedWithError:(NSError *)error
{
    [APPDELEGATE removeProgress];
    [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    
}

- (IBAction)profilePicClick:(id)sender {
    [txtName resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtPassword resignFirstResponder];
    [txtPasswordConfirm resignFirstResponder];
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Choose a source:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Camera",
                            @"Gallery",
                            nil];
    popup.backgroundColor = [UIColor whiteColor];
    [popup showInView:self.view];
    
}





/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
