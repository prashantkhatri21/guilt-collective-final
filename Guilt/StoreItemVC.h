//
//  StoreItemVC.h
//  
//
//  Created by Gourav Sharma on 12/16/15.
//
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#import "ModelGuilt.h"
#import "ModelProduct.h"
@interface StoreItemVC : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *imageview;
@property (strong, nonatomic) IBOutlet CustomLabel *lblName;
@property (strong, nonatomic) IBOutlet CustomLabel *lblProductInfo;

@property (strong, nonatomic) IBOutlet CustomLabel *lblPrice;
@property (strong, nonatomic)  ModelGuilt *objGuilt;
@property (strong, nonatomic)  ModelProduct *objProduct;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (strong, nonatomic)  NSString *navigation_type;
@property (weak, nonatomic) IBOutlet CustomLabel *lblUsername;
@property (weak, nonatomic) IBOutlet CustomButton *btnFollow;

- (IBAction)btnFollowClick:(id)sender;
@end
