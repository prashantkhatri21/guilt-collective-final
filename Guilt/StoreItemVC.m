//
//  StoreItemVC.m
//  
//
//  Created by Gourav Sharma on 12/16/15.
//
//

#import "StoreItemVC.h"
#import "UIImageView+WebCache.h"
#import "ModelProfile.h"
@interface StoreItemVC ()
{
    MBProgressHUD *HUD,*HUDToast;

}
@end

@implementation StoreItemVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeMBProgres:self.view];
    self.profilePic.layer.masksToBounds=YES;
    self.profilePic.layer.cornerRadius=self.profilePic.frame.size.width/2.0;
//    if (![self.navigation_type isEqualToString:@"search"]) {
    
        
        
        [self ShowProgress];
        NSDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,GETGUILT_DATA_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
     [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        [dict setValue:self.objGuilt.guilt_id forKey:@"guilt_id"];
        
        
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSLog(@"%@", responseObject);
            
            [self removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                NSDictionary *dataDict=responseObject;
                
               
                
                self.objGuilt.objProfile=[[ModelProfile alloc]init];
                
//                    self.objGuilt.objProfile.fullname= [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"first_name"]];
                if ([dataDict valueForKey:@"first_name"]!=nil && ![[dataDict valueForKey:@"first_name"] isKindOfClass:[NSNull class]]) {
                    self.objGuilt.objProfile.fullname= [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"first_name"]];
                    
                }
                
                    if ([dataDict valueForKey:@"profile_pic"] !=nil) {
                        NSString *str=[dataDict valueForKey:@"profile_pic"];
                        if ([str length]!=0) {
                            self.objGuilt.objProfile.profile_pic_url= [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[dataDict valueForKey:@"profile_pic"]]];
                        }
                        
                        
                    }
                
                NSDictionary *dictGuilt=[dataDict valueForKey:@"data"];
                
                if ([dictGuilt valueForKey:@"image"] !=nil) {
                    NSString *str=[dictGuilt valueForKey:@"image"];
                    if ([str length]!=0) {
                        self.objGuilt.image_url= [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[dictGuilt valueForKey:@"image"]]];
                    }
                    
                    
                }
                self.objGuilt.user_id=[NSString stringWithFormat:@"%@",[dictGuilt valueForKey:@"user_id"]];

                self.objGuilt.descriptionGuilt=[NSString stringWithFormat:@"%@",[dictGuilt valueForKey:@"description"]];
                
                self.objGuilt.title=[NSString stringWithFormat:@"%@",[dictGuilt valueForKey:@"title"]];

                self.objGuilt.price=[NSString stringWithFormat:@"%@",[dictGuilt valueForKey:@"price"]];
                if (self.objGuilt.price!=nil && [self.objGuilt.price length]!=0) {
                 NSString *str   =[self.objGuilt.price stringByReplacingOccurrencesOfString:@"$" withString:@""];
                    self.objGuilt.price=[NSString stringWithFormat:@"$ %@",str];
                }
                if ([[dictGuilt valueForKey:@"follow_chk"]  intValue] == 1)
                {
                    self.objGuilt.objProfile.follow_chk=YES;
                    [self.btnFollow setTitle:@"Following" forState:UIControlStateNormal];

                }
                else{
                    self.objGuilt.objProfile.follow_chk=NO;
                    [self.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];


                }

                if ([self.objGuilt.user_id isEqualToString:[DEFAULTS valueForKey:USERID]]) {
                    self.btnFollow.hidden=YES;
                }
                else{
                    self.btnFollow.hidden=NO;
                }
                
                [self.profilePic sd_setImageWithURL:self.objGuilt.objProfile.profile_pic_url placeholderImage:[UIImage imageNamed:@"user"]];
                self.lblUsername.text=self.objGuilt.objProfile.fullname;
                
                [self.imageview sd_setImageWithURL:self.objGuilt.image_url placeholderImage:nil];
                self.lblProductInfo.text=self.objGuilt.descriptionGuilt;
                self.lblName.text=self.objGuilt.title;
                if ([self.objGuilt.price length]==0) {
                    self.lblPrice.text=@"";
                }
                else{
                    self.lblPrice.text=[NSString stringWithFormat:@"%@",self.objGuilt.price];
                    
                }
                    
                    
                }
            
            else if ([[responseObject valueForKey:@"status"]  intValue] == 0)
            {
                if ([responseObject valueForKey:@"message"]!=nil) {
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];

                }
                else{
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];

                }
            }
            }
         
            
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        }];

//        [self.profilePic sd_setImageWithURL:self.objGuilt.objProfile.profile_pic_url placeholderImage:[UIImage imageNamed:@"user"]];
//        self.lblUsername.text=self.objGuilt.objProfile.fullname;
    

    //}
//    else  if ([self.navigation_type isEqualToString:@"search"]){
////        [self.imageview sd_setImageWithURL:self.objGuilt.image_url placeholderImage:nil];
////        self.lblProductInfo.text=self.objGuilt.descriptionGuilt;
////        self.lblName.text=self.objGuilt.title;
////        self.lblPrice.text=[NSString stringWithFormat:@"%@$",self.objGuilt.price];
////        
////        [self.profilePic sd_setImageWithURL:self.objGuilt.objProfile.profile_pic_url placeholderImage:[UIImage imageNamed:@"user"]];
////        self.lblUsername.text=self.objGuilt.objProfile.fullname;
//        
//        
//    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addtomyguiltClick:(id)sender {
}
- (IBAction)viewinStoreClick:(id)sender {
}
- (IBAction)backClick:(id)sender {
    POP
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnFollowClick:(id)sender {
    [self ShowProgress];
    NSMutableDictionary *dict;
    NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,ADD_FOLLOWING_API];
    dict=[[NSMutableDictionary alloc]init];
    [dict setValue:TOKEN forKey:@"token"];
    [dict setValue:self.objGuilt.user_id forKey:@"follow_id"];
    [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"follower_id"];
//     [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager setRequestSerializer:requestSerializer];
    [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [self removeProgress];
        if ([[responseObject valueForKey:@"status"]  intValue] == 1)
        {
            
            if ([responseObject valueForKey:@"message"]!=nil) {
                if ([[responseObject valueForKey:@"message"] isEqualToString:@"unfollow success"]) {
                    
                    [self.btnFollow setTitle:@"Follow" forState:UIControlStateNormal];
                    
                    
                }
                else if ([[responseObject valueForKey:@"message"] isEqualToString:@"follow success"]){
                   
                    [self.btnFollow setTitle:@"Following" forState:UIControlStateNormal];
                }
                
            }
            
        }
        else
        {
            if ([responseObject valueForKey:@"data"]!=nil) {
                if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                    
                }
                else{
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                    
                }
            }
            
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self removeProgress];
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        
        
    }];
    

}
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}

#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}

@end
