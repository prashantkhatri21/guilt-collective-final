//
//  TabBarVC.m
//  Guilt
//
//  Created by Gourav Sharma on 10/30/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "TabBarVC.h"
#import "Utility.h"
#import "LoginVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface TabBarVC ()


@end

@implementation TabBarVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [NS_NOTIFICATION addObserver:self selector:@selector(logout) name:NOTIFICATION_LOGOUT object:nil];

    NSLog(@"simple feedback");
   // Assign tab bar item with titles //Tab
//    UITabBar *tabBar = self.tabBar;
//    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
//    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
//    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
//    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
//    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
//    
//    tabBarItem1.title = @"Feed";
//    tabBarItem2.title = @"Search";
//    tabBarItem3.title = @"Snap It";
//    tabBarItem4.title = @"Shop";
//    tabBarItem5.title = @"Profile";
//    
//    [tabBarItem1 setFinishedSelectedImage:[UIImage imageNamed:@"feed_blk"] withFinishedUnselectedImage:[UIImage imageNamed:@"feed_wht"]];
//    
//    [tabBarItem2 setFinishedSelectedImage:[UIImage imageNamed:@"search_blk"] withFinishedUnselectedImage:[UIImage imageNamed:@"search_wht"]];
//    [tabBarItem3 setFinishedSelectedImage:[UIImage imageNamed:@"search_blk"] withFinishedUnselectedImage:[UIImage imageNamed:@"search_wht"]];
//    [tabBarItem4 setFinishedSelectedImage:[UIImage imageNamed:@"store_blk"] withFinishedUnselectedImage:[UIImage imageNamed:@"store_wht"]];
//    [tabBarItem5 setFinishedSelectedImage:[UIImage imageNamed:@"store_blk"] withFinishedUnselectedImage:[UIImage imageNamed:@"store_wht"]];
//    
//    // Change the title color of tab bar items
//    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                       [UIColor blackColor], NSForegroundColorAttributeName,
//                                                       nil] forState:UIControlStateNormal];
//    UIColor *titleHighlightedColor = [UIColor whiteColor];
//    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                       titleHighlightedColor, NSForegroundColorAttributeName,
//                                                       nil] forState:UIControlStateHighlighted];
//    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"black"]];
//
//    
//
//    
//    // Change the tab bar background
//    [[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
    //    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar_selected.png"]];
    
   

    // Do any additional setup after loading the view.
}
-(void)logout
{
    NSArray *arr=self.navigationController.viewControllers;
    for (int i=0;i<[arr count];i++) {
        UIViewController *view=[arr objectAtIndex:i];
        if ([view isKindOfClass:[LoginVC class]]) {
            [DEFAULTS setValue:nil forKey:USERID];
            [DEFAULTS synchronize];
            [[GIDSignIn sharedInstance] signOut];
            FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
            [loginManager logOut];
            //Second is by setting the currentAccessToken to nil
            AppDelegate* appDelegate =[AppDelegate sharedAppdelegate];
            [appDelegate.instagram logout];
            [FBSDKAccessToken setCurrentAccessToken:nil];
            [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"accessToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.navigationController popToViewController:view animated:NO];
            //            [NS_NOTIFICATION postNotificationName:NOTIFICATION_LOGOUT object:nil];
        }
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
