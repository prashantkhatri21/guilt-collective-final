//
//  TableCellComments.h
//  Guilt
//
//  Created by Gourav Sharma on 10/28/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#import "TLTagsControl.h"
@interface TableCellComments : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *ivProfile;
@property (strong, nonatomic) IBOutlet CustomLabel *lblName;
@property (strong, nonatomic) IBOutlet CustomLabel *lblDate;
@property (strong, nonatomic) IBOutlet CustomTextView *textviewComments;


//--For Feeds
@property (strong, nonatomic) IBOutlet UIImageView *ivFeeds;
@property (strong, nonatomic) IBOutlet UIView *viewcontent;
@property (strong, nonatomic) IBOutlet CustomLabel *lblTagOne;

@property (strong, nonatomic) IBOutlet CustomLabel *lblTagtwo;
@property (strong, nonatomic) IBOutlet TLTagsControl *tagsControl;
@property (strong, nonatomic) IBOutlet CustomLabel *lblTag3;
@property (strong, nonatomic) IBOutlet CustomLabel *lblTag4;
@property (strong, nonatomic) IBOutlet CustomLabel *lblDress;
@property (strong, nonatomic) IBOutlet CustomButton *btnColor1;
@property (strong, nonatomic) IBOutlet CustomButton *btnColor2;
@property (strong, nonatomic) IBOutlet CustomButton *btnColor3;
@property (strong, nonatomic) IBOutlet CustomButton *btnColor4;
@property (strong, nonatomic) IBOutlet CustomButton *btnComments;
@property (strong, nonatomic) IBOutlet CustomButton *btnShare;
@property (strong, nonatomic) IBOutlet CustomButton *btnFilter;
@property (strong, nonatomic) IBOutlet CustomLabel *lblCommentsCount;
@property (strong, nonatomic) IBOutlet CustomLabel *lblhareCounts;
@property (strong, nonatomic) IBOutlet CustomLabel *lblDlownloadCount;
@property (strong, nonatomic) IBOutlet CustomButton *btDownloads;
@property (strong, nonatomic) IBOutlet UIButton *btnLike;
@property (strong, nonatomic) IBOutlet UIButton *btnGender;

@end
