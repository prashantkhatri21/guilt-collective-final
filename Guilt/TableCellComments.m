//
//  TableCellComments.m
//  Guilt
//
//  Created by Gourav Sharma on 10/28/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "TableCellComments.h"

@implementation TableCellComments

- (void)awakeFromNib {
    // Initialization code
    [self.tagsControl setMode:TLTagsControlModeList];
    self.ivProfile.layer.masksToBounds=YES;
    self.ivProfile.layer.cornerRadius=self.ivProfile.frame.size.width/2.0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
