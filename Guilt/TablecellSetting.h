//
//  TablecellSetting.h
//  Guilt
//
//  Created by Gourav Sharma on 11/2/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface TablecellSetting : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *ivIcon;
@property (strong, nonatomic) IBOutlet CustomLabel *lblSetting;
@property (strong, nonatomic) IBOutlet UIImageView *ivCheck;

@end
