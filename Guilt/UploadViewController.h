//
//  UploadViewController.h
//  Guilt
//
//  Created by Gourav Sharma on 11/3/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#import "TLTagsControl.h"
#import "ModelProduct.h"
@interface UploadViewController : UIViewController
{
    __weak IBOutlet CustomButton *btnfindthisitem;
    __weak IBOutlet CustomLabel *lblPickOneofoption;
    __weak IBOutlet CustomTextView *txtDescription;
    __weak IBOutlet UISearchBar *searchbar;
    
    __weak IBOutlet NSLayoutConstraint *consVertical;
    __weak IBOutlet UITableView *tableview;
    __weak IBOutlet CustomLabel *lblColors;
}
@property (nonatomic, strong) IBOutlet TLTagsControl *defaultEditingTagControl;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewSelectedProduct;
@property(nonatomic,strong)UIImage *imageSelected;
@property(nonatomic,strong)NSMutableArray *arraySelectcolor;
@property(nonatomic,strong)ModelProduct *objCatagory;
@property(nonatomic,strong)NSString *gender;
-(void)selectedColor:(NSMutableArray*)arrayColors;

@end
