//
//  UploadViewController.m
//  Guilt
//
//  Created by Gourav Sharma on 11/3/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "UploadViewController.h"
#import "Utility.h"
#import "ColorPickerVC.h"
#import "FilterVCUpload.h"
#import "ModelColor.h"
#import "Base64/Base64Transcoder.h"
#import "ViewController.h"
@interface UploadViewController ()<TLTagsControlDelegate>
{
    NSMutableArray *objSearchTutorArray,*objArray;
    MBProgressHUD *HUD,*HUDToast;
    NSMutableArray *arrcolorsname;
    NSString *strGuiltID;
}
@end

@implementation UploadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    txtDescription.delegate=self;
    txtDescription.text=@"";
    txtDescription.placeholderText=@"Tell us about it (max 50 characters)";
    [txtDescription setTextAlignment:NSTextAlignmentLeft];

    [self initializeMBProgres:self.view];
    objSearchTutorArray=[[NSMutableArray alloc]init];
    objArray=[[NSMutableArray alloc]init];
    
    [objArray addObject:@"red"];
    [objArray addObject:@"jeans"];
    [objArray addObject:@"green"];
    [objArray addObject:@"shirt"];
    self.imageViewSelectedProduct.image=self.imageSelected;
    _defaultEditingTagControl.tagPlaceholder = @"Add Tag";
    self.defaultEditingTagControl.tapDelegate=self;
    [self.defaultEditingTagControl setMode:TLTagsControlModeEdit];
    self.defaultEditingTagControl.tag=1001;
    
//    [_defaultEditingTagControl reloadTagSubviews];
    
    self.navigationController.navigationBarHidden=YES;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)findThisitemClick:(id)sender {
    //    FilterVCUpload *obj =[self.storyboard instantiateViewControllerWithIdentifier:@"FilterVCUpload"];
    //    obj.arrayColor=self.arraySelectcolor;
    //    [self.navigationController pushViewController:obj animated:YES];
    if([self validate])
    {
        [self ShowProgress];
        Base64Transcoder *objBase64=[[Base64Transcoder alloc]init];
        NSData *data=UIImageJPEGRepresentation(self.imageViewSelectedProduct.image, 0.5);
        NSString *strImage= [objBase64 base64EncodedStringfromData:data];
        
        
        NSMutableDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,ADD_GUILT_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        [dict setValue:strImage forKey:@"image"];
        [dict setValue:txtDescription.text forKey:@"description"];
        [dict setValue:@"male" forKey:@"gender"];
        
        NSMutableArray *array=[[NSMutableArray alloc]init];
        for (NSString *str in arrcolorsname) {
            NSDictionary *dict = @{@"color":str};
            [array addObject:dict];
        }
        NSData *dataColors = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:nil];
        NSString *strColors=[[NSString alloc]initWithData:dataColors encoding:NSUTF8StringEncoding];
        NSLog(@"Colors=%@",strColors);
        [dict setValue:strColors forKey:@"colors"];
        
        
        NSMutableArray *arrayTags=[[NSMutableArray alloc]init];
        for (NSString *str in self.defaultEditingTagControl.tags) {
            NSString *string=[str stringByReplacingOccurrencesOfString:@"#" withString:@""];
            NSDictionary *dict = @{@"tag":string};
            [arrayTags addObject:dict];
        }
        NSData *dataTags = [NSJSONSerialization dataWithJSONObject:arrayTags options:NSJSONWritingPrettyPrinted error:nil];
        NSString *strTags=[[NSString alloc]initWithData:dataTags encoding:NSUTF8StringEncoding];
        NSLog(@"Colors=%@",strTags);
        [dict setValue:strTags forKey:@"tags"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                if([responseObject valueForKey:@"message"]!=nil)
                {
                    [self showToast:[responseObject valueForKey:@"message"]];
                }
                if ([responseObject valueForKey:@"data"]!=nil) {
                    NSDictionary *dictData=[responseObject valueForKey:@"data"];
                    strGuiltID=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"guilt_id"]];
                    
                    ViewController *objViewcontroller=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                    objViewcontroller.arrayColor=self.arraySelectcolor;
                    objViewcontroller.objProduct=self.objCatagory;
                    objViewcontroller.navigationType=@"FIND";
                    objViewcontroller.strGuiltID=strGuiltID;
                    [self.navigationController pushViewController:objViewcontroller animated:YES];
                }
                
            }
            else
            {
                if ([responseObject valueForKey:@"data"]!=nil) {
                    if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                        
                    }
                    else{
                        [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                        
                    }
                }
                
            }
            [tableview reloadData];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];
    }
    
    
    
}

-(BOOL)validate
{
    if (txtDescription.text.length==0) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter description" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    if (txtDescription.text.length>50) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Description should not greater than 50 characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    if ([self.arraySelectcolor count]==0) {
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please select color" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    else if ([[self.defaultEditingTagControl tags] count]==0){
        [[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter tag" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return false;
    }
    return true;
}

- (IBAction)buttonaddTagClick:(id)sender {
    
}
- (IBAction)backClick:(id)sender {
    POP;
}
- (IBAction)pickColorClick:(id)sender {
    [self performSegueWithIdentifier:@"color" sender:self];
}
- (IBAction)findBestDealClick:(id)sender {
    if([self validate])
    {
        [self ShowProgress];
        Base64Transcoder *objBase64=[[Base64Transcoder alloc]init];
        NSData *data=UIImageJPEGRepresentation(self.imageViewSelectedProduct.image, 0.5);
        NSString *strImage= [objBase64 base64EncodedStringfromData:data];
        
        
        NSMutableDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,ADD_GUILT_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        [dict setValue:strImage forKey:@"image"];
        [dict setValue:txtDescription.text forKey:@"description"];
        [dict setValue:@"male" forKey:@"gender"];
        
        NSMutableArray *array=[[NSMutableArray alloc]init];
        for (NSString *str in arrcolorsname) {
            NSDictionary *dict = @{@"color":str};
            [array addObject:dict];
        }
        NSData *dataColors = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:nil];
        NSString *strColors=[[NSString alloc]initWithData:dataColors encoding:NSUTF8StringEncoding];
        NSLog(@"Colors=%@",strColors);
        [dict setValue:strColors forKey:@"colors"];
        
        
        NSMutableArray *arrayTags=[[NSMutableArray alloc]init];
        for (NSString *str in self.defaultEditingTagControl.tags) {
            NSString *string=[str stringByReplacingOccurrencesOfString:@"#" withString:@""];
            NSDictionary *dict = @{@"tag":string};
            [arrayTags addObject:dict];
        }
        NSData *dataTags = [NSJSONSerialization dataWithJSONObject:arrayTags options:NSJSONWritingPrettyPrinted error:nil];
        NSString *strTags=[[NSString alloc]initWithData:dataTags encoding:NSUTF8StringEncoding];
        NSLog(@"Colors=%@",strTags);
        [dict setValue:strTags forKey:@"tags"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                if([responseObject valueForKey:@"message"]!=nil)
                {
                    [self showToast:[responseObject valueForKey:@"message"]];
                }
                if ([responseObject valueForKey:@"data"]!=nil) {
                    NSDictionary *dictData=[responseObject valueForKey:@"data"];
                    strGuiltID=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"guilt_id"]];
                    
                    ViewController *objViewcontroller=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                    objViewcontroller.arrayColor=self.arraySelectcolor;
                    objViewcontroller.objProduct=self.objCatagory;
                    objViewcontroller.navigationType=@"FIND";
                    objViewcontroller.strGuiltID=strGuiltID;
                    [self.navigationController pushViewController:objViewcontroller animated:YES];
                }
                
            }
            else
            {
                if ([responseObject valueForKey:@"data"]!=nil) {
                    if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                        
                    }
                    else{
                        [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                        
                    }
                }
                
            }
            [tableview reloadData];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];
    }
}
- (IBAction)shareclick:(id)sender {
    if([self validate])
    {
        [self ShowProgress];
        Base64Transcoder *objBase64=[[Base64Transcoder alloc]init];
        NSData *data=UIImageJPEGRepresentation(self.imageViewSelectedProduct.image, 0.5);
        NSString *strImage= [objBase64 base64EncodedStringfromData:data];
        
        
        NSMutableDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,ADD_GUILT_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        [dict setValue:strImage forKey:@"image"];
        [dict setValue:txtDescription.text forKey:@"description"];
        [dict setValue:@"male" forKey:@"gender"];
        
        NSMutableArray *array=[[NSMutableArray alloc]init];
        for (NSString *str in arrcolorsname) {
            NSDictionary *dict = @{@"color":str};
            [array addObject:dict];
        }
        NSData *dataColors = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:nil];
        NSString *strColors=[[NSString alloc]initWithData:dataColors encoding:NSUTF8StringEncoding];
        NSLog(@"Colors=%@",strColors);
        [dict setValue:strColors forKey:@"colors"];
        
        
        NSMutableArray *arrayTags=[[NSMutableArray alloc]init];
        for (NSString *str in self.defaultEditingTagControl.tags) {
            NSString *string=[str stringByReplacingOccurrencesOfString:@"#" withString:@""];
            NSDictionary *dict = @{@"tag":string};
            [arrayTags addObject:dict];
        }
        NSData *dataTags = [NSJSONSerialization dataWithJSONObject:arrayTags options:NSJSONWritingPrettyPrinted error:nil];
        NSString *strTags=[[NSString alloc]initWithData:dataTags encoding:NSUTF8StringEncoding];
        NSLog(@"Colors=%@",strTags);
        [dict setValue:strTags forKey:@"tags"];
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                if([responseObject valueForKey:@"message"]!=nil)
                {
                    [self showToast:[responseObject valueForKey:@"message"]];
                }
                if ([responseObject valueForKey:@"data"]!=nil) {
//                    NSDictionary *dictData=[responseObject valueForKey:@"data"];
//                    strGuiltID=[NSString stringWithFormat:@"%@",[dictData valueForKey:@"guilt_id"]];
//                    
//                    ViewController *objViewcontroller=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//                    objViewcontroller.arrayColor=self.arraySelectcolor;
//                    objViewcontroller.isshare=YES;
//                    objViewcontroller.objProduct=self.objCatagory;
//                    objViewcontroller.navigationType=@"FIND";
//                    objViewcontroller.strGuiltID=strGuiltID;
//                    [self.navigationController pushViewController:objViewcontroller animated:YES];
                }
                
            }
            else
            {
                if ([responseObject valueForKey:@"data"]!=nil) {
                    if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                        
                    }
                    else{
                        [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                        
                    }
                }
                
            }
            [tableview reloadData];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];
    }
}


-(void)selectedColor:(NSMutableArray*)arrayColors
{
    self.arraySelectcolor=arrayColors;
    arrcolorsname=[[NSMutableArray alloc]init];
    for (ModelColor *objcolor in self.arraySelectcolor) {
        [arrcolorsname addObject:objcolor.name];
    }
    lblColors.text=[arrcolorsname componentsJoinedByString:@","];
}

#pragma mark -------- SearchBar Delegate -----------


/*!
 @brief search bar handler is called when text did begins editing
 
 @discussion search bar handler is called when text did begins editing
 
 @param UISearchBar
 
 @return nothing
 */
- (void) searchBarTextDidBeginEditing:(UISearchBar *)theSearchBar
{
    searchbar.showsCancelButton = NO;
}



/*!
 @brief search bar handler is called when text did change in the search bar
 
 @discussion search bar handler is called when text did change in the search bar
 
 @param UISearchBar
 
 @return nothing
 */
- (void)searchBar:(UISearchBar *)theSearchBar textDidChange:(NSString *)searchText
{
    // Call When text edit
    
    if ([objSearchTutorArray count] >0)
    {
        [objSearchTutorArray removeAllObjects];
    }
    if([searchText length] == 0) {
        
        [theSearchBar performSelector: @selector(resignFirstResponder)
                           withObject: nil
                           afterDelay: 0.1];
        
        
    }
    
    if([searchText length] > 0)
    {
        [self searchTableView];
        
    }
    
    
    // Call createDataSource method
    
}








/*!
 @brief search bar handler is called when the cancel button of search bar is clicked
 
 @discussion search bar handler is called when the cancel button of search bar is clicked
 
 @param UISearchBar
 
 @return nothing
 */
- (void)searchBarCancelButtonClicked:(UISearchBar *) theSearchBar
{
    
    searchbar.showsCancelButton = NO;
    
    [searchbar resignFirstResponder];
    
    searchbar.text = @"";
    
    searchbar.hidden=YES;
    tableview.hidden=YES;
    consVertical.constant=193;
    
    
    //[self createDataSource];
    
    
    
    // Reload Data
    
    
    //    [self createDataSource];
    
    // [mTableView reloadData];
}


/*!
 @brief search bar handler is called when it's search button is clicked
 
 @discussion search bar handler is called to load the results
 
 @param UISearchBar
 
 @return nothing
 */
- (void) searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    searchbar.showsCancelButton = NO;
    
    [searchbar resignFirstResponder];
    
    searchbar.text = @"";
    
    searchbar.hidden=YES;
    tableview.hidden=YES;
    consVertical.constant=193;
    //This will dismiss the keyboard
    //    [self.tutorCollectionView reloadData];
    //[mTableView reloadData];
}


/*!
 @brief Search the contacts with keyword
 
 @discussion Search the contacts with keyword
 
 @param nothing
 
 @return nothing
 */
- (void) searchTableView
{
    //arr=objSearchLocalAray;
    NSString *string=searchbar.text;
    for (NSString* item in objArray)
    {
        if ([item rangeOfString:string].location != NSNotFound)
            [objSearchTutorArray addObject:item];
        
    }
    [tableview reloadData];
}

#pragma mark TAGVIEW delegate
-(void)searchTag:(NSString*)search
{
    //arr=objSearchLocalAray;
    NSString *string=search;
    for (NSString* item in objArray)
    {
        if ([item rangeOfString:string].location != NSNotFound)
            [objSearchTutorArray addObject:item];
        
    }
    [tableview reloadData];
}

-(void)beginediting
{
    tableview.hidden=YES;
//    consVertical.constant=20;
}
-(void)endediting
{
    tableview.hidden=YES;
//    consVertical.constant=193;
}
#pragma mark Tableview Delegate

//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return  1;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return objSearchTutorArray.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.text=[objSearchTutorArray objectAtIndex:indexPath.row];
    return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    [self.defaultEditingTagControl addTag:[NSString stringWithFormat:@"#%@",[objSearchTutorArray objectAtIndex:indexPath.row]]];
    [self.defaultEditingTagControl reloadTagSubviews];
    searchbar.hidden=YES;
    tableview.hidden=YES;
    consVertical.constant=193;
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"color"]) {
        ColorPickerVC *obj=[segue destinationViewController];
        obj.delegate=self;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark MBProgress
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}

#pragma mark- text View delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    return YES;
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}




@end
