//
//  UserProfileVC.h
//  Guilt
//
//  Created by Gourav Sharma on 11/2/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
@interface UserProfileVC : UIViewController
{
    IBOutlet UIScrollView *scrollviewCollection;
    __weak IBOutlet CustomLabel *lblGuiltyFindcount;
    __weak IBOutlet UIImageView *imageProfile;
    __weak IBOutlet CustomLabel *lblNogulitFound;
    
    __weak IBOutlet CustomLabel *lblUsername;
    IBOutlet CustomLabel *lblGuiultyFinders;
    IBOutlet CustomLabel *lblMycollective;
    IBOutlet CustomLabel *LBLFOLLOWERS;
    IBOutlet CustomLabel *LBLFOLLOWING;
    IBOutlet UIImageView *ivBorder;
    IBOutlet UIScrollView *scrollview;
    IBOutlet CustomLabel *lblMycollectionCount;
    IBOutlet CustomButton *btnimgProfile;
    IBOutlet CustomLabel *lblDiscription;
    IBOutlet CustomLabel *lblFollowerCount;
    IBOutlet CustomLabel *lblFollowingCount;
    IBOutlet UICollectionView *collectionviewMycollective;
    IBOutlet UICollectionView *CollectiionViewGuilty;
    IBOutlet UIView *viewSlideMycollective;
    IBOutlet UIView *viewGuiltyFinds;
}

@end
