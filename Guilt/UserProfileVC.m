//
//  UserProfileVC.m
//  Guilt
//
//  Created by Gourav Sharma on 11/2/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "UserProfileVC.h"
#import "CollectioncellCatagory.h"
#import "ModelGuilt.h"
#import "UIImageView+WebCache.h"
#import "StoreItemVC.h"
#import "LoginVC.h"
@interface UserProfileVC ()
{
    MBProgressHUD *HUD,*HUDToast;
    NSMutableArray *arrayfoundGuilt,*arrayNotfoundGuilt;
}
@end

@implementation UserProfileVC

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Set Title
        UIImage *musicImage = [UIImage imageNamed:@"feed_blk"];
        UIImage *musicImageSel = [UIImage imageNamed:@"feed_wht"];
        
        musicImage = [musicImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        musicImageSel = [musicImageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        self.navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"Profile" image:musicImage selectedImage:musicImageSel];
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor blackColor], NSForegroundColorAttributeName,[UIFont fontWithName:@"OSWALD" size:10], UITextAttributeFont,
                                                           nil] forState:UIControlStateNormal];
        
        
        
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];

    [NS_NOTIFICATION addObserver:self selector:@selector(getProfileData) name:NOTIFICATION_PROFILEUPDATE object:nil];

    [self initializeMBProgres:self.view];
    arrayfoundGuilt=[[NSMutableArray alloc]init];
    arrayNotfoundGuilt=[[NSMutableArray alloc]init];
    
    self.tabBarController.tabBar.hidden=YES;
    self.tabBarController.hidesBottomBarWhenPushed=YES;
    btnimgProfile.layer.masksToBounds= YES;
    btnimgProfile.layer.cornerRadius= btnimgProfile.frame.size.width/2;
    imageProfile.layer.masksToBounds= YES;
    imageProfile.layer.cornerRadius= imageProfile.frame.size.width/2;
    viewSlideMycollective.hidden=NO;
    viewGuiltyFinds.hidden=YES;
    
    // Do any additional setup after loading the view.
}



-(void)getProfileData
{
    [self ShowProgress];
    NSDictionary *dict;
    NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,USERPROFILE_API];
    dict=[[NSMutableDictionary alloc]init];
    [dict setValue:TOKEN forKey:@"token"];
    [dict setValue:[DEFAULTS valueForKey:USERID]forKey:@"user_id"];
    
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    
    [manager setRequestSerializer:requestSerializer];
    
    [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        
        [self removeProgress];
        if ([[responseObject valueForKey:@"status"]  intValue] == 1)
        {
            if ([responseObject valueForKey:@"data"]!=nil) {
                NSDictionary *dataDict=[responseObject valueForKey:@"data"];
                AppDelegate *objAPPdelegate=APPDELEGATE;
                objAPPdelegate.profile=[[ModelProfile alloc]init];
                objAPPdelegate.profile.fullname= [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"first_name"]];
                objAPPdelegate.profile.email= [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"email"]];
                if ([dataDict valueForKey:@"description"]!=nil) {
                    objAPPdelegate.profile.description_profile= [NSString stringWithFormat:@"%@",[dataDict valueForKey:@"description"]];

                }
                else{
                    objAPPdelegate.profile.description_profile=@"";
                }
                if ([dataDict valueForKey:@"profile_pic"] !=nil) {
                    NSString *str=[dataDict valueForKey:@"profile_pic"];
                    if ([str length]!=0) {
                        objAPPdelegate.profile.profile_pic_url= [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[dataDict valueForKey:@"profile_pic"]]];
                    }
                    

                }
                
                objAPPdelegate.profile.age= [[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"age"]] intValue];
                if ([dataDict valueForKey:@"gender"]!=nil) {
                    NSString *str=[[dataDict valueForKey:@"gender"] lowercaseString];
                    
                    
                    objAPPdelegate.profile.gender=str;
                }
                objAPPdelegate.profile.followingCount= [[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"following_count"]] intValue];
                objAPPdelegate.profile.followercCount= [[NSString stringWithFormat:@"%@",[dataDict valueForKey:@"follower_count"]] intValue];

                //-----Found Guilt--//
                
                
                if ([dataDict valueForKey:@"found_guilt"]!=nil) {
                    [arrayfoundGuilt removeAllObjects];
                    for (id DictfoundGuilt in [dataDict valueForKey:@"found_guilt"]) {
                        ModelGuilt *objGuilt=[[ModelGuilt alloc]init];
                        objGuilt.guilt_id=[DictfoundGuilt valueForKey:@"guilt_id"];
                        objGuilt.descriptionGuilt=[DictfoundGuilt valueForKey:@"description"];

                        objGuilt.user_id=[DictfoundGuilt valueForKey:@"user_id"];
                        if ([DictfoundGuilt valueForKey:@"image"]!=nil) {
                            objGuilt.image_url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[DictfoundGuilt valueForKey:@"image"]]];

                        }
                        if ([DictfoundGuilt valueForKey:@"gender"]!=nil) {
                            NSString *str=[[DictfoundGuilt valueForKey:@"gender"] lowercaseString];
                            if ([str isEqualToString:@"male"]) {
                                objGuilt.ismale=YES;
                            }
                            else
                            {
                                objGuilt.ismale=NO;
                            }
                            
                            
                        }
                        if ([DictfoundGuilt valueForKey:@"found_link"]!=nil) {
                            objGuilt.found_link=[DictfoundGuilt valueForKey:@"found_link"];
                            
                        }
                        
                        if ([DictfoundGuilt valueForKey:@"price"]!=nil) {
                            objGuilt.price=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"price"]];
                            
                        }
                        if ([[DictfoundGuilt valueForKey:@"found"]  intValue] == 1)
                        {
                            objGuilt.found=YES;
                        }
                        else{
                            objGuilt.found=NO;

                        }
                        [arrayfoundGuilt addObject:objGuilt];
                       
                    }
                }
                
                //-----Not found Guilt--//
                
                if ([dataDict valueForKey:@"not_found_guilt"]!=nil) {
                    [arrayNotfoundGuilt removeAllObjects];
                    for (id DictfoundGuilt in [dataDict valueForKey:@"not_found_guilt"]) {
                        ModelGuilt *objGuilt=[[ModelGuilt alloc]init];
                        objGuilt.guilt_id=[DictfoundGuilt valueForKey:@"guilt_id"];
                        objGuilt.descriptionGuilt=[DictfoundGuilt valueForKey:@"description"];
                        
                        objGuilt.user_id=[DictfoundGuilt valueForKey:@"user_id"];
                        if ([DictfoundGuilt valueForKey:@"image"]!=nil) {
                            objGuilt.image_url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kBaseImageUrl,[DictfoundGuilt valueForKey:@"image"]]];
                            
                        }
                        if ([DictfoundGuilt valueForKey:@"gender"]!=nil) {
                            NSString *str=[[DictfoundGuilt valueForKey:@"gender"] lowercaseString];
                            if ([str isEqualToString:@"male"]) {
                                objGuilt.ismale=YES;
                            }
                            else
                            {
                                objGuilt.ismale=NO;
                            }
                            
                            
                        }
                        if ([DictfoundGuilt valueForKey:@"found_link"]!=nil) {
                            objGuilt.found_link=[DictfoundGuilt valueForKey:@"found_link"];
                            
                        }
                        if ([DictfoundGuilt valueForKey:@"price"]!=nil) {
                            objGuilt.price=[NSString stringWithFormat:@"%@",[DictfoundGuilt valueForKey:@"price"]];
                            
                        }
                        if ([[DictfoundGuilt valueForKey:@"found"]  intValue] == 1)
                        {
                            objGuilt.found=YES;
                        }
                        else{
                            objGuilt.found=NO;
                            
                        }
                        [arrayNotfoundGuilt addObject:objGuilt];
                        
                    }
                }

                
               
                
            }
            [self updateUIProfile];
            collectionviewMycollective.hidden=NO;
            CollectiionViewGuilty.hidden=NO;
            [CollectiionViewGuilty reloadData];
            [collectionviewMycollective reloadData];
            
        }
        else if ([[responseObject valueForKey:@"status"]  intValue] == 0)
        {
            
            if ([responseObject valueForKey:@"message"]!=nil) {
                [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];

            }
            else{
                [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];

            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [self removeProgress];
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    }];
}
-(void)updateUIProfile
{
    AppDelegate *objapdelegate= [AppDelegate sharedAppdelegate];
    lblDiscription.text= objapdelegate.profile.description_profile;
    lblUsername.text =objapdelegate.profile.fullname;
    lblFollowerCount.text =[NSString stringWithFormat:@"%d",objapdelegate.profile.followercCount];
    lblFollowingCount.text =[NSString stringWithFormat:@"%d",objapdelegate.profile.followingCount];
    lblMycollectionCount.text =[NSString stringWithFormat:@"%lu",(unsigned long)[arrayNotfoundGuilt count]];
    lblGuiltyFindcount.text =[NSString stringWithFormat:@"%lu",(unsigned long)[arrayfoundGuilt count]];
    
    UIImageView *localimageview=[[UIImageView alloc]init];
    [imageProfile sd_setImageWithURL:objapdelegate.profile.profile_pic_url placeholderImage:[UIImage imageNamed:@"profile_pic.png"]];
//    [localimageview sd_setImageWithURL:objapdelegate.profile.profile_pic_url placeholderImage:[UIImage imageNamed:@"circle_cam_white.png"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        [btnimgProfile setImage:image forState:UIControlStateNormal];
//    }];
   
    

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self getProfileData];
//    [self initializeMBProgres:self.view];
    self.tabBarController.tabBar.hidden=NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView dataSource/delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    if (collectionView==CollectiionViewGuilty) {
        if ([arrayfoundGuilt count]==0) {
            lblNogulitFound.hidden=NO;
            CollectiionViewGuilty.hidden=YES;
        }
        else{
            lblNogulitFound.hidden=YES;
            CollectiionViewGuilty.hidden=NO;

        }
        return arrayfoundGuilt.count;
    }
    else{
        if ([arrayNotfoundGuilt count]==0) {
            lblNogulitFound.hidden=NO;
            collectionviewMycollective.hidden=YES;
        }
        else{
            lblNogulitFound.hidden=YES;
            collectionviewMycollective.hidden=NO;

        }
        return arrayNotfoundGuilt.count;

    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectioncellCatagory *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    ModelGuilt *objGuilt;
    if (collectionView==collectionviewMycollective) {
        objGuilt=[arrayNotfoundGuilt objectAtIndex:indexPath.row];
    }
    else{
        objGuilt=[arrayfoundGuilt objectAtIndex:indexPath.row];

    }
    [cell.ivProduct sd_setImageWithURL:objGuilt.image_url placeholderImage:nil];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    StoreItemVC *objVC=[self.storyboard instantiateViewControllerWithIdentifier:@"StoreItemVC"];
    if (collectionView==collectionviewMycollective) {
         objVC.objGuilt=[arrayNotfoundGuilt objectAtIndex:indexPath.row];
    }
    else{
         objVC.objGuilt=[arrayfoundGuilt objectAtIndex:indexPath.row];
        
    }
    [self.navigationController pushViewController:objVC animated:YES];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return CGSizeMake(self.view.frame.size.width/3-10, self.view.frame.size.width/3-10);
}
- (IBAction)myCollectiveClick:(id)sender {
    if ([arrayNotfoundGuilt count]==0) {
        lblNogulitFound.hidden=NO;
        collectionviewMycollective.hidden=YES;
    }
    else{
        lblNogulitFound.hidden=YES;
        collectionviewMycollective.hidden=NO;
        
    }
    viewSlideMycollective.hidden=NO;
    viewGuiltyFinds.hidden=YES;
    [scrollviewCollection setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction)guiltyFindsClick:(id)sender {
    if ([arrayfoundGuilt count]==0) {
        lblNogulitFound.hidden=NO;
        CollectiionViewGuilty.hidden=YES;
    }
    else{
        lblNogulitFound.hidden=YES;
        CollectiionViewGuilty.hidden=NO;
        
    }
    viewSlideMycollective.hidden=YES;
    viewGuiltyFinds.hidden=NO;
    [scrollviewCollection setContentOffset:CGPointMake(CollectiionViewGuilty.frame.origin.x, 0) animated:YES];
    
}

-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}

#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
