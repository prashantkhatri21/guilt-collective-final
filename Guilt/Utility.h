//
//  Utility.h
//  Guilt
//
//  Created by Gourav Sharma on 10/30/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomButton.h"
#import "CustomLabel.h"
#import "CustomTextField.h"
#import "CustomTextView.h"
#import "WebHelper.h"
#import "AppDelegate.h"
#import "AFNetworking.h"

@interface Utility : NSObject
#define POP  [self.navigationController popViewControllerAnimated:YES];
#define WHITE [UIColor whiteColor]
#define GREY [UIColor grayColor]

#define POP     [self.navigationController popViewControllerAnimated:YES];

#define  kClientId = @"877623624245-5quo1h9gisukq566e5amtfhkceb1t4mo.apps.googleusercontent.com";

#define kBaseUrl @"http://52.24.60.37/guilt/index.php/app/"


#define kBaseUrl_Live @"http://api.shopstyle.com/api/v2/"


#define kBaseImageUrl @"http://52.24.60.37/guilt/"



#define APITokenURL_Product @"pid=uid5409-32142847-98"

#define APITokenURL @"token=a152e84173914146e4bc4f391sd0f686ebc4f31"
#define TOKEN @"a152e84173914146e4bc4f391sd0f686ebc4f31"
#define DEFAULTS  [NSUserDefaults standardUserDefaults]

#define NS_NOTIFICATION [NSNotificationCenter defaultCenter]
#define APPDELEGATE [AppDelegate sharedAppdelegate]

#define IMAGESize_Iphone @"IPhone"
#define IMAGESize_Iphone_Small @"IPhoneSmall"


#define USERID @"USERID"

#define WEBHELPER [WebHelper sharedHelper]

#define INITIALIZE_LOADER [APPDELEGATE initializeMBProgres:self.view];


#define NETWORKALERT [APPDELEGATE showAlert:@"" message:@"No Internet Access"];
#define NETWORKCHECK [APPDELEGATE isNetwork];

#define toastTimeDuration 2.0

#define NOTIFICATION_LOGOUT @"NOTIFICATION_LOGOUT"
#define NOTIFICATION_PROFILEUPDATE @"NOTIFICATION_PROFILEUPDATE"
#define NOTIFICATION_UPDATEFEEDS @"NOTIFICATION_UPDATEFEEDS"


//API METHOD
#define REGISTER_API @"register"
#define LOGIN_API @"login"
#define FORGETPASSWORD_API @"forgot_password"
#define CHANGEPASSWORD_API @"change_password"
#define COLORS_API @"colors?"
#define CATAGORIES_API @"categories?"
#define Brands_API @"brands?"
#define PRODUCTS_API @"products?"
#define USERPROFILE_API @"get_user_profile"
#define Update_USERPROFILE_API @"update_user_data"
#define COMMENTS_API @"get_comments"
#define ADD_COMMENTS_API @"add_comment"
#define FOLLOWING_API @"following_list"
#define FOLLOWER_API @"follower_list"
#define ADD_FOLLOWING_API @"add_following"
#define ADD_GUILT_API @"add_guilt"
#define FEED_API @"guilt_feed"
#define LIKE_GUILT_API @"like_guilt"
#define SUBMIT_QUERY_API @"submit_query"
#define NOTIFICATIONS_API @"get_notifications"
#define SHARE_GUILT_API @"share_guilt"
#define SEARCH_GUILT_API @"search_guilt"
#define Found_GUILT_API @"found_guilt"
#define DELETE_ACCOUNT_API @"delete_account"
#define GETGUILT_DATA_API @"get_guilt_data"
#define NEWSLETTER_API @"newsletter"

@end
