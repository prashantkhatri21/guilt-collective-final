//
//  webVC.h
//  Guilt
//
//  Created by Jatin on 11/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface webVC : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property(strong,nonatomic)NSURL *urlString;
@end
