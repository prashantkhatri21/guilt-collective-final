//
//  webVC.m
//  Guilt
//
//  Created by Jatin on 11/12/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "webVC.h"
#import "Utility.h"
@interface webVC ()

@end

@implementation webVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
    NSLog(@"URL string%@",self.urlString);
    
    NSMutableURLRequest * request =[NSMutableURLRequest requestWithURL:self.urlString];
    [self.webview loadRequest:request];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backClick:(id)sender {
    POP;
}

@end
