//
//  ImageCell.h
//  Gilt
//
//  Created by Prashant  on 28/10/15.
//  Copyright © 2015 Prashant . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utility.h"
#import "TLTagsControl.h"
@interface ImageCell : UICollectionViewCell
{
    
//    IBOutlet UIImageView *ivProfile;
//    IBOutlet CustomLabel *lblTag2;
//      IBOutlet CustomLabel *lblTag1;
//      IBOutlet CustomLabel *lblTag3;
//      IBOutlet CustomLabel *lblTag4;
//    IBOutlet CustomLabel *lblName;
//    IBOutlet UIImageView *ivProduct;
}

@property (strong, nonatomic) IBOutlet UIImageView *ivProfile;
@property (weak, nonatomic) IBOutlet CustomLabel *lblPrice;

@property (strong, nonatomic)IBOutlet CustomLabel *lblTag2;
@property (strong, nonatomic)IBOutlet CustomLabel *lblTag1;
@property (strong, nonatomic)IBOutlet CustomLabel *lblTag3;
@property (strong, nonatomic)IBOutlet CustomLabel *lblTag4;
@property (strong, nonatomic)IBOutlet CustomLabel *lblName;
@property (strong, nonatomic) IBOutlet CustomLabel *lblBrand;
@property (strong, nonatomic) IBOutlet UIButton *btncolor1;
@property (strong, nonatomic) IBOutlet UIButton *btncolor3;
@property (strong, nonatomic) IBOutlet TLTagsControl *tagscontrol;
@property (strong, nonatomic) IBOutlet UIButton *btncolor2;
@property (strong, nonatomic) IBOutlet UIButton *btncolor4;
@property (strong, nonatomic)IBOutlet UIImageView *ivProduct;
@end
