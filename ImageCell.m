//
//  ImageCell.m
//  Gilt
//
//  Created by Prashant  on 28/10/15.
//  Copyright © 2015 Prashant . All rights reserved.
//

#import "ImageCell.h"

@implementation ImageCell
- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        [self.tagscontrol setMode:TLTagsControlModeList];
        self.ivProfile.layer.masksToBounds=YES;
        self.ivProfile.layer.cornerRadius=self.ivProfile.frame.size.width/2.0;

    }
    
    return self;
    
}
@end
