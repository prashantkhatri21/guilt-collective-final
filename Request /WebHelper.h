//
//  WebHelper.h
//  ChatR
//
//  Created by Prashant on 5/20/15.
//  Copyright (c) 2015 Prashant. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol WebHelperDelegate <NSObject, NSURLConnectionDelegate>


- (void) didReceivedresponse: (NSDictionary *) responsedict;

- (void) didFailedWithError: (NSError *) error;


@end


@interface WebHelper : NSObject
{
    NSMutableData *responseData;
    
}
@property (nonatomic, retain) NSOperationQueue *queue;


@property (nonatomic, weak) id <WebHelperDelegate> WebHelperDelegate;

@property (nonatomic, retain) NSDictionary *fbDict;

//@property (nonatomic, strong) NSString *serverAddress;


+ (WebHelper *) sharedHelper;


- (void) submitPost:(NSString *)urlStr param:(NSDictionary *)dict img:(UIImage*)image;

- (void) calBackServices:(NSString *)urlStr prm:(NSString *)parameterStr arratContact:(NSMutableArray*)arrContact;

- (void) updateProfile:(NSString *)urlStr param:(NSDictionary *)dict img:(UIImage*)image;

- (void) registerProfile:(NSString *)urlStr param:(NSDictionary *)dict img:(UIImage*)image;

- (void) submitPost:(NSString *)urlStr param:(NSDictionary *)dict pdf:(NSData*)pdf;

@end
