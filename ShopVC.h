//
//  ShopVC.h
//  Guilt
//
//  Created by Prashant khatri on 03/11/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelProduct.h"
@interface ShopVC : UIViewController
{
    __weak IBOutlet UICollectionView *collectionview;
    
    __weak IBOutlet UITableView *tableview;
}
-(void)selectFilter:(ModelProduct*)product genderType:(NSString *)gendertype;
@end
