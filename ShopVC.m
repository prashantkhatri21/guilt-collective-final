//
//  ShopVC.m
//  Guilt
//
//  Created by Prashant khatri on 03/11/15.
//  Copyright © 2015 Prashant khatri. All rights reserved.
//

#import "ShopVC.h"
#import "ImageCell.h"
#import "ModelProduct.h"
#import "TablecellSetting.h"
#import "ViewController.h"
#import "FilterVC.h"
@interface ShopVC ()
{
    BOOL isMen;
    NSMutableArray *arrayBrands,*newDataarray;
    ModelProduct *objProduct;
    NSString *stringCatagoryMenWomen;
    NSString *gender;
}
@end

@implementation ShopVC

- (void)viewDidLoad {
    [super viewDidLoad];
    INITIALIZE_LOADER
    arrayBrands=[[NSMutableArray alloc]init];
    newDataarray=[[NSMutableArray alloc]init];
    [self GetBrands];
    
    // Do any additional setup after loading the view.
}

/**
 *  Fetch all brands
 */
-(void)GetBrands
{
    [APPDELEGATE ShowProgress];
    
    NSString *url;
    if (objProduct!=nil) {
        url =[NSString stringWithFormat:@"%@%@%@%@",kBaseUrl_Live,Brands_API,APITokenURL_Product,@"&offset=0&limit=10"];

    }
    else{
        url =[NSString stringWithFormat:@"%@%@%@%@",kBaseUrl_Live,Brands_API,APITokenURL_Product,@"&offset=0&limit=10"];

    }
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [APPDELEGATE removeProgress];
        [self parseData:responseObject];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        [APPDELEGATE removeProgress];
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    }];
   
}

-(void)parseData:(id)responseObject
{
    [arrayBrands removeAllObjects];
    
    if ([responseObject valueForKey:@"brands"] !=nil)
    {
        NSLog(@"brands=%@",[responseObject valueForKey:@"brands"]);
        if ([[responseObject valueForKey:@"brands"] count]==0) {
            [APPDELEGATE showToast:@"No brands Found"];
        }
        for (id dictProduct in [responseObject valueForKey:@"brands"]) {
            ModelProduct *objModelProdct=[[ModelProduct alloc]init];
            
            if ([dictProduct valueForKey:@"name"]!=nil) {
                objModelProdct.name=[dictProduct valueForKey:@"name"];
            }
            if ([dictProduct valueForKey:@"id"]!=nil) {
                objModelProdct.idProduct=[dictProduct valueForKey:@"id"];
                
            }
            
            [arrayBrands addObject:objModelProdct];
        }
    }
    [tableview reloadData];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    INITIALIZE_LOADER
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView dataSource/delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    return CGSizeMake(self.view.frame.size.width/3-4, self.view.frame.size.width/3-5);
}


#pragma mark Tableview Delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayBrands.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    
    TablecellSetting *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[TablecellSetting alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    ModelProduct *objProduct=[arrayBrands objectAtIndex:indexPath.row];
    cell.ivCheck.hidden = YES;
    cell.lblSetting.text = objProduct.name;
    
        if (indexPath.row==[arrayBrands count]-2) {
            [self addMoreSearchData:[arrayBrands count]];
        }
    //    if ([[arrayChecks objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
    //        cell.ivCheck.hidden = YES;
    //
    //    }
    //    else{
    //        cell.ivCheck.hidden = NO;
    //
    //    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
    ViewController *objVC=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    if ([gender length]>0) {
    }
        //    objVC.arrayColor=self.arrayColor;
    objVC.objBrands=[arrayBrands objectAtIndex:indexPath.row];
    objVC.objProduct=objProduct;
    [self.navigationController pushViewController:objVC animated:YES];
    //    if ([str isEqualToString:@"0"]) {
    //        [arrayChecks replaceObjectAtIndex:indexPath.row withObject:@"1"];
    //    }
    //    else{
    //        [arrayChecks replaceObjectAtIndex:indexPath.row withObject:@"0"];
    //
    //    }
    //    [tableView reloadData];
}
/**
 *  For getting more Brands
 *
 *  @param offset offest is index from where you wanto get data
 */
-(void)addMoreSearchData :(int)offset
{
    NSString *stringCatagoryMenWomen=@"";
    
    NSString *url =[NSString stringWithFormat:@"%@%@%@&offset=%@&limit=%@",kBaseUrl_Live,Brands_API,APITokenURL_Product,[NSString stringWithFormat:@"%d",offset],@"10"];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject valueForKey:@"brands"] !=nil)
        {
            NSLog(@"brands=%@",[responseObject valueForKey:@"brands"]);
            [newDataarray removeAllObjects ];
            
            for (id dictProduct in [responseObject valueForKey:@"brands"]) {
                ModelProduct *objModelProdct=[[ModelProduct alloc]init];
                
                if ([dictProduct valueForKey:@"name"]!=nil) {
                    objModelProdct.name=[dictProduct valueForKey:@"name"];
                }
                if ([dictProduct valueForKey:@"id"]!=nil) {
                    objModelProdct.idProduct=[dictProduct valueForKey:@"id"];
                    
                }
                
                
                [newDataarray addObject:objModelProdct];
            }
        }
        [self addNewCells];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
    }];
    
    
   
}

-(void)selectFilter:(ModelProduct*)product genderType:(NSString *)gendertype

{
    objProduct=product;
    gender=gendertype;
    //[self GetBrands];
}


-(void)addNewCells {
    
    [tableview beginUpdates];
    
    
    
    
    int resultsSize = [arrayBrands count];
    [arrayBrands addObjectsFromArray:newDataarray];
    NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
    for (int i = resultsSize; i < resultsSize + newDataarray.count; i++)
    {
        [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    
    [tableview insertRowsAtIndexPaths:arrayWithIndexPaths withRowAnimation:UITableViewRowAnimationNone];
    [tableview endUpdates];
    
}





- (IBAction)filterClick:(id)sender {
    
    [self performSegueWithIdentifier:@"shop" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"shop"]) {
        FilterVC *obj=segue.destinationViewController;
        obj.delegate=self;
        obj.navigationType=@"shop";
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
