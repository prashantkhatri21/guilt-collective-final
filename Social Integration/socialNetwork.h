//
//  socialNetwork.h
//  Billangua
//
//  Created by Suraj on 10/09/15.
//  Copyright (c) 2015 360itpro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@protocol socialNetworkDelegate <NSObject>


-(void)success :(NSString*)socialType;

-(void)failedWithError:(NSString *)error;

-(void)cancel;

@end

@interface socialNetwork : NSObject
//+(id)sharedInstance;
-(void)getFbInfo;
-(void)getTwitterInfo;
-(void)getIntagramInfo;

//facebook
@property(nonatomic,strong)NSString *fbUserName;
@property(nonatomic,strong)NSString *fbUserId;
@property(nonatomic,strong)NSString *fbEmail;
@property(nonatomic,strong)NSString *fbProfilePic;


//twitter
@property(nonatomic,strong)NSString *twitterUserName;
@property(nonatomic,strong)NSString *twitterScreenName;
@property(nonatomic,strong)NSString *twitterProfilePic;
@property(nonatomic,strong)NSString *twitterUserId;


@property(nonatomic,retain)id<socialNetworkDelegate> delegate;






@end
