//
//  socialNetwork.m
//  Billangua
//
//  Created by Suraj on 10/09/15.
//  Copyright (c) 2015 360itpro. All rights reserved.
//

#import "socialNetwork.h"
static socialNetwork *_sharedSocial = nil;

@implementation socialNetwork
@synthesize delegate;
//+(id)sharedInstance{
//    static dispatch_once_t oncePredicate;
//    dispatch_once(&oncePredicate, ^{
//        _sharedSocial = [[self alloc] init];
//    });
//    return _sharedSocial;
//}


-(void)getFbInfo{
    
    
    
    
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorSystemAccount;
    
    [login logOut];
      [FBSDKAccessToken setCurrentAccessToken:nil];
    [login
     logInWithReadPermissions: @[@"public_profile"]
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
             [delegate failedWithError:[error localizedDescription]];
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
             [delegate cancel];
         } else {
             NSLog(@"Logged in");
             
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"picture, email, name"}]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                      if (!error) {
                          NSLog(@"fetched user:%@", result);
                          
                          if ([result valueForKey:@"email"]!=nil) {
                              
                              self.fbEmail = [result valueForKey:@"email"];
                              
                          }
                          
                          if ([result valueForKey:@"name"]!=nil) {
                              
                              self.fbUserName = [result valueForKey:@"name"];
                              
                          }
                          
                          if ([result valueForKey:@"id"]!=nil) {
                              
                              self.fbUserId = [result valueForKey:@"id"];
                              
                          }
                          
                          NSDictionary *pic = [[NSDictionary alloc] init];
                          pic =  [[result valueForKey:@"picture"] valueForKey:@"data"];
                          
                          if ([pic valueForKey:@"url"]!=nil) {
                              self.fbProfilePic = [pic valueForKey:@"url"];
                              
                          }
                          
                          [delegate success:@"facebook"];
                          
                          
                          //                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success facebook login" message:[NSString stringWithFormat:@"Facebook account details::  %@ ",result] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                          //
                          //                          [alert show];
                      }
                  }];
             
         }
     }];
    
    
}
-(void)getIntagramInfo{
    
}

-(void)getTwitterInfo{
    
//    [[Twitter sharedInstance] logInWithCompletion:^
//     (TWTRSession *session, NSError *error) {
//         if (session) {
//             NSLog(@"signed in as %@", [session userName]);
//             
//             [[[Twitter sharedInstance] APIClient] loadUserWithID:session.userID completion:^(TWTRUser *user, NSError *error) {
//                 NSLog(@"User info ------------- user name : %@ , ------------- user profile pic url : %@ , Screen name : %@ , user ID %@", user.name , user.profileImageURL , user.screenName , user.userID );
//                 
//                 
//                 self.twitterProfilePic = user.profileImageLargeURL;
//                 self.twitterUserName = user.name;
//                 self.twitterScreenName = user.screenName;
//                 self.twitterUserId = user.userID;
//                 
//                 [delegate success:@"twitter"];
//                 //                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success twitter login" message:[NSString stringWithFormat:@"Twitter account details::       User info ------------- user name : %@ , ------------- user profile pic url : %@ , Screen name : %@ , user ID %@",user.name , user.profileImageURL , user.screenName , user.userID] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//                 //
//                 //                 [alert show];
//                 
//             }];
//         } else {
//             
//             [delegate failedWithError:[error localizedDescription]];
//             NSLog(@"error: %@", [error localizedDescription]);
//         }
//     }];
    
    
}

@end
