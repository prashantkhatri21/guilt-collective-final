//
//  ViewController.h
//  Gilt
//
//  Created by Prashant  on 28/10/15.
//  Copyright © 2015 Prashant . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModelColor.h"
#import "Utility.h"
#import "ModelProduct.h"
@interface ViewController : UIViewController
@property (strong, nonatomic) NSMutableArray *arrayColor;
@property (strong, nonatomic) ModelProduct *objBrands;
@property (strong, nonatomic) ModelProduct *objProduct;
@property (strong, nonatomic) NSString *navigationType;
@property (nonatomic) BOOL isshare;
@property (strong, nonatomic) NSString *strGuiltID;

@property (weak, nonatomic) IBOutlet CustomButton *btnDone;
@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

//Delegate method
-(void)getSelectFilter:(NSMutableArray*)arraycolorlocal catagory:(ModelProduct*)objcatagory barnd:(ModelProduct*)objBrandlocal;

@end

