//
//  ViewController.m
//  Gilt
//
//  Created by Prashant  on 28/10/15.
//  Copyright © 2015 Prashant . All rights reserved.
//

#import "ViewController.h"
#import "ImageCell.h"
#import "Utility.h"
#import "Addmorecell.h"
#import "UIImageView+WebCache.h"
#import "FilterVC.h"
#import "webVC.h"
@interface ViewController ()
{
    NSMutableArray *arrayProduct,*newDataarray;
    MBProgressHUD *HUD,*HUDToast;

}
@end

@implementation ViewController


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        // Set Title
        
        UIImage *musicImage = [UIImage imageNamed:@"store_blk"];
        UIImage *musicImageSel = [UIImage imageNamed:@"cart_white"];
        
        musicImage = [musicImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        musicImageSel = [musicImageSel imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        self.navigationController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"SHOP" image:musicImage selectedImage:musicImageSel];
        [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor blackColor], NSForegroundColorAttributeName,[UIFont fontWithName:@"OSWALD" size:10], UITextAttributeFont,
                                                           nil] forState:UIControlStateNormal];
        
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initializeMBProgres:self.view];
    
    arrayProduct=[[NSMutableArray alloc]init];
    newDataarray=[[NSMutableArray alloc]init];
    if ([self.navigationType isEqualToString:@"FIND"]) {
        [self.btnBack setImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
        self.btnDone.hidden=NO;
    }
    
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getMatches];
    self.tabBarController.tabBar.hidden=NO;
}


-(NSString *)createUrl:(int)offset
{
    
    NSMutableArray *arrayParameter=[[NSMutableArray alloc]init];
    for (ModelColor *objcolor in self.arrayColor) {
        
        NSString *str=[NSString stringWithFormat:@"fl=c%@",objcolor.idcolor];
        [arrayParameter addObject:str];
    }
    if (_objBrands.name!=nil) {
        [arrayParameter addObject:[NSString stringWithFormat:@"fl=b%@",_objBrands.idProduct]];
    }
    if (_objProduct.name!=nil) {
        [arrayParameter addObject:[NSString stringWithFormat:@"cat=%@",_objProduct.idProduct]];
    }
    
    
    NSString *str=[arrayParameter componentsJoinedByString:@"&"];
    
    NSString *url =[NSString stringWithFormat:@"%@%@%@&%@&offset=%d&limit=10",kBaseUrl_Live,PRODUCTS_API,APITokenURL_Product,str,offset];
    NSLog(@"stringPaameter=%@",url);
    NSString *encoded = [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"URL=%@",encoded);
    return encoded;
}
/**
 *  GetMatches
 */
-(void)getMatches
{
    [self ShowProgress];
    
    NSString *url = [self createUrl:0];
    
    if (url!=nil) {
        
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            [self parseData:responseObject];
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"Error: %@", error);
            [self removeProgress];
            
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
        }];
        
    }
    
}

-(void)parseData:(id)responseObject
{
    [arrayProduct removeAllObjects];
    
    if ([responseObject valueForKey:@"products"] !=nil)
    {
        NSLog(@"PRODUCTS=%@",[responseObject valueForKey:@"products"]);
        if ([[responseObject valueForKey:@"products"] count]==0) {
            [APPDELEGATE showToast:@"No product Found"];
        }
        for (id dictProduct in [responseObject valueForKey:@"products"]) {
            ModelProduct *objModelProdct=[[ModelProduct alloc]init];
            
            if ([dictProduct valueForKey:@"name"]!=nil) {
                objModelProdct.name=[dictProduct valueForKey:@"name"];
            }
            if ([dictProduct valueForKey:@"id"]!=nil) {
                objModelProdct.idProduct=[dictProduct valueForKey:@"id"];
                
            }
            if ([dictProduct valueForKey:@"brand"]!=nil) {
                NSDictionary *dictBrand=[dictProduct valueForKey:@"brand"];
                objModelProdct.brand=[dictBrand valueForKey:@"name"];
                
            }
            if ([dictProduct valueForKey:@"price"]!=nil) {
                
                NSString* str = [NSString stringWithFormat:@"%@", [dictProduct valueForKey:@"price"]];
                float pricefloat=[str floatValue];
                NSString* formattedNumber = [NSString stringWithFormat:@"%.02f", pricefloat];


                objModelProdct.price=[NSString stringWithFormat:@"$%@",formattedNumber] ;
            }
            if ([dictProduct valueForKey:@"clickUrl"]!=nil) {
                objModelProdct.productUrl=[NSURL URLWithString:[dictProduct valueForKey:@"clickUrl"]];
            }
            if ([dictProduct valueForKey:@"image"]!=nil) {
                if ([[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]!=nil) {
                    if ([[[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]valueForKey:IMAGESize_Iphone]!=nil) {
                        objModelProdct.imageurl=[NSURL URLWithString:[[[[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]valueForKey:IMAGESize_Iphone] valueForKey:@"url"]];
                    }
                }
            }
            [arrayProduct addObject:objModelProdct];
        }
    }
    [_collectionview reloadData];
}

- (NSInteger)numberOfSections
{
    return 0;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrayProduct.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"image";
    
    ImageCell *cell;
    Addmorecell *addMoreCell;
    if(indexPath.row < [arrayProduct count]){
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        [cell.ivProfile updateConstraints];
        ModelProduct *objModelProduct=[arrayProduct objectAtIndex:indexPath.row];
        cell.lblName.text =objModelProduct.name;
        cell.lblBrand.text =objModelProduct.brand;
        cell.lblPrice.text =objModelProduct.price;
        [cell.ivProduct sd_setImageWithURL:objModelProduct.imageurl placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        cell.ivProfile.layer.cornerRadius = 52/2;
        cell.ivProfile.layer.borderWidth = 1;
        
        if (indexPath.row==[arrayProduct count]-2) {
            [self addMoreSearchData:[arrayProduct count]];
        }
        //  UIImageView *recipeImageView = (UIImageView *)[cell viewWithTag:100];
        
        return cell;
    }else{
        addMoreCell = (Addmorecell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"celladdmore"
                                                                              forIndexPath:indexPath];
        return addMoreCell;
        
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.navigationType isEqualToString:@"FIND"]) {
       
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Guilt" message:@"Did you found Guilt?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            alert.tag=indexPath.row;
            [alert show];
        
        
    }
    else{
        ModelProduct *objModelProduct=[arrayProduct objectAtIndex:indexPath.row];
        webVC *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"webVC"];
        obj.urlString=objModelProduct.productUrl;
        [self.navigationController pushViewController:obj animated:YES];
    }
   
    
}
- (CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
//    return CGSizeMake([[UIScreen mainScreen] bounds].size.width /2-8, ([[UIScreen mainScreen] bounds].size.width/2-8)*1.8 );
    
    return CGSizeMake([[UIScreen mainScreen] bounds].size.width /2-8, ([[UIScreen mainScreen] bounds].size.width/2-8+20));

    
}

-(void)addMoreSearchData :(int)offset
{
    
    
    NSString *url = [self createUrl:offset];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject valueForKey:@"products"] !=nil)
        {
            NSLog(@"PRODUCTS=%@",[responseObject valueForKey:@"products"]);
            [newDataarray removeAllObjects ];
            for (id dictProduct in [responseObject valueForKey:@"products"]) {
                ModelProduct *objModelProdct=[[ModelProduct alloc]init];
                
                if ([dictProduct valueForKey:@"name"]!=nil) {
                    objModelProdct.name=[dictProduct valueForKey:@"name"];
                }
                if ([dictProduct valueForKey:@"id"]!=nil) {
                    objModelProdct.idProduct=[dictProduct valueForKey:@"id"];
                    
                }
                
                if ([dictProduct valueForKey:@"brand"]!=nil) {
                    NSDictionary *dictBrand=[dictProduct valueForKey:@"brand"];
                    objModelProdct.brand=[dictBrand valueForKey:@"name"];
                    
                }
                if ([dictProduct valueForKey:@"price"]!=nil) {
                    objModelProdct.price=[NSString stringWithFormat:@"$%@",[dictProduct valueForKey:@"price"]] ;
                }
                if ([dictProduct valueForKey:@"clickUrl"]!=nil) {
                    objModelProdct.productUrl=[NSURL URLWithString:[dictProduct valueForKey:@"clickUrl"]];
                }
                if ([dictProduct valueForKey:@"image"]!=nil) {
                    if ([[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]!=nil) {
                        if ([[[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]valueForKey:IMAGESize_Iphone]!=nil) {
                            objModelProdct.imageurl=[NSURL URLWithString:[[[[dictProduct valueForKey:@"image"] valueForKey:@"sizes"]valueForKey:IMAGESize_Iphone] valueForKey:@"url"]];
                        }
                    }
                }
                [newDataarray addObject:objModelProdct];
            }
        }
        [self addNewCells];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        
        
        [APPDELEGATE showAlert:@"" message:error.localizedDescription];
        
    }];
    
}
-(void)addNewCells {
    [self.collectionview performBatchUpdates:^{
        int resultsSize = [arrayProduct count];
        [arrayProduct addObjectsFromArray:newDataarray];
        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
        for (int i = resultsSize; i < resultsSize + newDataarray.count; i++)
        {
            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
        [self.collectionview insertItemsAtIndexPaths:arrayWithIndexPaths];
    }
                                  completion:nil];
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    ModelProduct *objModelProduct=[arrayProduct objectAtIndex:alertView.tag];

    if (buttonIndex==1) {
        
        
        [self ShowProgress];
        
        
        NSMutableDictionary *dict;
        NSString *url=[NSString stringWithFormat:@"%@%@",kBaseUrl,Found_GUILT_API];
        dict=[[NSMutableDictionary alloc]init];
        [dict setValue:TOKEN forKey:@"token"];
        [dict setValue:[DEFAULTS valueForKey:USERID] forKey:@"user_id"];
        [dict setValue:self.strGuiltID forKey:@"guilt_id"];
        [dict setValue:@"1" forKey:@"found"];
        [dict setValue:[NSString stringWithFormat:@"%@",objModelProduct.productUrl] forKey:@"found_link"];
        [dict setValue:[NSString stringWithFormat:@"%@",objModelProduct.price] forKey:@"price"];
        [dict setValue:[NSString stringWithFormat:@"%@",objModelProduct.name] forKey:@"title"];
        
        
        
        
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        
        [manager setRequestSerializer:requestSerializer];
        [manager POST:url parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            [self removeProgress];
            if ([[responseObject valueForKey:@"status"]  intValue] == 1)
            {
                //-----Not found Guilt--//
                
                if ([responseObject valueForKey:@"message"]!=nil) {
                    [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"message"]];
                }
                webVC *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"webVC"];
                obj.urlString=objModelProduct.productUrl;
                [self.navigationController pushViewController:obj animated:YES];
            }
            else
            {
                if ([responseObject valueForKey:@"data"]!=nil) {
                    if ([[responseObject valueForKey:@"data"] isKindOfClass:[NSArray class]]) {
                        
                    }
                    else{
                        [APPDELEGATE showAlert:@"" message:[responseObject valueForKey:@"data"]];
                        
                    }
                }
                
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            [self removeProgress];
            [APPDELEGATE showAlert:@"" message:error.localizedDescription];
            
            
        }];

    }
    else{
        webVC *obj=[self.storyboard instantiateViewControllerWithIdentifier:@"webVC"];
        obj.urlString=objModelProduct.productUrl;
        [self.navigationController pushViewController:obj animated:YES];
    }
    
}
- (IBAction)doneClick:(id)sender {
    self.tabBarController.selectedIndex = 0;
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    
}


- (IBAction)backClick:(id)sender {
    if ([self.navigationType isEqualToString:@"FIND"]) {
        POP
    }
    else{
        [self performSegueWithIdentifier:@"FILTER" sender:self];
        
    }
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"FILTER"]) {
        FilterVC *objVC=segue.destinationViewController;
        objVC.delegate=self;
    }
}

/**
 *  delegate for filter data data come to this method
 */
-(void)getSelectFilter:(NSMutableArray*)arraycolorlocal catagory:(ModelProduct*)objcatagory barnd:(ModelProduct*)objBrandlocal
{
    self.arrayColor=arraycolorlocal;
    self.objProduct=objcatagory;
    self.objBrands=objBrandlocal;
    [self getMatches];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark MBProgress
-(void)initializeMBProgres:(UIView*)view
{
    if(HUD!=nil) {
        [HUD removeFromSuperview];
        HUD =nil;
        
    }
    if(HUDToast !=nil) {
        [HUDToast removeFromSuperview];
    }
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    HUD.color=[UIColor whiteColor];
    
    [view addSubview:HUD];
    
    HUD.labelText = @"Please wait...";
    HUD.mode=MBProgressHUDModeIndeterminate;
    
    HUDToast = [[MBProgressHUD alloc] initWithView:self.view];
    HUDToast.color=[UIColor whiteColor];
    
    HUDToast.mode=MBProgressHUDModeText;
    
    
    [view addSubview:HUDToast];
    
}
#pragma mark-
#pragma mark-Toast Delegate

-(void)showToast:(NSString *)msgString
{
    [HUDToast show:YES];
    HUDToast.labelText = msgString;
    [self performSelector:@selector(removeToast) withObject:nil afterDelay:toastTimeDuration];
}
-(void)removeToast
{
    [HUDToast hide:YES];
    
}

#pragma mark - Progress
-(void)removeProgress
{
    [HUD hide:YES];
}
-(void)ShowProgress
{
    [HUD show:YES];
}




@end
